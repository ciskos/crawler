package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserDTOTest {

	private static final LocalDateTime LOCALDATETIME = LocalDateTime.now();
	private static final LocalDateTime LOCALDATETIME_NEW = LocalDateTime.now();
	private UserDTO userDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		userDTO = new UserDTO();
		
		userDTO.setId(1L);
		userDTO.setUsername("user1");
		userDTO.setPassword("pass1");
		userDTO.setEmail("mail1@email.com");
		userDTO.setEnabled(Boolean.TRUE);
		userDTO.setRegistered(LOCALDATETIME);
	}

	@Test
	void testGetId() {
		assertEquals(userDTO.getId(), 1L);
	}

	@Test
	void testGetUsername() {
		assertEquals(userDTO.getUsername(), "user1");
	}

	@Test
	void testGetPassword() {
		assertEquals(userDTO.getPassword(), "pass1");
	}

	@Test
	void testGetEmail() {
		assertEquals(userDTO.getEmail(), "mail1@email.com");
	}

	@Test
	void testGetEnabled() {
		assertEquals(userDTO.getEnabled(), Boolean.TRUE);
	}

	@Test
	void testGetRegistered() {
		assertEquals(userDTO.getRegistered(), LOCALDATETIME);
	}

	@Test
	void testSetId() {
		userDTO.setId(2L);
		assertEquals(userDTO.getId(), 2L);
	}

	@Test
	void testSetUsername() {
		userDTO.setUsername("user2");
		assertEquals(userDTO.getUsername(), "user2");
	}

	@Test
	void testSetPassword() {
		userDTO.setPassword("pass2");
		assertEquals(userDTO.getPassword(), "pass2");
	}

	@Test
	void testSetEmail() {
		userDTO.setEmail("mail2@email.com");
		assertEquals(userDTO.getEmail(), "mail2@email.com");
	}

	@Test
	void testSetEnabled() {
		userDTO.setEnabled(Boolean.FALSE);
		assertEquals(userDTO.getEnabled(), Boolean.FALSE);
	}

	@Test
	void testSetRegistered() {
		userDTO.setRegistered(LOCALDATETIME_NEW);
		assertEquals(userDTO.getRegistered(), LOCALDATETIME_NEW);
	}

}
