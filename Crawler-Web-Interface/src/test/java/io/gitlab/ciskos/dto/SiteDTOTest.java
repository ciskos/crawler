package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SiteDTOTest {

	private SiteDTO siteDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		siteDTO = new SiteDTO();
		
		siteDTO.setId(1L);
		siteDTO.setUrl("siteUrl1");
		siteDTO.setDescription("siteDescription1");
	}

	@Test
	void testGetId() {
		assertEquals(siteDTO.getId(), 1L);
	}

	@Test
	void testGetUrl() {
		assertEquals(siteDTO.getUrl(), "siteUrl1");
	}

	@Test
	void testGetDescription() {
		assertEquals(siteDTO.getDescription(), "siteDescription1");
	}

	@Test
	void testSetId() {
		siteDTO.setId(2L);
		assertEquals(siteDTO.getId(), 2L);
	}

	@Test
	void testSetUrl() {
		siteDTO.setUrl("siteUrl2");
		assertEquals(siteDTO.getUrl(), "siteUrl2");
	}

	@Test
	void testSetDescription() {
		siteDTO.setDescription("siteDescription2");
		assertEquals(siteDTO.getDescription(), "siteDescription2");
	}

}
