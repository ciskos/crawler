package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ThemeDTOTest {

	private ThemeDTO themeDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		themeDTO = new ThemeDTO();
		
		themeDTO.setId(1L);
		themeDTO.setName("theme1");
	}

	@Test
	void testGetId() {
		assertEquals(themeDTO.getId(), 1L);
	}

	@Test
	void testGetName() {
		assertEquals(themeDTO.getName(), "theme1");
	}

	@Test
	void testSetId() {
		themeDTO.setId(2L);
		assertEquals(themeDTO.getId(), 2L);
	}

	@Test
	void testSetName() {
		themeDTO.setName("theme2");
		assertEquals(themeDTO.getName(), "theme2");
	}

}
