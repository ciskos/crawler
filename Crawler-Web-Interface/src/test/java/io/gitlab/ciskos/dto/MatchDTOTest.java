package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatchDTOTest {

	private MatchDTO matchDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		matchDTO = new MatchDTO();
		
		matchDTO.setId(1L);
		matchDTO.setThemeId(1L);
		matchDTO.setPageId(1L);
		matchDTO.setThemeName("theme1");
		matchDTO.setPageName("page1");
		matchDTO.setCounts(1L);
	}

	@Test
	void testGetId() {
		assertEquals(matchDTO.getId(), 1L);
	}

	@Test
	void testGetThemeId() {
		assertEquals(matchDTO.getThemeId(), 1L);
	}

	@Test
	void testGetPageId() {
		assertEquals(matchDTO.getPageId(), 1L);
	}

	@Test
	void testGetThemeName() {
		assertEquals(matchDTO.getThemeName(), "theme1");
	}

	@Test
	void testGetPageName() {
		assertEquals(matchDTO.getPageName(), "page1");
	}

	@Test
	void testGetCounts() {
		assertEquals(matchDTO.getCounts(), 1L);
	}

	@Test
	void testSetId() {
		matchDTO.setId(2L);
		assertEquals(matchDTO.getId(), 2L);
	}

	@Test
	void testSetThemeId() {
		matchDTO.setThemeId(2L);
		assertEquals(matchDTO.getThemeId(), 2L);
	}

	@Test
	void testSetPageId() {
		matchDTO.setPageId(2L);
		assertEquals(matchDTO.getPageId(), 2L);
	}

	@Test
	void testSetThemeName() {
		matchDTO.setThemeName("theme2");
		assertEquals(matchDTO.getThemeName(), "theme2");
	}

	@Test
	void testSetPageName() {
		matchDTO.setPageName("page2");
		assertEquals(matchDTO.getPageName(), "page2");
	}

	@Test
	void testSetCounts() {
		matchDTO.setCounts(2L);
		assertEquals(matchDTO.getCounts(), 2L);
	}

}
