package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class KeywordDTOTest {

	private KeywordDTO keywordDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		keywordDTO = new KeywordDTO();
		
		keywordDTO.setId(1L);
		keywordDTO.setThemeId(1L);
		keywordDTO.setThemeName("theme1");
		keywordDTO.setKeyword("keyword1");
	}

	@Test
	void testGetId() {
		assertEquals(keywordDTO.getId(), 1L);
	}

	@Test
	void testGetThemeId() {
		assertEquals(keywordDTO.getThemeId(), 1L);
	}

	@Test
	void testGetThemeName() {
		assertEquals(keywordDTO.getThemeName(), "theme1");
	}

	@Test
	void testGetKeyword() {
		assertEquals(keywordDTO.getKeyword(), "keyword1");
	}

	@Test
	void testSetId() {
		keywordDTO.setId(2L);
		assertEquals(keywordDTO.getId(), 2L);
	}

	@Test
	void testSetThemeId() {
		keywordDTO.setThemeId(2L);
		assertEquals(keywordDTO.getThemeId(), 2L);
	}

	@Test
	void testSetThemeName() {
		keywordDTO.setThemeName("theme2");
		assertEquals(keywordDTO.getThemeName(), "theme2");
	}

	@Test
	void testSetKeyword() {
		keywordDTO.setKeyword("keyword2");
		assertEquals(keywordDTO.getKeyword(), "keyword2");
	}

}
