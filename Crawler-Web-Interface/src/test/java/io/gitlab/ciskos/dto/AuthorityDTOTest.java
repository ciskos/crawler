package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.gitlab.ciskos.model.Role;

class AuthorityDTOTest {

	private AuthorityDTO authorityDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		authorityDTO = new AuthorityDTO();
		
		authorityDTO.setId(1L);
		authorityDTO.setUserId(1L);
		authorityDTO.setUsername("user1");
		authorityDTO.setRole(Role.ROLE_ADMIN.name());
	}

	@Test
	void testGetId() {
		assertEquals(authorityDTO.getId(), 1L);
	}

	@Test
	void testGetUserId() {
		assertEquals(authorityDTO.getUserId(), 1L);
	}

	@Test
	void testGetUsername() {
		assertEquals(authorityDTO.getUsername(), "user1");
	}

	@Test
	void testGetRole() {
		assertEquals(authorityDTO.getRole(), Role.ROLE_ADMIN.name());
	}

	@Test
	void testSetId() {
		authorityDTO.setId(2L);
		assertEquals(authorityDTO.getId(), 2L);
	}

	@Test
	void testSetUserId() {
		authorityDTO.setUserId(2L);
		assertEquals(authorityDTO.getUserId(), 2L);
	}

	@Test
	void testSetUsername() {
		authorityDTO.setUsername("user2");
		assertEquals(authorityDTO.getUsername(), "user2");
	}

	@Test
	void testSetRole() {
		authorityDTO.setRole(Role.ROLE_USER.name());
		assertEquals(authorityDTO.getRole(), Role.ROLE_USER.name());
	}

}
