package io.gitlab.ciskos.dto;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class PageDTOTest {

	private static final LocalDateTime LOCALDATETIME = LocalDateTime.now();
	private static final LocalDateTime LOCALDATETIME_NEW = LocalDateTime.now();
	private PageDTO pageDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		pageDTO = new PageDTO();
		
		pageDTO.setId(1L);
		pageDTO.setSiteId(1L);
		pageDTO.setSiteUrl("siteUrl1");
		pageDTO.setPageUrl("pageUrl1");
		pageDTO.setRegistered(LOCALDATETIME);
		pageDTO.setLastScan(LOCALDATETIME);
	}

	@Test
	void testGetId() {
		assertEquals(pageDTO.getId(), 1L);
	}

	@Test
	void testGetSiteId() {
		assertEquals(pageDTO.getSiteId(), 1L);
	}

	@Test
	void testGetSiteUrl() {
		assertEquals(pageDTO.getSiteUrl(), "siteUrl1");
	}

	@Test
	void testGetPageUrl() {
		assertEquals(pageDTO.getPageUrl(), "pageUrl1");
	}

	@Test
	void testGetRegistered() {
		assertEquals(pageDTO.getRegistered(), LOCALDATETIME);
	}

	@Test
	void testGetLastScan() {
		assertEquals(pageDTO.getLastScan(), LOCALDATETIME);
	}

	@Test
	void testSetId() {
		pageDTO.setId(2L);
		assertEquals(pageDTO.getId(), 2L);
	}

	@Test
	void testSetSiteId() {
		pageDTO.setSiteId(2L);
		assertEquals(pageDTO.getSiteId(), 2L);
	}

	@Test
	void testSetSiteUrl() {
		pageDTO.setSiteUrl("siteUrl2");
		assertEquals(pageDTO.getSiteUrl(), "siteUrl2");
	}

	@Test
	void testSetPageUrl() {
		pageDTO.setPageUrl("pageUrl2");
		assertEquals(pageDTO.getPageUrl(), "pageUrl2");
	}

	@Test
	void testSetRegistered() {
		pageDTO.setRegistered(LOCALDATETIME_NEW);
		assertEquals(pageDTO.getRegistered(), LOCALDATETIME_NEW);
	}

	@Test
	void testSetLastScan() {
		pageDTO.setLastScan(LOCALDATETIME_NEW);
		assertEquals(pageDTO.getLastScan(), LOCALDATETIME_NEW);
	}

}
