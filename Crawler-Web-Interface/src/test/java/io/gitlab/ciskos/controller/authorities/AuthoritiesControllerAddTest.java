package io.gitlab.ciskos.controller.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

import io.gitlab.ciskos.model.Role;

class AuthoritiesControllerAddTest extends AbstractAuthoritiesControllerTest {

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/authorities/add"))
			.andExpect(view().name("/add/authority"))
			.andExpect(model().attributeExists("authorityToUpdate"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("roles"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Authority")))
			.andExpect(content().string(containsString("User:")))
			.andExpect(content().string(containsString("Role:")));
	}

	@Test
	void testProcessCreationForm_success() throws Exception {
		mockMvc.perform(post("/authorities/add")
				.param("userId", "2")
				.param("role", Role.ROLE_ADMIN.name()))
			.andExpect(view().name("redirect:/authorities"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/authorities"));
	}

	@Test
	void testProcessCreationForm_username_must_not_be_blank() throws Exception {
		mockMvc.perform(post("/authorities/add")
					.param("userId", "")
					.param("role", ""))
			.andExpect(view().name("add/authority"))
			.andExpect(model().attributeExists("authorityToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Authority")))
			.andExpect(content().string(containsString("User must not be blank.")));
	}

	@Test
	void testProcessCreationForm_role_must_not_be_blank() throws Exception {
		mockMvc.perform(post("/authorities/add")
					.param("userId", "")
					.param("role", ""))
			.andExpect(view().name("add/authority"))
			.andExpect(model().attributeExists("authorityToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Authority")))
			.andExpect(content().string(containsString("Role must not be blank.")));
	}

}
