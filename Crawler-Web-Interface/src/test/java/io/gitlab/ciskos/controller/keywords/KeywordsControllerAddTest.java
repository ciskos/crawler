package io.gitlab.ciskos.controller.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class KeywordsControllerAddTest extends AbstractKeywordsControllerTest {

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/keywords/add"))
			.andExpect(view().name("/add/keyword"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(model().attributeExists("themes"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Keyword")))
			.andExpect(content().string(containsString("Theme:")))
			.andExpect(content().string(containsString("Keyword:")));
	}

	@Test
	void testProcessCreationForm_success() throws Exception {
		mockMvc.perform(post("/keywords/add")
				.param("themeId", "1")
				.param("keyword", "keyword"))
			.andExpect(view().name("redirect:/keywords"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/keywords"));
	}
	
	@Test
	void testProcessCreationForm_keyword_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/keywords/add")
					.param("themeId", "1")
					.param("keyword", ""))
			.andExpect(view().name("add/keyword"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Keyword")))
			.andExpect(content().string(containsString("Keyword must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Keyword must not be blank.")));
	}

}
