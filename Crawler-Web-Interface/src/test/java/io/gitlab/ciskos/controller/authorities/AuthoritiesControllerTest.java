package io.gitlab.ciskos.controller.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class AuthoritiesControllerTest extends AbstractAuthoritiesControllerTest {

	@Test
	void testGetAllAuthorities() throws Exception {
		mockMvc.perform(get("/authorities"))
			.andExpect(view().name("get/authorities"))
			.andExpect(model().attributeExists("authorities"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Authorities")))
			.andExpect(content().string(containsString("user1")))
			.andExpect(content().string(containsString("user2")));
	}

	@Test
	void testDeleteAuthorityById() throws Exception {
		mockMvc.perform(get("/authorities/{id}/delete", TEST_USER_ID))
			.andExpect(view().name("redirect:/authorities"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/authorities"));
	}

}
