package io.gitlab.ciskos.controller.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class AuthoritiesControllerUpdateTest extends AbstractAuthoritiesControllerTest {
	
	@Test
	void testInitUpdateAuthorityById_success() throws Exception {
		mockMvc.perform(get("/authorities/{id}/update", TEST_USER_ID))
			.andExpect(view().name("update/authority"))
			.andExpect(model().attributeExists("authorityToUpdate"))
			.andExpect(model().attribute("authorityToUpdate", hasProperty("id")))
			.andExpect(model().attribute("authorityToUpdate", hasProperty("userId")))
			.andExpect(model().attribute("authorityToUpdate", hasProperty("username")))
			.andExpect(model().attribute("authorityToUpdate", hasProperty("role")))
			.andExpect(model().attributeExists("roles"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Authority")))
			.andExpect(content().string(containsString("Username:")))
			.andExpect(content().string(containsString("Role:")));
	}

	@Test
	void testInitUpdateAuthorityById_fail() throws Exception {
		mockMvc.perform(get("/authorities/{id}/update", TEST_FAIL_USER_ID))
			.andExpect(view().name("redirect:/authorities"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/authorities"));
	}

	@Test
	void testProcessUpdateAuthorityById_success() throws Exception {
		mockMvc.perform(post("/authorities/{id}/update", TEST_USER_ID)
				.param("userId", "3")
				.param("role", "ROLE_ADMIN"))
			.andExpect(view().name("redirect:/authorities"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/authorities"));
	}

	@Test
	void testProcessUpdateAuthorityById_role_must_not_be_blank() throws Exception {
		mockMvc.perform(post("/authorities/{id}/update", TEST_USER_ID)
					.param("role", ""))
			.andExpect(view().name("update/authority"))
			.andExpect(model().attributeExists("authorityToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Authority")))
			.andExpect(content().string(containsString("Role must not be blank.")));
	}

}
