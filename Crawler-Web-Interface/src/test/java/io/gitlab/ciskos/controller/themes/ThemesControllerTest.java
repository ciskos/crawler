package io.gitlab.ciskos.controller.themes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class ThemesControllerTest extends AbstractThemesControllerTest {
	
	@Test
	void testGetAllThemes() throws Exception {
		mockMvc.perform(get("/themes"))
			.andExpect(view().name("get/themes"))
			.andExpect(model().attributeExists("themes"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Themes")))
			.andExpect(content().string(containsString("theme1")))
			.andExpect(content().string(containsString("theme2")));
	}

	@Test
	void testDeleteThemeById() throws Exception {
		mockMvc.perform(get("/themes/{id}/delete", TEST_THEME_ID))
			.andExpect(view().name("redirect:/themes"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/themes"));
	}

}
