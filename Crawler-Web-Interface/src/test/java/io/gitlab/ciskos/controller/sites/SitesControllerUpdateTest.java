package io.gitlab.ciskos.controller.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class SitesControllerUpdateTest extends AbstractSitesControllerTest {

	@Test
	void testInitUpdateSiteById_success() throws Exception {
		mockMvc.perform(get("/sites/{id}/update", TEST_SITE_ID))
			.andExpect(view().name("update/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(model().attribute("siteToUpdate", hasProperty("id", is(TEST_SITE_ID))))
			.andExpect(model().attribute("siteToUpdate", hasProperty("url")))
			.andExpect(model().attribute("siteToUpdate", hasProperty("description")))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Site")))
			.andExpect(content().string(containsString("Url:")))
			.andExpect(content().string(containsString("Description:")));
	}

	@Test
	void testInitUpdateSiteById_fail() throws Exception {
		mockMvc.perform(get("/sites/{id}/update", TEST_FAIL_SITE_ID))
			.andExpect(view().name("redirect:/sites"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/sites"));
	}

	@Test
	void testProcessUpdateSiteById_success() throws Exception {
		mockMvc.perform(post("/sites/{id}/update", TEST_SITE_ID)
				.param("url", "siteUrl1")
				.param("description", "siteDescription1"))
			.andExpect(view().name("redirect:/sites"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/sites"));
	}
	
	@Test
	void testProcessUpdateSiteById_url_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/sites/{id}/update", TEST_SITE_ID)
					.param("url", "")
					.param("description", ""))
			.andExpect(view().name("update/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Site")))
			.andExpect(content().string(containsString("Url must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Url must not be blank.")));
	}

	@Test
	void testProcessUpdateSiteById_description_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/sites/{id}/update", TEST_SITE_ID)
					.param("url", "")
					.param("description", ""))
			.andExpect(view().name("update/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Site")))
			.andExpect(content().string(containsString("Description must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Description must not be blank.")));
	}

}
