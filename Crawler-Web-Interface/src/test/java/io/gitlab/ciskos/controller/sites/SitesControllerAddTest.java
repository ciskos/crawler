package io.gitlab.ciskos.controller.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class SitesControllerAddTest extends AbstractSitesControllerTest {

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/sites/add"))
			.andExpect(view().name("/add/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Site")))
			.andExpect(content().string(containsString("Url:")))
			.andExpect(content().string(containsString("Description:")));

	}

	@Test
	void testProcessCreationForm_success() throws Exception {
		mockMvc.perform(post("/sites/add")
				.param("url", "siteUrl1")
				.param("description", "siteDescription1"))
			.andExpect(view().name("redirect:/sites"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/sites"));
	}
	
	@Test
	void testProcessCreationForm_url_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/sites/add")
					.param("url", "")
					.param("description", ""))
			.andExpect(view().name("add/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Site")))
			.andExpect(content().string(containsString("Url must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Url must not be blank.")));
	}
	
	@Test
	void testProcessCreationForm_description_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/sites/add")
					.param("url", "")
					.param("description", ""))
			.andExpect(view().name("add/site"))
			.andExpect(model().attributeExists("siteToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Site")))
			.andExpect(content().string(containsString("Description must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Description must not be blank.")));
	}

}
