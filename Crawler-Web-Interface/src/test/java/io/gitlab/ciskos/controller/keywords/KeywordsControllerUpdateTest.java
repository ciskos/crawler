package io.gitlab.ciskos.controller.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class KeywordsControllerUpdateTest extends AbstractKeywordsControllerTest {

	@Test
	void testInitUpdateKeywordById_success() throws Exception {
		mockMvc.perform(get("/keywords/{id}/update", TEST_KEYWORD_ID))
			.andExpect(view().name("update/keyword"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(model().attribute("keywordToUpdate", hasProperty("id", is(TEST_KEYWORD_ID))))
			.andExpect(model().attribute("keywordToUpdate", hasProperty("themeId")))
			.andExpect(model().attribute("keywordToUpdate", hasProperty("themeName")))
			.andExpect(model().attribute("keywordToUpdate", hasProperty("keyword")))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Keyword")))
			.andExpect(content().string(containsString("theme1")))
			.andExpect(content().string(containsString("keyword1")));
	}

	@Test
	void testInitUpdateKeywordById_fail() throws Exception {
		mockMvc.perform(get("/keywords/{id}/update", TEST_FAIL_KEYWORD_ID))
			.andExpect(view().name("redirect:/keywords"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/keywords"));
	}

	@Test
	void testProcessUpdateKeywordById_success() throws Exception {
		mockMvc.perform(post("/keywords/{id}/update", TEST_KEYWORD_ID)
				.param("themeId", "1")
				.param("keyword", "keyword1"))
			.andExpect(view().name("redirect:/keywords"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/keywords"));
	}

	@Test
	void testProcessUpdateKeywordById_themeId_must_not_be_null() throws Exception {
		mockMvc.perform(post("/keywords/{id}/update", TEST_KEYWORD_ID)
					.param("themeId", "")
					.param("keyword", ""))
			.andExpect(view().name("update/keyword"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Keyword")))
			.andExpect(content().string(containsString("Keyword must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Keyword must not be blank.")));
	}

	@Test
	void testProcessUpdateKeywordById_keyword_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/keywords/{id}/update", TEST_KEYWORD_ID)
					.param("themeId", "")
					.param("keyword", ""))
			.andExpect(view().name("update/keyword"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Keyword")))
			.andExpect(content().string(containsString("Theme must not be blank.")));
	}

}
