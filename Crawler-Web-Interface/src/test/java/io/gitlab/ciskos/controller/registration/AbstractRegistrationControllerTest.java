package io.gitlab.ciskos.controller.registration;

import static org.mockito.BDDMockito.given;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.RegistrationController;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = RegistrationController.class)
public abstract class AbstractRegistrationControllerTest {

	private static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.now();
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private UserService userService;

	@MockBean
	private AuthorityService authorityService;
	
	private User userToAdd;
	private User userSaved;
	
	@BeforeEach
	void setUp() throws Exception {
		userToAdd = new User();
		userSaved = new User();
		
		userToAdd.setId(null);
		userToAdd.setUsername("user1");
		userToAdd.setPassword("pass1");
		userToAdd.setEmail("mail1@email.org");
		userToAdd.setEnabled(true);
		userToAdd.setRegistered(TEST_LOCALDATETIME);

		userSaved.setId(1L);
		userSaved.setUsername("user1");
		userSaved.setPassword("pass1");
		userSaved.setEmail("mail1@email.org");
		userSaved.setEnabled(true);
		userSaved.setRegistered(TEST_LOCALDATETIME);
		
		given(this.userService.addUser(userToAdd)).willReturn(userSaved);
	}

}