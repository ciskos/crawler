package io.gitlab.ciskos.controller.matches;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class MatchesControllerTest extends AbstractMatchesControllerTest {

	@Test
	void testGetAllThemesPageMatches() throws Exception {
		mockMvc.perform(get("/matches"))
			.andExpect(view().name("get/matches"))
			.andExpect(model().attributeExists("matches"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Matches")))
			.andExpect(content().string(containsString("theme1")))
			.andExpect(content().string(containsString("theme2")));
	}

	@Test
	void testDeleteMatchById() throws Exception {
		mockMvc.perform(get("/matches/{id}/delete", TEST_MATCH_ID))
			.andExpect(view().name("redirect:/matches"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/matches"));
	}

}
