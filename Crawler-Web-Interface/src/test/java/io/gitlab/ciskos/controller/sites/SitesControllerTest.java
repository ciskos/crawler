package io.gitlab.ciskos.controller.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class SitesControllerTest extends AbstractSitesControllerTest {

	@Test
	void testGetAllSites() throws Exception {
		mockMvc.perform(get("/sites"))
			.andExpect(view().name("get/sites"))
			.andExpect(model().attributeExists("sites"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Sites")))
			.andExpect(content().string(containsString("siteUrl1")))
			.andExpect(content().string(containsString("siteUrl2")));
	}

	@Test
	void testDeleteSiteById() throws Exception {
		mockMvc.perform(get("/sites/{id}/delete", TEST_SITE_ID))
			.andExpect(view().name("redirect:/sites"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/sites"));
	}

}
