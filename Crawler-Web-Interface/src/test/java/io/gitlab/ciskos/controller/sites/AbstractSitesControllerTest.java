package io.gitlab.ciskos.controller.sites;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.SitesController;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = SitesController.class)
public abstract class AbstractSitesControllerTest {

	protected static final Long TEST_SITE_ID = 1L;
	protected static final Long TEST_FAIL_SITE_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private SiteService siteService;
	
	private Site site0;
	private Site site1;
	private Site site2;

	@BeforeEach
	void setUp() throws Exception {
		site0 = new Site();
		site1 = new Site();
		site2 = new Site();
		
		site1.setId(1L);
		site1.setUrl("siteUrl1");
		site1.setDescription("siteDescription1");
	
		site2.setId(2L);
		site2.setUrl("siteUrl2");
		site2.setDescription("siteDescription2");
		
		given(this.siteService.getAllSites()).willReturn(Lists.newArrayList(site1, site2));
		willDoNothing().given(this.siteService).deleteSiteById(1L);
		given(this.siteService.updateSite(site1)).willReturn(site1);
		given(this.siteService.getSiteById(0L)).willReturn(site0);
		given(this.siteService.getSiteById(1L)).willReturn(site1);
		given(this.siteService.addSite(site1)).willReturn(site1);
	}

}