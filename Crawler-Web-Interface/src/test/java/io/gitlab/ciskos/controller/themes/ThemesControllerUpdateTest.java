package io.gitlab.ciskos.controller.themes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class ThemesControllerUpdateTest extends AbstractThemesControllerTest {
	
	@Test
	void testInitUpdateThemeById_success() throws Exception {
		mockMvc.perform(get("/themes/{id}/update", TEST_THEME_ID))
			.andExpect(view().name("update/theme"))
			.andExpect(model().attributeExists("themeToUpdate"))
			.andExpect(model().attribute("themeToUpdate", hasProperty("id", is(TEST_THEME_ID))))
			.andExpect(model().attribute("themeToUpdate", hasProperty("name")))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Theme")))
			.andExpect(content().string(containsString("Name:")));
	}

	@Test
	void testInitUpdateThemeById_fail() throws Exception {
		mockMvc.perform(get("/themes/{id}/update", TEST_FAIL_THEME_ID))
			.andExpect(view().name("redirect:/themes"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/themes"));
	}

	@Test
	void testProcessUpdateThemeById_success() throws Exception {
		mockMvc.perform(post("/themes/{id}/update", TEST_THEME_ID)
				.param("name", "theme1"))
			.andExpect(view().name("redirect:/themes"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/themes"));
	}
	
	@Test
	void testProcessUpdateThemeById_theme_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/themes/{id}/update", TEST_THEME_ID)
					.param("name", ""))
			.andExpect(view().name("update/theme"))
			.andExpect(model().attributeExists("themeToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Theme")))
			.andExpect(content().string(containsString("Theme must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Theme must not be blank.")));
	}

}
