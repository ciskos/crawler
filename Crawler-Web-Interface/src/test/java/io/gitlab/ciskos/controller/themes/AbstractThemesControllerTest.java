package io.gitlab.ciskos.controller.themes;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.ThemesController;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = ThemesController.class)
public abstract class AbstractThemesControllerTest {

	protected static final Long TEST_THEME_ID = 1L;
	protected static final Long TEST_FAIL_THEME_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private ThemeService themeService;
	
	private Theme theme0;
	private Theme theme1;
	private Theme theme2;

	@BeforeEach
	void setUp() throws Exception {
		theme0 = new Theme();
		theme1 = new Theme();
		theme2 = new Theme();
		
		theme1.setId(1l);
		theme1.setName("theme1");
	
		theme2.setId(2l);
		theme2.setName("theme2");
		
		given(this.themeService.getAllThemes()).willReturn(Lists.newArrayList(theme1, theme2));
		willDoNothing().given(this.themeService).deleteThemeById(1L);
		given(this.themeService.updateTheme(theme1)).willReturn(theme1);
		given(this.themeService.getThemeById(0L)).willReturn(theme0);
		given(this.themeService.getThemeById(1L)).willReturn(theme1);
		given(this.themeService.addTheme(theme1)).willReturn(theme1);
	}

}