package io.gitlab.ciskos.controller.registration;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class RegistrationControllerTest extends AbstractRegistrationControllerTest {

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/registration"))
			.andExpect(view().name("/registration"))
			.andExpect(model().attributeExists("user"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Register")))
			.andExpect(content().string(containsString("Username:")))
			.andExpect(content().string(containsString("Password:")))
			.andExpect(content().string(containsString("E-mail:")));
	}

	@Test
	void testProcessCreationForm_success() throws Exception {
		mockMvc.perform(post("/registration")
				.param("username", "username1")
				.param("password", "password1")
				.param("email", "mail1@email.com"))
			.andExpect(view().name("redirect:/login"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/login"));
	}

	@Test
	void testProcessCreationForm_username_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/registration")
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("/registration"))
			.andExpect(model().attributeExists("user"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Register")))
			.andExpect(content().string(containsString("Username must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Username must not be blank.")));
	}
	
	@Test
	void testProcessCreationForm_password_must_be_8_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/registration")
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("/registration"))
			.andExpect(model().attributeExists("user"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Register")))
			.andExpect(content().string(containsString("Password must be minimum 8 and maximum 255.")))
			.andExpect(content().string(containsString("Password must not be blank.")));
	}
	
	@Test
	void testProcessCreationForm_email_must_be_8_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/registration")
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("/registration"))
			.andExpect(model().attributeExists("user"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Register")))
			.andExpect(content().string(containsString("E-mail must be minimum 8 and maximum 255.")))
			.andExpect(content().string(containsString("E-mail must not be blank.")));
	}
	
	@Test
	void testProcessCreationForm_email_must_be_email() throws Exception {
		mockMvc.perform(post("/registration")
					.param("username", "")
					.param("password", "")
					.param("email", "mail"))
			.andExpect(view().name("/registration"))
			.andExpect(model().attributeExists("user"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Register")))
			.andExpect(content().string(containsString("Must be e-mail.")));
	}

}
