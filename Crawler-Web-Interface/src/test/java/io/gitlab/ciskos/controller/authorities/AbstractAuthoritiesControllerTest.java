package io.gitlab.ciskos.controller.authorities;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.AuthoritiesController;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = AuthoritiesController.class)
public abstract class AbstractAuthoritiesControllerTest {

	protected static final Long TEST_USER_ID = 1L;
	protected static final Long TEST_FAIL_USER_ID = 0L;
	private static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.now();
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private AuthorityService authorityService;
	
	@MockBean
	private UserService userService;
	
	private Authority authority0;
	private Authority authority1;
	private Authority authority2;
	private User user1;
	private User user2;

	@BeforeEach
	void setUp() throws Exception {
		user1 = new User();
		user2 = new User();
		
		user1.setId(1L);
		user1.setUsername("user1");
		user1.setPassword("pass1");
		user1.setEmail("mail1@email.org");
		user1.setEnabled(true);
		user1.setRegistered(TEST_LOCALDATETIME);
		
		user2.setId(2L);
		user2.setUsername("user2");
		user2.setPassword("pass3");
		user2.setEmail("mail2@email.org");
		user2.setEnabled(true);
		user2.setRegistered(TEST_LOCALDATETIME);
		
		authority0 = new Authority();
		authority1 = new Authority();
		authority2 = new Authority();
		
		authority1.setId(1L);
		authority1.setUser(user1);
		authority1.setRole(Role.ROLE_ADMIN);
	
		authority2.setId(2L);
		authority2.setUser(user2);
		authority2.setRole(Role.ROLE_USER);
	
		given(this.authorityService.getAllAuthorities()).willReturn(Lists.newArrayList(authority1, authority2));
		willDoNothing().given(this.authorityService).deleteAuthorityById(1L);
		given(this.authorityService.updateAuthority(authority1)).willReturn(authority1);
		given(this.authorityService.getAuthorityById(0L)).willReturn(authority0);
		given(this.authorityService.getAuthorityById(1L)).willReturn(authority1);
		given(this.authorityService.addAuthority(authority1)).willReturn(authority1);
	}

}