package io.gitlab.ciskos.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = RootController.class)
class RootControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testRoot() throws Exception {
		mockMvc.perform(get("/"))
			.andExpect(view().name("root"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Home")));
	}

	@Test
	void testLogin() throws Exception {
		mockMvc.perform(get("/login"))
			.andExpect(view().name("login"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Username:")))
			.andExpect(content().string(containsString("Password:")));
	}

}
