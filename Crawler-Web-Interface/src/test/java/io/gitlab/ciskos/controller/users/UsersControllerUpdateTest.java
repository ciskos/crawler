package io.gitlab.ciskos.controller.users;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class UsersControllerUpdateTest extends AbstractUsersControllerTest {

	@Test
	void testInitUpdateUserById_success() throws Exception {
		mockMvc.perform(get("/users/{id}/update", TEST_USER_ID))
			.andExpect(view().name("update/user"))
			.andExpect(model().attributeExists("userToUpdate"))
			.andExpect(model().attribute("userToUpdate", hasProperty("id", is(TEST_USER_ID))))
			.andExpect(model().attribute("userToUpdate", hasProperty("username")))
			.andExpect(model().attribute("userToUpdate", hasProperty("password")))
			.andExpect(model().attribute("userToUpdate", hasProperty("email")))
			.andExpect(model().attribute("userToUpdate", hasProperty("enabled")))
			.andExpect(model().attribute("userToUpdate", hasProperty("registered")))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update User")))
			.andExpect(content().string(containsString("Username:")))
			.andExpect(content().string(containsString("Password:")))
			.andExpect(content().string(containsString("E-mail:")));
	}

	@Test
	void testInitUpdateUserById_fail() throws Exception {
		mockMvc.perform(get("/users/{id}/update", TEST_FAIL_USER_ID))
			.andExpect(view().name("redirect:/users"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/users"));
	}

	@Test
	void testProcessUpdateUserById_success() throws Exception {
		mockMvc.perform(post("/users/{id}/update", TEST_USER_ID)
					.param("username", "user3")
					.param("password", "password3")
					.param("email", "mail3@email.com"))
			.andExpect(view().name("redirect:/users"))
			.andExpect(status().is3xxRedirection());
	}

	@Test
	void testProcessUpdateUserById_username_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/users/{id}/update", TEST_USER_ID)
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("update/user"))
			.andExpect(model().attributeExists("userToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update User")))
			.andExpect(content().string(containsString("Username must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Username must not be blank.")));
	}
	
	@Test
	void testProcessUpdateUserById_password_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/users/{id}/update", TEST_USER_ID)
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("update/user"))
			.andExpect(model().attributeExists("userToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update User")))
			.andExpect(content().string(containsString("Password must be minimum 8 and maximum 255.")))
			.andExpect(content().string(containsString("Password must not be blank.")));
	}
	
	@Test
	void testProcessUpdateUserById_email_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/users/{id}/update", TEST_USER_ID)
					.param("username", "")
					.param("password", "")
					.param("email", ""))
			.andExpect(view().name("update/user"))
			.andExpect(model().attributeExists("userToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update User")))
			.andExpect(content().string(containsString("E-mail must be minimum 8 and maximum 255.")))
			.andExpect(content().string(containsString("E-mail must not be blank.")));
	}
	
	@Test
	void testProcessUpdateUserById_email_must_be_email() throws Exception {
		mockMvc.perform(post("/users/{id}/update", TEST_USER_ID)
					.param("username", "")
					.param("password", "")
					.param("email", "mail"))
			.andExpect(view().name("update/user"))
			.andExpect(model().attributeExists("userToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update User")))
			.andExpect(content().string(containsString("Must be e-mail.")));
	}

}
