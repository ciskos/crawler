package io.gitlab.ciskos.controller.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class KeywordsControllerTest extends AbstractKeywordsControllerTest {

	@Test
	void testGetAllKeywords() throws Exception {
		mockMvc.perform(get("/keywords"))
			.andExpect(view().name("get/keywords"))
			.andExpect(model().attributeExists("keywordToUpdate"))
			.andExpect(model().attributeExists("keywords"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Keywords")))
			.andExpect(content().string(containsString("keyword1")))
			.andExpect(content().string(containsString("keyword2")));
	}

	@Test
	void testDeleteKeywordById() throws Exception {
		mockMvc.perform(get("/keywords/{id}/delete", TEST_KEYWORD_ID))
			.andExpect(view().name("redirect:/keywords"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/keywords"));
	}

}
