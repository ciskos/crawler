package io.gitlab.ciskos.controller.pages;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class PagesControllerUpdateTest extends AbstractPagesControllerTest {
	
	@Test
	void testtInitUpdatePageById_success() throws Exception {
		mockMvc.perform(get("/pages/{id}/update", TEST_PAGE_ID))
			.andExpect(view().name("update/page"))
			.andExpect(model().attributeExists("pageToUpdate"))
			.andExpect(model().attribute("pageToUpdate", hasProperty("id", is(TEST_PAGE_ID))))
			.andExpect(model().attribute("pageToUpdate", hasProperty("siteId")))
			.andExpect(model().attribute("pageToUpdate", hasProperty("siteUrl")))
			.andExpect(model().attribute("pageToUpdate", hasProperty("pageUrl")))
			.andExpect(model().attribute("pageToUpdate", hasProperty("registered")))
			.andExpect(model().attribute("pageToUpdate", hasProperty("lastScan")))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Page")))
			.andExpect(content().string(containsString("Site:")))
			.andExpect(content().string(containsString("Url:")));
	}

	@Test
	void testInitUpdatePageById_fail() throws Exception {
		mockMvc.perform(get("/pages/{id}/update", TEST_FAIL_PAGE_ID))
			.andExpect(view().name("redirect:/pages"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/pages"));
	}

	@Test
	void testProcessUpdatePageById_success() throws Exception {
		mockMvc.perform(post("/pages/{id}/update", TEST_PAGE_ID)
				.param("siteId", "1")
				.param("pageUrl", "pageUrl1"))
			.andExpect(view().name("redirect:/pages"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/pages"));
	}

	@Test
	void testProcessUpdatePageById_siteId_must_not_be_null() throws Exception {
		mockMvc.perform(post("/pages/{id}/update", TEST_PAGE_ID)
					.param("siteId", "")
					.param("pageUrl", ""))
			.andExpect(view().name("update/page"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Page")))
			.andExpect(content().string(containsString("Site must not be blank.")));
	}

	@Test
	void testProcessUpdatePageById() throws Exception {
		mockMvc.perform(post("/pages/{id}/update", TEST_PAGE_ID)
					.param("siteId", "")
					.param("pageUrl", ""))
			.andExpect(view().name("update/page"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Update Page")))
			.andExpect(content().string(containsString("Url must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Url must not be blank.")));
	}

}
