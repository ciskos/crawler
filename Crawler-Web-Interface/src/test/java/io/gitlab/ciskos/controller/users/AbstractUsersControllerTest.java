package io.gitlab.ciskos.controller.users;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.UsersController;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = UsersController.class)
public abstract class AbstractUsersControllerTest {

	protected static final Long TEST_USER_ID = 1L;
	protected static final Long TEST_FAIL_USER_ID = 0L;
	private static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.now();
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private AuthorityService authorityService;
	
	private User user0;
	private User user1;
	private User user2;
	private Authority authority1;
	private Authority authority2;

	@BeforeEach
	void setUp() throws Exception {
		user0 = new User();
		user1 = new User();
		user2 = new User();
		
		user1.setId(1L);
		user1.setUsername("user1");
		user1.setPassword("pass1");
		user1.setEmail("mail1@email.org");
		user1.setEnabled(true);
		user1.setRegistered(TEST_LOCALDATETIME);
		
		user2.setId(2L);
		user2.setUsername("user2");
		user2.setPassword("pass3");
		user2.setEmail("mail2@email.org");
		user2.setEnabled(true);
		user2.setRegistered(TEST_LOCALDATETIME);
		
		authority1 = new Authority();
		authority2 = new Authority();
		
		authority1.setId(1L);
		authority1.setUser(user1);
		authority1.setRole(Role.ROLE_ADMIN);
	
		authority2.setId(2L);
		authority2.setUser(user2);
		authority2.setRole(Role.ROLE_USER);
	
		given(this.userService.getAllUsers()).willReturn(Lists.newArrayList(user1, user2));
		willDoNothing().given(this.userService).deleteUserById(1L);
		given(this.userService.updateUser(user1)).willReturn(user1);
		given(this.userService.getUserById(0L)).willReturn(user0);
		given(this.userService.getUserById(1L)).willReturn(user1);
		given(this.userService.addUser(user1)).willReturn(user1);
	}

}