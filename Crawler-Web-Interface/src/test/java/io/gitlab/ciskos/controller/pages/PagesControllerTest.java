package io.gitlab.ciskos.controller.pages;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class PagesControllerTest extends AbstractPagesControllerTest {
	
	@Test
	void testGetAllPages() throws Exception {
		mockMvc.perform(get("/pages"))
			.andExpect(view().name("get/pages"))
			.andExpect(model().attributeExists("pages"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Pages")))
			.andExpect(content().string(containsString("pageUrl1")))
			.andExpect(content().string(containsString("pageUrl2")));
	}

	@Test
	void testDeletePageById() throws Exception {
		mockMvc.perform(get("/pages/{id}/delete", TEST_PAGE_ID))
			.andExpect(view().name("redirect:/pages"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/pages"));
	}

}
