package io.gitlab.ciskos.controller.keywords;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.KeywordsController;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.KeywordService;
import io.gitlab.ciskos.service.ThemeService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = KeywordsController.class)
public abstract class AbstractKeywordsControllerTest {

	protected static final Long TEST_KEYWORD_ID = 1L;
	protected static final Long TEST_FAIL_KEYWORD_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private KeywordService keywordService;
	
	@MockBean
	private ThemeService themeService;
	
	private Keyword keyword0;
	private Keyword keyword1;
	private Keyword keyword2;
	private Theme theme1;
	private Theme theme2;

	@BeforeEach
	void setUp() throws Exception {
		theme1 = new Theme();
		theme2 = new Theme();
		
		theme1.setId(1L);
		theme1.setName("theme1");
	
		theme2.setId(2L);
		theme2.setName("theme2");
		
		keyword0 = new Keyword();
		keyword1 = new Keyword();
		keyword2 = new Keyword();
		
		keyword1.setId(1L);
		keyword1.setTheme(theme1);
		keyword1.setWord("keyword1");
	
		keyword2.setId(2L);
		keyword2.setTheme(theme2);
		keyword2.setWord("keyword2");
		
		given(this.themeService.getAllThemes()).willReturn(Lists.newArrayList(theme1, theme2));
		
		given(this.keywordService.getAllKeywords()).willReturn(Lists.newArrayList(keyword1, keyword2));
		willDoNothing().given(this.keywordService).deleteKeywordById(1L);
		given(this.keywordService.updateKeyword(keyword1)).willReturn(keyword1);
		given(this.keywordService.getKeywordById(0L)).willReturn(keyword0);
		given(this.keywordService.getKeywordById(1L)).willReturn(keyword1);
		given(this.keywordService.addKeyword(keyword1)).willReturn(keyword1);
	}

}