package io.gitlab.ciskos.controller.pages;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.controller.PagesController;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.PageService;
import io.gitlab.ciskos.service.SiteService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = PagesController.class)
public abstract class AbstractPagesControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_FAIL_PAGE_ID = 0L;
	private static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.now();
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private PageService pageService;
	
	@MockBean
	private SiteService siteService;
	
	private Page page0;
	private Page page1;
	private Page page2;
	private Site site1;
	private Site site2;

	@BeforeEach
	void setUp() throws Exception {
		site1 = new Site();
		site2 = new Site();
		
		site1.setId(1L);
		site1.setUrl("siteUrl1");
		site1.setDescription("siteDescription1");
	
		site2.setId(2L);
		site2.setUrl("siteUrl2");
		site2.setDescription("siteDescription2");
		
		page0 = new Page();
		page1 = new Page();
		page2 = new Page();
		
		page1.setId(1L);
		page1.setSite(site1);
		page1.setUrl("pageUrl1");
		page1.setRegistered(TEST_LOCALDATETIME);
		page1.setLastScan(TEST_LOCALDATETIME);
	
		page2.setId(2L);
		page2.setSite(site2);
		page2.setUrl("pageUrl2");
		page2.setRegistered(TEST_LOCALDATETIME);
		page2.setLastScan(TEST_LOCALDATETIME);
		
		given(this.pageService.getAllPages()).willReturn(Lists.newArrayList(page1, page2));
		willDoNothing().given(this.pageService).deletePageById(1L);
		given(this.pageService.updatePage(page1)).willReturn(page1);
		given(this.pageService.getPageById(0L)).willReturn(page0);
		given(this.pageService.getPageById(1L)).willReturn(page1);
		given(this.pageService.addPage(page1)).willReturn(page1);
	}

}