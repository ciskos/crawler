package io.gitlab.ciskos.controller.users;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class UsersControllerTest extends AbstractUsersControllerTest {

	@Test
	void testGetAllUsers() throws Exception {
		mockMvc.perform(get("/users"))
			.andExpect(view().name("get/users"))
			.andExpect(model().attributeExists("users"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Users")))
			.andExpect(content().string(containsString("user1")))
			.andExpect(content().string(containsString("user2")));
	}

	@Test
	void testDeleteUserById() throws Exception {
		mockMvc.perform(get("/users/{id}/delete", TEST_USER_ID))
			.andExpect(view().name("redirect:/users"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/users"));
	}

}
