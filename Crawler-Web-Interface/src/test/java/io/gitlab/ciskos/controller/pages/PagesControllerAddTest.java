package io.gitlab.ciskos.controller.pages;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;

class PagesControllerAddTest extends AbstractPagesControllerTest {

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/pages/add"))
			.andExpect(view().name("/add/page"))
			.andExpect(model().attributeExists("pageToUpdate"))
			.andExpect(model().attributeExists("sites"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Page")))
			.andExpect(content().string(containsString("Site:")))
			.andExpect(content().string(containsString("Url:")));
	}

	@Test
	void testProcessCreationForm_success() throws Exception {
		mockMvc.perform(post("/pages/add")
				.param("siteId", "1")
				.param("pageUrl", "pageUrl1"))
			.andExpect(view().name("redirect:/pages"))
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl("/pages"));
	}
	
	@Test
	void testProcessCreationForm_url_must_be_3_or_255_and_not_blank() throws Exception {
		mockMvc.perform(post("/pages/add")
					.param("siteId", "1")
					.param("pageUrl", ""))
			.andExpect(model().attributeExists("pageToUpdate"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString("Add Page")))
			.andExpect(content().string(containsString("Url must be minimum 3 and maximum 255.")))
			.andExpect(content().string(containsString("Url must not be blank.")));
	}

}
