package io.gitlab.ciskos.restController.users;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class UsersRestControllerTest extends AbstractUsersRestControllerTest {

	@Test
	void testGetAllUsers() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"_embedded\":{\"users\":[{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},{\"username\":\"user2\",\"password\":\"pass2\",\"email\":\"mail2@email.com\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}}]},\"_links\":{\"allUsers\":{\"href\":\"http://localhost/users\"}}}";
		
		mockMvc.perform(get("/users")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllUsersPage() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"_embedded\":{\"users\":[{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},{\"username\":\"user2\",\"password\":\"pass2\",\"email\":\"mail2@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}}]},\"_links\":{\"usersPage1\":{\"href\":\"http://localhost/users/page/1\"}}}";
		
		mockMvc.perform(get("/users/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetUserById() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}}";
		
		mockMvc.perform(get("/users/{id}", TEST_USER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteUserById() throws Exception {
		mockMvc.perform(delete("/users/{id}", TEST_USER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
