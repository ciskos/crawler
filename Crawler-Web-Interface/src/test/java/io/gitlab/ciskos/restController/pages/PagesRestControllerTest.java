package io.gitlab.ciskos.restController.pages;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class PagesRestControllerTest extends AbstractPagesRestControllerTest {
	
	@Test
	void testGetAllPages() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"_embedded\":{\"pages\":[{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}},{\"site\":{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}},\"url\":\"pageUrl2\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/2\"}}}]},\"_links\":{\"allPages\":{\"href\":\"http://localhost/pages\"}}}";
		
		mockMvc.perform(get("/pages")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllPagesPage() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"_embedded\":{\"pages\":[{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}},{\"site\":{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}},\"url\":\"pageUrl2\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/2\"}}}]},\"_links\":{\"pagesPage1\":{\"href\":\"http://localhost/matches/page/1\"}}}";
		
		mockMvc.perform(get("/pages/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetPageById() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}}";
		
		mockMvc.perform(get("/pages/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeletePageById() throws Exception {
		mockMvc.perform(delete("/pages/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
