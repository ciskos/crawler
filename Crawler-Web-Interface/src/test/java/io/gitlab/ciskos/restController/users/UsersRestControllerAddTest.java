package io.gitlab.ciskos.restController.users;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class UsersRestControllerAddTest extends AbstractUsersRestControllerTest {

	@Test
	void testAddUser() throws Exception {
		var content = "{\"id\":null,\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + "\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/4\"}}}";
		
		mockMvc.perform(post("/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
