package io.gitlab.ciskos.restController.users;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class UsersRestControllerUpdateTest extends AbstractUsersRestControllerTest {

	@Test
	void testPutUser() throws Exception {
		var content = "{\"id\":4,\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + "\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/4\"}}}";

		mockMvc.perform(put("/users/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchUser() throws Exception {
		var content = "{\"username\":\"user1\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.com\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/4\"}}}";

		mockMvc.perform(patch("/users/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
