package io.gitlab.ciskos.restController.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class KeywordsRestControllerUpdateTest extends AbstractKeywordsRestControllerTest {

	@Test
	void testPutKeyword() throws Exception {
		var content = "{\"id\":4,\"themeId\":1,\"themeName\":\"theme1\",\"keyword\":\"keywordPuted\"}";
		var result = "{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keywordPuted\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/4\"}}}";
		
		mockMvc.perform(put("/keywords/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchKeyword() throws Exception {
		var content = "{\"keyword\":\"keywordPatched\"}";
		var result = "{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keywordPatched\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/4\"}}}";
		
		mockMvc.perform(patch("/keywords/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)))
			.andDo(print())
			;
	}
	
}
