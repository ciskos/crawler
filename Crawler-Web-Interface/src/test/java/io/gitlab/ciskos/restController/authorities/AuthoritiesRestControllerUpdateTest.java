package io.gitlab.ciskos.restController.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class AuthoritiesRestControllerUpdateTest extends AbstractAuthoritiesRestControllerTest {
	
	@Test
	void testPutAuthority() throws Exception {
		var content = "{\"id\":4,\"userId\":3,\"username\":\"user2\",\"role\":\"ROLE_ADMIN\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"user\":{\"username\":\"user2\",\"password\":\"pass3\",\"email\":\"mail2@email.org\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/4\"}}}";

		mockMvc.perform(put("/authorities/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchAuthority() throws Exception {
		var content = "{\"role\":\"ROLE_ADMIN\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"user\":{\"username\":\"user2\",\"password\":\"pass3\",\"email\":\"mail2@email.org\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/4\"}}}";

		mockMvc.perform(patch("/authorities/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
