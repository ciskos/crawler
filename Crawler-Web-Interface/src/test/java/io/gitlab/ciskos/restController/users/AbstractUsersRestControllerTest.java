package io.gitlab.ciskos.restController.users;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.restController.UsersRestController;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = UsersRestController.class)
public abstract class AbstractUsersRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_USER_ID = 1L;
	protected static final Long TEST_FAIL_USER_ID = 0L;
	protected static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.of(2021, 05, 28, 12, 0);
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private AuthorityService authorityService;
	
	private User userToAdd;
	private User userAdded;
	private User userToPut;
	private User userPuted;
	private User userToPatch;
	private User userPatched;
	private User user1;
	private User user2;
	private Authority authority1;
	private Authority authority2;

	@BeforeEach
	void setUp() throws Exception {
		userToAdd = new User();
		userAdded = new User();
		userToPut = new User();
		userPuted = new User();
		userToPatch = new User();
		userPatched = new User();
		user1 = new User();
		user2 = new User();
		
		user1.setId(1L);
		user1.setUsername("user1");
		user1.setPassword("pass1");
		user1.setEmail("mail1@email.com");
		user1.setEnabled(true);
		user1.setRegistered(TEST_LOCALDATETIME);
		
		user2.setId(2L);
		user2.setUsername("user2");
		user2.setPassword("pass2");
		user2.setEmail("mail2@email.com");
		user2.setEnabled(true);
		user2.setRegistered(TEST_LOCALDATETIME);
		
		userToAdd.setId(null);
		userToAdd.setUsername("user1");
		userToAdd.setPassword("pass1");
		userToAdd.setEmail("mail1@email.com");
		userToAdd.setEnabled(true);
		userToAdd.setRegistered(TEST_LOCALDATETIME);
		
		userAdded.setId(4L);
		userAdded.setUsername("user1");
		userAdded.setPassword("pass1");
		userAdded.setEmail("mail1@email.com");
		userAdded.setEnabled(true);
		userAdded.setRegistered(TEST_LOCALDATETIME);
		
		userToPut.setId(4L);
		userToPut.setUsername("user1");
		userToPut.setPassword("pass1");
		userToPut.setEmail("mail1@email.com");
		userToPut.setEnabled(true);
		userToPut.setRegistered(TEST_LOCALDATETIME);
		
		userPuted.setId(4L);
		userPuted.setUsername("user1");
		userPuted.setPassword("pass1");
		userPuted.setEmail("mail1@email.com");
		userPuted.setEnabled(true);
		userPuted.setRegistered(TEST_LOCALDATETIME);
		
		userToPatch.setId(4L);
		userToPatch.setUsername("user1");
		userToPatch.setPassword("pass1");
		userToPatch.setEmail("mail1@email.com");
		userToPatch.setEnabled(true);
		userToPatch.setRegistered(TEST_LOCALDATETIME);
		
		userPatched.setId(4L);
		userPatched.setUsername("user1");
		userPatched.setPassword("pass1");
		userPatched.setEmail("mail1@email.com");
		userPatched.setEnabled(true);
		userPatched.setRegistered(TEST_LOCALDATETIME);
		
		authority1 = new Authority();
		authority2 = new Authority();
		
		authority1.setId(1L);
		authority1.setUser(user1);
		authority1.setRole(Role.ROLE_ADMIN);
	
		authority2.setId(2L);
		authority2.setUser(user2);
		authority2.setRole(Role.ROLE_USER);

		given(this.authorityService.getAuthorityById(1L)).willReturn(authority1);
		
		given(this.userService.getAllUsers()).willReturn(Lists.newArrayList(user1, user2));
		given(this.userService.getAllUsersPage(1)).willReturn(Lists.newArrayList(user1, user2));
		willDoNothing().given(this.userService).deleteUserById(1L);
		given(this.userService.getUserById(1L)).willReturn(user1);
		given(this.userService.getUserById(4L)).willReturn(userPatched);
		given(this.userService.addUser(userToAdd)).willReturn(userAdded);
		given(this.userService.addUser(userToPut)).willReturn(userPuted);
		given(this.userService.addUser(userToPatch)).willReturn(userPatched);
	}

}