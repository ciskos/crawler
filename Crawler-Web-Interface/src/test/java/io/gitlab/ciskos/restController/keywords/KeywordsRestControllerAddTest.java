package io.gitlab.ciskos.restController.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class KeywordsRestControllerAddTest extends AbstractKeywordsRestControllerTest {

	@Test
	void testAddKeyword() throws Exception {
		var content = "{\"id\":null,\"themeId\":1,\"themeName\":\"theme1\",\"keyword\":\"keywordAdd\"}";
		var result = "{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keywordAdd\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/4\"}}}";
		
		mockMvc.perform(post("/keywords")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
