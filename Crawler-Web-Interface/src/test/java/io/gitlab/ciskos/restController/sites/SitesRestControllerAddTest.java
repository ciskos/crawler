package io.gitlab.ciskos.restController.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class SitesRestControllerAddTest extends AbstractSitesRestControllerTest {

	@Test
	void testAddSite() throws Exception {
		var content = "{\"id\":null,\"url\":\"siteUrl1\",\"description\":\"siteDescription1\"}";
		var result = "{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/4\"}}}";
		
		mockMvc.perform(post("/sites")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
