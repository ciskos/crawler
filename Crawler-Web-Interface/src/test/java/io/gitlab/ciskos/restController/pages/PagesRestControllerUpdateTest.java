package io.gitlab.ciskos.restController.pages;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class PagesRestControllerUpdateTest extends AbstractPagesRestControllerTest {
	
	@Test
	void testPutPage() throws Exception {
		var content = "{\"id\":3,\"siteId\":1,\"siteUrl\":\"siteUrl1\",\"pageUrl\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + "\",\"lastScan\":\"" + TEST_LOCALDATETIME + "\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/3\"}}}";

		mockMvc.perform(put("/pages/{id}", 3L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchPage() throws Exception {
		var content = "{\"pageUrl\":\"pageUrl1\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"lastScan\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/pages/3\"}}}";

		mockMvc.perform(patch("/pages/{id}", 3L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
