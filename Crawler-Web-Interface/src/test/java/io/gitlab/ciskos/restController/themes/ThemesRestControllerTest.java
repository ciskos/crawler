package io.gitlab.ciskos.restController.themes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class ThemesRestControllerTest extends AbstractThemesRestControllerTest {
	
	@Test
	void testGetAllThemes() throws Exception {
		var result = "{\"_embedded\":{\"themes\":[{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}}]},\"_links\":{\"allThemes\":{\"href\":\"http://localhost/themes\"}}}";
		
		mockMvc.perform(get("/themes")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllThemesPage() throws Exception {
		var result = "{\"_embedded\":{\"themes\":[{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}}]},\"_links\":{\"themesPage1\":{\"href\":\"http://localhost/themes/page/1\"}}}";
		
		mockMvc.perform(get("/themes/page/{id}", TEST_THEME_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetThemeById() throws Exception {
		var result = "{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}}";
		
		mockMvc.perform(get("/themes/{id}", TEST_THEME_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteThemeById() throws Exception {
		mockMvc.perform(delete("/themes/{id}", TEST_THEME_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
