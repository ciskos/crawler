package io.gitlab.ciskos.restController.matches;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class MatchesRestControllerTest extends AbstractMatchesRestControllerTest {

	@Test
	void testGetAllMatches() throws Exception {
		var result = "{\"_embedded\":{\"matches\":[{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"page\":{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":null,\"lastScan\":null,\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}},\"counts\":1,\"_links\":{\"self\":{\"href\":\"http://localhost/matches/1\"}}},{\"theme\":{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}},\"page\":{\"site\":{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}},\"url\":\"pageUrl2\",\"registered\":null,\"lastScan\":null,\"_links\":{\"self\":{\"href\":\"http://localhost/pages/2\"}}},\"counts\":2,\"_links\":{\"self\":{\"href\":\"http://localhost/matches/2\"}}}]},\"_links\":{\"allMatches\":{\"href\":\"http://localhost/matches\"}}}";
		
		mockMvc.perform(get("/matches")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllMatchesPage() throws Exception {
		var result = "{\"_embedded\":{\"matches\":[{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"page\":{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":null,\"lastScan\":null,\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}},\"counts\":1,\"_links\":{\"self\":{\"href\":\"http://localhost/matches/1\"}}},{\"theme\":{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}},\"page\":{\"site\":{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}},\"url\":\"pageUrl2\",\"registered\":null,\"lastScan\":null,\"_links\":{\"self\":{\"href\":\"http://localhost/pages/2\"}}},\"counts\":2,\"_links\":{\"self\":{\"href\":\"http://localhost/matches/2\"}}}]},\"_links\":{\"matchesPage1\":{\"href\":\"http://localhost/matches/page/1\"}}}";
		
		mockMvc.perform(get("/matches/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetMatchById() throws Exception {
		var result = "{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"page\":{\"site\":{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},\"url\":\"pageUrl1\",\"registered\":null,\"lastScan\":null,\"_links\":{\"self\":{\"href\":\"http://localhost/pages/1\"}}},\"counts\":1,\"_links\":{\"self\":{\"href\":\"http://localhost/matches/1\"}}}";
		
		mockMvc.perform(get("/matches/{id}", TEST_MATCH_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteMatchById() throws Exception {
		mockMvc.perform(delete("/matches/{id}", TEST_MATCH_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
