package io.gitlab.ciskos.restController.sites;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.restController.SitesRestController;
import io.gitlab.ciskos.service.SiteService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = SitesRestController.class)
public abstract class AbstractSitesRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_SITE_ID = 1L;
	protected static final Long TEST_FAIL_SITE_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private SiteService siteService;
	
	private Site siteToAdd;
	private Site siteAdded;
	private Site siteToPut;
	private Site sitePuted;
	private Site siteToPatch;
	private Site sitePatched;
	private Site site1;
	private Site site2;

	@BeforeEach
	void setUp() throws Exception {
		siteToAdd = new Site();
		siteAdded = new Site();
		siteToPut = new Site();
		sitePuted = new Site();
		siteToPatch = new Site();
		sitePatched = new Site();
		site1 = new Site();
		site2 = new Site();
		
		site1.setId(1L);
		site1.setUrl("siteUrl1");
		site1.setDescription("siteDescription1");
	
		site2.setId(2L);
		site2.setUrl("siteUrl2");
		site2.setDescription("siteDescription2");
		
		siteToAdd.setId(null);
		siteToAdd.setUrl("siteUrl1");
		siteToAdd.setDescription("siteDescription1");
		
		siteAdded.setId(4L);
		siteAdded.setUrl("siteUrl1");
		siteAdded.setDescription("siteDescription1");
		
		siteToPut.setId(4L);
		siteToPut.setUrl("siteUrl1");
		siteToPut.setDescription("siteDescription1");
		
		sitePuted.setId(4L);
		sitePuted.setUrl("siteUrl1");
		sitePuted.setDescription("siteDescription1");
		
		siteToPatch.setId(4L);
		siteToPatch.setUrl("siteUrl1");
		siteToPatch.setDescription("siteDescription1");
		
		sitePatched.setId(4L);
		sitePatched.setUrl("siteUrl1");
		sitePatched.setDescription("siteDescription1");
		
		given(this.siteService.getAllSites()).willReturn(Lists.newArrayList(site1, site2));
		given(this.siteService.getAllSitesPage(1)).willReturn(Lists.newArrayList(site1, site2));
		willDoNothing().given(this.siteService).deleteSiteById(1L);
		given(this.siteService.getSiteById(1L)).willReturn(site1);
		given(this.siteService.getSiteById(4L)).willReturn(sitePatched);
		given(this.siteService.addSite(siteToAdd)).willReturn(siteAdded);
		given(this.siteService.addSite(siteToPut)).willReturn(sitePuted);
		given(this.siteService.addSite(siteToPatch)).willReturn(sitePatched);
	}

}