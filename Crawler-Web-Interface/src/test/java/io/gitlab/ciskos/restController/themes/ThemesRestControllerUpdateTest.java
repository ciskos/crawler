package io.gitlab.ciskos.restController.themes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class ThemesRestControllerUpdateTest extends AbstractThemesRestControllerTest {
	
	@Test
	void testPutTheme() throws Exception {
		var content = "{\"id\":4,\"name\":\"theme1\"}";
		var result = "{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/4\"}}}";

		mockMvc.perform(put("/themes/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchTheme() throws Exception {
		var content = "{\"name\":\"theme1\"}";
		var result = "{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/4\"}}}";

		mockMvc.perform(patch("/themes/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
