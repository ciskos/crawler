package io.gitlab.ciskos.restController.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class SitesRestControllerTest extends AbstractSitesRestControllerTest {

	@Test
	void testGetAllSites() throws Exception {
		var result = "{\"_embedded\":{\"sites\":[{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}}]},\"_links\":{\"allSites\":{\"href\":\"http://localhost/sites\"}}}";
		
		mockMvc.perform(get("/sites")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllSitesPage() throws Exception {
		var result = "{\"_embedded\":{\"sites\":[{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}},{\"url\":\"siteUrl2\",\"description\":\"siteDescription2\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/2\"}}}]},\"_links\":{\"sitesPage1\":{\"href\":\"http://localhost/sites/page/1\"}}}";
		
		mockMvc.perform(get("/sites/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetSiteById() throws Exception {
		var result = "{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/1\"}}}";
		
		mockMvc.perform(get("/sites/{id}", TEST_SITE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteSiteById() throws Exception {
		mockMvc.perform(delete("/sites/{id}", TEST_SITE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
