package io.gitlab.ciskos.restController.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class AuthoritiesRestControllerAddTest extends AbstractAuthoritiesRestControllerTest {

	@Test
	void testAddAuthority() throws Exception {
		var content = "{\"id\":null,\"userId\":2,\"username\":\"user1\",\"role\":\"ROLE_ADMIN\"}";
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"user\":{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.org\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/5\"}}}";
		
		mockMvc.perform(post("/authorities")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
