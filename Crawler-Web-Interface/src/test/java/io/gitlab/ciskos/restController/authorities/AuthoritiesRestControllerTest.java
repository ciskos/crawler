package io.gitlab.ciskos.restController.authorities;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class AuthoritiesRestControllerTest extends AbstractAuthoritiesRestControllerTest {

	@Test
	void testGetAllAuthorities() throws Exception {
		var result = "{\"_embedded\":{\"authorities\":[{\"user\":{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.org\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/1\"}}},{\"user\":{\"username\":\"user2\",\"password\":\"pass3\",\"email\":\"mail2@email.org\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}},\"role\":\"ROLE_USER\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/2\"}}}]},\"_links\":{\"allAuthorities\":{\"href\":\"http://localhost/authorities\"}}}";
		
		mockMvc.perform(get("/authorities")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	void testGetAllAuthoritiesPage() throws Exception {
		var result = "{\"_embedded\":{\"authorities\":[{\"user\":{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.org\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/1\"}}},{\"user\":{\"username\":\"user2\",\"password\":\"pass3\",\"email\":\"mail2@email.org\",\"enabled\":true,\"registered\":\"2021-05-28T12:00:00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/2\"}}},\"role\":\"ROLE_USER\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/2\"}}}]},\"_links\":{\"authoritiesPage1\":{\"href\":\"http://localhost/authorities/page/1\"}}}";
		
		mockMvc.perform(get("/authorities/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAuthorityById() throws Exception {
		// :00 в полях registered и lastScan нужны. Без них не проходит тест
		var result = "{\"user\":{\"username\":\"user1\",\"password\":\"pass1\",\"email\":\"mail1@email.org\",\"enabled\":true,\"registered\":\"" + TEST_LOCALDATETIME + ":00\",\"_links\":{\"self\":{\"href\":\"http://localhost/users/1\"}}},\"role\":\"ROLE_ADMIN\",\"_links\":{\"self\":{\"href\":\"http://localhost/authorities/1\"}}}";
		
		mockMvc.perform(get("/authorities/{id}", TEST_USER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteAuthorityById() throws Exception {
		mockMvc.perform(delete("/authorities/{id}", TEST_USER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
