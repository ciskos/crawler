package io.gitlab.ciskos.restController.themes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class ThemesRestControllerAddTest extends AbstractThemesRestControllerTest {

	@Test
	void testAddTheme() throws Exception {
		var content = "{\"id\":null,\"name\":\"theme1\"}";
		var result = "{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/4\"}}}";
		
		mockMvc.perform(post("/themes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
