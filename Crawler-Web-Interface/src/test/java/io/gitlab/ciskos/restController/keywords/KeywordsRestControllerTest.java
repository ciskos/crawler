package io.gitlab.ciskos.restController.keywords;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class KeywordsRestControllerTest extends AbstractKeywordsRestControllerTest {

	@Test
	void testGetAllKeywords() throws Exception {
		var result = "{\"_embedded\":{\"keywords\":[{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keyword1\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/1\"}}},{\"theme\":{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}},\"word\":\"keyword2\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/2\"}}}]},\"_links\":{\"allKeywords\":{\"href\":\"http://localhost/keywords\"}}}";
		
		mockMvc.perform(get("/keywords")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetAllKeywordsPage() throws Exception {
		var result = "{\"_embedded\":{\"keywords\":[{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keyword1\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/1\"}}},{\"theme\":{\"name\":\"theme2\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/2\"}}},\"word\":\"keyword2\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/2\"}}}]},\"_links\":{\"keywordsPage1\":{\"href\":\"http://localhost/keywords/page/1\"}}}";
		
		mockMvc.perform(get("/keywords/page/{id}", TEST_PAGE_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testGetKeywordById() throws Exception {
		var result = "{\"theme\":{\"name\":\"theme1\",\"_links\":{\"self\":{\"href\":\"http://localhost/themes/1\"}}},\"word\":\"keyword1\",\"_links\":{\"self\":{\"href\":\"http://localhost/keywords/1\"}}}";
		
		mockMvc.perform(get("/keywords/{id}", TEST_KEYWORD_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	void testDeleteKeywordById() throws Exception {
		mockMvc.perform(delete("/keywords/{id}", TEST_KEYWORD_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}

}
