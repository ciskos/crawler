package io.gitlab.ciskos.restController.pages;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.restController.PagesRestController;
import io.gitlab.ciskos.service.PageService;
import io.gitlab.ciskos.service.SiteService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = PagesRestController.class)
public abstract class AbstractPagesRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_FAIL_PAGE_ID = 0L;
	protected static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.of(2021, 05, 27, 12, 00);
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private PageService pageService;
	
	@MockBean
	private SiteService siteService;
	
	private Page pageToAdd;
	private Page pageAdded;
	private Page pageToPut;
	private Page pagePuted;
	private Page pageToPatch;
	private Page pagePatched;
	private Page page1;
	private Page page2;
	private Site site1;
	private Site site2;

	@BeforeEach
	void setUp() throws Exception {
		site1 = new Site();
		site2 = new Site();
		
		site1.setId(1L);
		site1.setUrl("siteUrl1");
		site1.setDescription("siteDescription1");
	
		site2.setId(2L);
		site2.setUrl("siteUrl2");
		site2.setDescription("siteDescription2");
		
		pageToAdd = new Page();
		pageAdded = new Page();
		pageToPut = new Page();
		pagePuted = new Page();
		pageToPatch = new Page();
		pagePatched = new Page();
		page1 = new Page();
		page2 = new Page();
		
		page1.setId(1L);
		page1.setSite(site1);
		page1.setUrl("pageUrl1");
		page1.setRegistered(TEST_LOCALDATETIME);
		page1.setLastScan(TEST_LOCALDATETIME);
	
		page2.setId(2L);
		page2.setSite(site2);
		page2.setUrl("pageUrl2");
		page2.setRegistered(TEST_LOCALDATETIME);
		page2.setLastScan(TEST_LOCALDATETIME);
		
		pageToAdd.setId(null);
		pageToAdd.setSite(site1);
		pageToAdd.setUrl("pageUrl1");
		pageToAdd.setRegistered(TEST_LOCALDATETIME);
		pageToAdd.setLastScan(TEST_LOCALDATETIME);
		
		pageAdded.setId(4L);
		pageAdded.setSite(site1);
		pageAdded.setUrl("pageUrl1");
		pageAdded.setRegistered(TEST_LOCALDATETIME);
		pageAdded.setLastScan(TEST_LOCALDATETIME);
		
		pageToPut.setId(3L);
		pageToPut.setSite(site1);
		pageToPut.setUrl("pageUrl1");
		pageToPut.setRegistered(TEST_LOCALDATETIME);
		pageToPut.setLastScan(TEST_LOCALDATETIME);
		
		pagePuted.setId(3L);
		pagePuted.setSite(site1);
		pagePuted.setUrl("pageUrl1");
		pagePuted.setRegistered(TEST_LOCALDATETIME);
		pagePuted.setLastScan(TEST_LOCALDATETIME);
		
		pageToPatch.setId(3L);
		pageToPatch.setSite(site1);
		pageToPatch.setUrl("pageUrl1");
		pageToPatch.setRegistered(TEST_LOCALDATETIME);
		pageToPatch.setLastScan(TEST_LOCALDATETIME);
		
		pagePatched.setId(3L);
		pagePatched.setSite(site1);
		pagePatched.setUrl("pageUrl1");
		pagePatched.setRegistered(TEST_LOCALDATETIME);
		pagePatched.setLastScan(TEST_LOCALDATETIME);
		
		given(this.siteService.getSiteById(1L)).willReturn(site1);
		
		given(this.pageService.getAllPages()).willReturn(Lists.newArrayList(page1, page2));
		given(this.pageService.getAllPagesPage(1)).willReturn(Lists.newArrayList(page1, page2));
		willDoNothing().given(this.pageService).deletePageById(1L);
		given(this.pageService.getPageById(1L)).willReturn(page1);
		given(this.pageService.getPageById(3L)).willReturn(pagePatched);
		given(this.pageService.addPage(pageToAdd)).willReturn(pageAdded);
		given(this.pageService.addPage(pageToPut)).willReturn(pagePuted);
		given(this.pageService.addPage(pageToPatch)).willReturn(pagePatched);
	}

}