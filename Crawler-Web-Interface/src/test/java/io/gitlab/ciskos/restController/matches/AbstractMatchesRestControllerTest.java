package io.gitlab.ciskos.restController.matches;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.restController.MatchesRestController;
import io.gitlab.ciskos.service.MatchService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = MatchesRestController.class)
public abstract class AbstractMatchesRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_MATCH_ID = 1L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private MatchService matchService;
	
	private Match match1;
	private Match match2;
	private Theme theme1;
	private Theme theme2;
	private Site site1;
	private Site site2;
	private Page page1;
	private Page page2;
	
	@BeforeEach
	void setUp() throws Exception {
		theme1 = new Theme();
		theme2 = new Theme();
		
		theme1.setId(1L);
		theme1.setName("theme1");
		     
		theme2.setId(2L);
		theme2.setName("theme2");

		site1 = new Site();
		site2 = new Site();
		
		site1.setId(1L);
		site1.setUrl("siteUrl1");
		site1.setDescription("siteDescription1");

		site2.setId(2L);
		site2.setUrl("siteUrl2");
		site2.setDescription("siteDescription2");
		
		page1 = new Page();
		page2 = new Page();
		
		page1.setId(1L);
		page1.setSite(site1);
		page1.setUrl("pageUrl1");
		page1.setRegistered(LocalDateTime.now());
		page1.setLastScan(LocalDateTime.now());

		page2.setId(2L);
		page2.setSite(site2);
		page2.setUrl("pageUrl2");
		page2.setRegistered(LocalDateTime.now());
		page2.setLastScan(LocalDateTime.now());
		
		match1 = new Match();
		match2 = new Match();
		
		match1.setId(1L);
		match1.setTheme(theme1);
		match1.setPage(page1);
		match1.setCounts(1L);

		match2.setId(2L);
		match2.setTheme(theme2);
		match2.setPage(page2);
		match2.setCounts(2L);
		
		given(this.matchService.getAllMatches()).willReturn(Lists.newArrayList(match1, match2));
		given(this.matchService.getAllMatchesPage(1)).willReturn(Lists.newArrayList(match1, match2));
		willDoNothing().given(this.matchService).deleteMatchById(1L);
		given(this.matchService.getMatchById(1L)).willReturn(match1);
	}

}