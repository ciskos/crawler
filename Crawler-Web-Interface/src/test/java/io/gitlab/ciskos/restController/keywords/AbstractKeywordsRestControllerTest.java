package io.gitlab.ciskos.restController.keywords;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.restController.KeywordsRestController;
import io.gitlab.ciskos.service.KeywordService;
import io.gitlab.ciskos.service.ThemeService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = KeywordsRestController.class)
public abstract class AbstractKeywordsRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_KEYWORD_ID = 1L;
	protected static final Long TEST_FAIL_KEYWORD_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private KeywordService keywordService;
	
	@MockBean
	private ThemeService themeService;
	
	private Keyword keywordToAdd;
	private Keyword keywordAdded;
	private Keyword keywordToPut;
	private Keyword keywordPuted;
	private Keyword keywordToPatch;
	private Keyword keywordPatched;
	private Keyword keyword1;
	private Keyword keyword2;
	private Theme theme1;
	private Theme theme2;

	@BeforeEach
	void setUp() throws Exception {
		theme1 = new Theme();
		theme2 = new Theme();
		
		theme1.setId(1L);
		theme1.setName("theme1");
	
		theme2.setId(2L);
		theme2.setName("theme2");
		
		keywordToAdd = new Keyword();
		keywordAdded = new Keyword();
		keywordToPut = new Keyword();
		keywordPuted = new Keyword();
		keywordToPatch = new Keyword();
		keywordPatched = new Keyword();
		keyword1 = new Keyword();
		keyword2 = new Keyword();
		
		keyword1.setId(1L);
		keyword1.setTheme(theme1);
		keyword1.setWord("keyword1");
	
		keyword2.setId(2L);
		keyword2.setTheme(theme2);
		keyword2.setWord("keyword2");
		
		keywordToAdd.setId(null);
		keywordToAdd.setTheme(theme1);
		keywordToAdd.setWord("keywordAdd");
		
		keywordAdded.setId(4L);
		keywordAdded.setTheme(theme1);
		keywordAdded.setWord("keywordAdd");
		
		keywordToPut.setId(4L);
		keywordToPut.setTheme(theme1);
		keywordToPut.setWord("keywordPuted");
		
		keywordPuted.setId(4L);
		keywordPuted.setTheme(theme1);
		keywordPuted.setWord("keywordPuted");
		
		keywordToPatch.setId(4L);
		keywordToPatch.setTheme(theme1);
		keywordToPatch.setWord("keywordPatched");
		
		keywordPatched.setId(4L);
		keywordPatched.setTheme(theme1);
		keywordPatched.setWord("keywordPatched");
		
		given(this.themeService.getThemeById(1L)).willReturn(theme1);
		
		given(this.keywordService.getAllKeywords()).willReturn(Lists.newArrayList(keyword1, keyword2));
		given(this.keywordService.getAllKeywordsPage(1)).willReturn(Lists.newArrayList(keyword1, keyword2));
		willDoNothing().given(this.keywordService).deleteKeywordById(1L);
		given(this.keywordService.getKeywordById(1L)).willReturn(keyword1);
		given(this.keywordService.getKeywordById(4L)).willReturn(keywordPatched);
		given(this.keywordService.addKeyword(keywordToAdd)).willReturn(keywordAdded);
		given(this.keywordService.addKeyword(keywordToPut)).willReturn(keywordPuted);
		given(this.keywordService.addKeyword(keywordToPatch)).willReturn(keywordPatched);
	}

}