package io.gitlab.ciskos.restController.sites;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

class SitesRestControllerUpdateTest extends AbstractSitesRestControllerTest {

	@Test
	void testPutSite() throws Exception {
		var content = "{\"id\":4,\"url\":\"siteUrl1\",\"description\":\"siteDescription1\"}";
		var result = "{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/4\"}}}";

		mockMvc.perform(put("/sites/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	void testPatchSite() throws Exception {
		var content = "{\"url\":\"siteUrl1\"}";
		var result = "{\"url\":\"siteUrl1\",\"description\":\"siteDescription1\",\"_links\":{\"self\":{\"href\":\"http://localhost/sites/4\"}}}";

		mockMvc.perform(patch("/sites/{id}", 4L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
