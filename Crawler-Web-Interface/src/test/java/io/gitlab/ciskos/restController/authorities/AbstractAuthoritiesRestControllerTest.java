package io.gitlab.ciskos.restController.authorities;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import java.time.LocalDateTime;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.restController.AuthoritiesRestController;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = AuthoritiesRestController.class)
public abstract class AbstractAuthoritiesRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_USER_ID = 1L;
	protected static final Long TEST_FAIL_USER_ID = 0L;
	protected static final LocalDateTime TEST_LOCALDATETIME = LocalDateTime.of(2021, 05, 28, 12, 0);
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private AuthorityService authorityService;
	
	@MockBean
	private UserService userService;
	
	private Authority authorityToAdd;
	private Authority authorityAdded;
	private Authority authorityToPut;
	private Authority authorityPuted;
	private Authority authorityToPatch;
	private Authority authorityPatched;
	private Authority authority1;
	private Authority authority2;
	private User user1;
	private User user2;

	@BeforeEach
	void setUp() throws Exception {
		user1 = new User();
		user2 = new User();
		
		user1.setId(1L);
		user1.setUsername("user1");
		user1.setPassword("pass1");
		user1.setEmail("mail1@email.org");
		user1.setEnabled(true);
		user1.setRegistered(TEST_LOCALDATETIME);
		
		user2.setId(2L);
		user2.setUsername("user2");
		user2.setPassword("pass3");
		user2.setEmail("mail2@email.org");
		user2.setEnabled(true);
		user2.setRegistered(TEST_LOCALDATETIME);

		authorityToAdd = new Authority();
		authorityAdded = new Authority();
		authorityToPut = new Authority();
		authorityPuted = new Authority();
		authorityToPatch = new Authority();
		authorityPatched = new Authority();
		authority1 = new Authority();
		authority2 = new Authority();
		
		authority1.setId(1L);
		authority1.setUser(user1);
		authority1.setRole(Role.ROLE_ADMIN);
	
		authority2.setId(2L);
		authority2.setUser(user2);
		authority2.setRole(Role.ROLE_USER);

		authorityToAdd.setId(null);
		authorityToAdd.setUser(user1);
		authorityToAdd.setRole(Role.ROLE_ADMIN);

		authorityAdded.setId(5L);
		authorityAdded.setUser(user1);
		authorityAdded.setRole(Role.ROLE_ADMIN);
		
		authorityToPut.setId(4L);
		authorityToPut.setUser(user2);
		authorityToPut.setRole(Role.ROLE_ADMIN);
		
		authorityPuted.setId(4L);
		authorityPuted.setUser(user2);
		authorityPuted.setRole(Role.ROLE_ADMIN);
		
		authorityToPatch.setId(4L);
		authorityToPatch.setUser(user2);
		authorityToPatch.setRole(Role.ROLE_ADMIN);
		
		authorityPatched.setId(4L);
		authorityPatched.setUser(user2);
		authorityPatched.setRole(Role.ROLE_ADMIN);

		given(this.userService.getUserById(2L)).willReturn(user1);
		given(this.userService.getUserById(3L)).willReturn(user2);
		
		given(this.authorityService.getAllAuthorities()).willReturn(Lists.newArrayList(authority1, authority2));
		given(this.authorityService.getAllAuthoritiesPage(1)).willReturn(Lists.newArrayList(authority1, authority2));
		willDoNothing().given(this.authorityService).deleteAuthorityById(1L);
		given(this.authorityService.getAuthorityById(1L)).willReturn(authority1);
		given(this.authorityService.getAuthorityById(4L)).willReturn(authorityPatched);
		given(this.authorityService.addAuthority(authorityToAdd)).willReturn(authorityAdded);
		given(this.authorityService.addAuthority(authorityToPut)).willReturn(authorityPuted);
		given(this.authorityService.addAuthority(authorityToPatch)).willReturn(authorityPatched);
	}

}