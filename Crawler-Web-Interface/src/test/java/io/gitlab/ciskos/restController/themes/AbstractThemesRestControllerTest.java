package io.gitlab.ciskos.restController.themes;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.restController.ThemesRestController;
import io.gitlab.ciskos.service.ThemeService;

@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
@WebMvcTest(controllers = ThemesRestController.class)
public abstract class AbstractThemesRestControllerTest {

	protected static final Long TEST_PAGE_ID = 1L;
	protected static final Long TEST_THEME_ID = 1L;
	protected static final Long TEST_FAIL_THEME_ID = 0L;
	
	@Autowired
	protected MockMvc mockMvc;
	
	@MockBean
	private ThemeService themeService;
	
	private Theme themeToAdd;
	private Theme themeAdded;
	private Theme themeToPut;
	private Theme themePuted;
	private Theme themeToPatch;
	private Theme themePatched;
	private Theme theme1;
	private Theme theme2;

	@BeforeEach
	void setUp() throws Exception {
		themeToAdd = new Theme();
		themeAdded = new Theme();
		themeToPut = new Theme();
		themePuted = new Theme();
		themeToPatch = new Theme();
		themePatched = new Theme();
		theme1 = new Theme();
		theme2 = new Theme();
		
		theme1.setId(1L);
		theme1.setName("theme1");
	
		theme2.setId(2L);
		theme2.setName("theme2");
		
		themeToAdd.setId(null);
		themeToAdd.setName("theme1");
		
		themeAdded.setId(4L);
		themeAdded.setName("theme1");
		
		themeToPut.setId(4L);
		themeToPut.setName("theme1");
		
		themePuted.setId(4L);
		themePuted.setName("theme1");
		
		themeToPatch.setId(4L);
		themeToPatch.setName("theme1");
		
		themePatched.setId(4L);
		themePatched.setName("theme1");
		
		given(this.themeService.getAllThemes()).willReturn(Lists.newArrayList(theme1, theme2));
		given(this.themeService.getAllThemesPage(1)).willReturn(Lists.newArrayList(theme1, theme2));
		willDoNothing().given(this.themeService).deleteThemeById(1L);
		given(this.themeService.getThemeById(1L)).willReturn(theme1);
		given(this.themeService.getThemeById(4L)).willReturn(themePatched);
		given(this.themeService.addTheme(themeToAdd)).willReturn(themeAdded);
		given(this.themeService.addTheme(themeToPut)).willReturn(themePuted);
		given(this.themeService.addTheme(themeToPatch)).willReturn(themePatched);
	}

}