package io.gitlab.ciskos;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@AutoConfigureDataJdbc
@ActiveProfiles(profiles = "h2")
class CrawlerWebInterfaceApplicationTests {

	@Test
	void contextLoads() {
	}

}
