package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AuthorityTest {

	private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now();
	
	private Authority authority;
	private User user;
	private User userNew;
	private Role role;
	private Role roleNew;
	
	@BeforeEach
	void setUp() throws Exception {
		role = Role.ROLE_ADMIN;
		roleNew = Role.ROLE_USER;

		user = new User();
		
		user.setId(1L);
		user.setUsername("username1");
		user.setPassword("password1");
		user.setEmail("mail1@mail.com");
		user.setEnabled(true);
		user.setRegistered(LOCAL_DATE_TIME);

		userNew = new User();

		userNew.setId(2L);
		userNew.setUsername("username2");
		userNew.setPassword("password2");
		userNew.setEmail("mail2@mail.com");
		userNew.setEnabled(true);
		userNew.setRegistered(LOCAL_DATE_TIME);
		
		authority = new Authority();
		
		authority.setId(1L);
		authority.setUser(user);
		authority.setRole(role);
	}

	@Test
	void testGetId() {
		assertEquals(authority.getId(), 1L);
	}

	@Test
	void testGetUser() {
		assertEquals(authority.getUser(), user);
	}

	@Test
	void testGetRole() {
		assertEquals(authority.getRole(), role);
	}

	@Test
	void testSetId() {
		authority.setId(2L);
		assertEquals(authority.getId(), 2L);
	}

	@Test
	void testSetUser() {
		authority.setUser(userNew);
		assertEquals(authority.getUser(), userNew);
	}

	@Test
	void testSetRole() {
		authority.setRole(roleNew);
		assertEquals(authority.getRole(), roleNew);
	}
	
}
