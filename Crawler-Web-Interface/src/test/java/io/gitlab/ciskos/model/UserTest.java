package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserTest {

	private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now();
	private static final LocalDateTime LOCAL_DATE_TIME_NEW = LocalDateTime.now();
	
	private User user;
	
	@BeforeEach
	void setUp() throws Exception {
		user = new User();
		
		user.setId(1L);
		user.setUsername("username1");
		user.setPassword("password1");
		user.setEmail("mail1@mail.com");
		user.setEnabled(true);
		user.setRegistered(LOCAL_DATE_TIME);
	}

	@Test
	void testGetId() {
		assertEquals(user.getId(), 1L);
	}

	@Test
	void testGetUsername() {
		assertEquals(user.getUsername(), "username1");
	}

	@Test
	void testGetPassword() {
		assertEquals(user.getPassword(), "password1");
	}

	@Test
	void testGetEmail() {
		assertEquals(user.getEmail(), "mail1@mail.com");
	}

	@Test
	void testGetEnabled() {
		assertEquals(user.getEnabled(), true);
	}

	@Test
	void testGetRegistered() {
		assertEquals(user.getRegistered(), LOCAL_DATE_TIME);
	}

	@Test
	void testSetId() {
		user.setId(2L);
		assertEquals(user.getId(), 2L);
	}

	@Test
	void testSetUsername() {
		user.setUsername("username2");
		assertEquals(user.getUsername(), "username2");
	}

	@Test
	void testSetPassword() {
		user.setPassword("password2");
		assertEquals(user.getPassword(), "password2");
	}

	@Test
	void testSetEmail() {
		user.setEmail("mail2@mail.com");
		assertEquals(user.getEmail(), "mail2@mail.com");
	}

	@Test
	void testSetEnabled() {
		user.setEnabled(false);
		assertEquals(user.getEnabled(), false);
	}

	@Test
	void testSetRegistered() {
		user.setRegistered(LOCAL_DATE_TIME_NEW);
		assertEquals(user.getRegistered(), LOCAL_DATE_TIME_NEW);
	}

}
