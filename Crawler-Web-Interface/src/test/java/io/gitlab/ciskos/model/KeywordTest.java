package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class KeywordTest {
	
	private Keyword keyword;
	private Theme theme;
	private Theme themeNew;

	@BeforeEach
	void setUp() throws Exception {
		theme = new Theme();
		
		theme.setId(1L);
		theme.setName("theme1");

		themeNew = new Theme();

		themeNew.setId(1L);
		themeNew.setName("theme2");
		
		keyword = new Keyword();
		
		keyword.setId(1L);
		keyword.setTheme(theme);
		keyword.setWord("keyword1");
	}

	@Test
	void testGetId() {
		assertEquals(keyword.getId(), 1L);
	}

	@Test
	void testGetTheme() {
		assertEquals(keyword.getTheme(), theme);
	}

	@Test
	void testGetWord() {
		assertEquals(keyword.getWord(), "keyword1");
	}

	@Test
	void testSetId() {
		keyword.setId(2L);
		assertEquals(keyword.getId(), 2L);
	}

	@Test
	void testSetTheme() {
		keyword.setTheme(themeNew);
		assertEquals(keyword.getTheme(), themeNew);
	}

	@Test
	void testSetWord() {
		keyword.setWord("keyword2");
		assertEquals(keyword.getWord(), "keyword2");
	}

}
