package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PageTest {
	
	private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now();
	private static final LocalDateTime LOCAL_DATE_TIME_NEW = LocalDateTime.now();
	
	
	private Page page;
	private Site site;
	private Site siteNew;

	@BeforeEach
	void setUp() throws Exception {
		site = new Site();
		
		site.setId(1L);
		site.setUrl("url1");
		site.setDescription("description1");

		siteNew = new Site();
		
		siteNew.setId(2L);
		siteNew.setUrl("url2");
		siteNew.setDescription("description2");

		page = new Page();
		
		page.setId(1L);
		page.setSite(site);
		page.setUrl("url1");
		page.setRegistered(LOCAL_DATE_TIME);
		page.setLastScan(LOCAL_DATE_TIME);
	}

	@Test
	void testGetId() {
		assertEquals(page.getId(), 1L);
	}

	@Test
	void testGetSite() {
		assertEquals(page.getSite(), site);
	}

	@Test
	void testGetUrl() {
		assertEquals(page.getUrl(), "url1");
	}

	@Test
	void testGetRegistered() {
		assertEquals(page.getRegistered(), LOCAL_DATE_TIME);
	}

	@Test
	void testGetLastScan() {
		assertEquals(page.getLastScan(), LOCAL_DATE_TIME);
	}

	@Test
	void testSetId() {
		page.setId(2L);
		assertEquals(page.getId(), 2L);
	}

	@Test
	void testSetSite() {
		page.setSite(siteNew);
		assertEquals(page.getSite(), siteNew);
	}

	@Test
	void testSetUrl() {
		page.setUrl("url2");
		assertEquals(page.getUrl(), "url2");
	}

	@Test
	void testSetRegistered() {
		page.setRegistered(LOCAL_DATE_TIME_NEW);
		assertEquals(page.getRegistered(), LOCAL_DATE_TIME_NEW);
	}

	@Test
	void testSetLastScan() {
		page.setLastScan(LOCAL_DATE_TIME_NEW);
		assertEquals(page.getLastScan(), LOCAL_DATE_TIME_NEW);
	}

}
