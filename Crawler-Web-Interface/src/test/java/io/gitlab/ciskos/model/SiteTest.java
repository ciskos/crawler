package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SiteTest {

	private Site site;
	
	@BeforeEach
	void setUp() throws Exception {
		site = new Site();
		
		site.setId(1L);
		site.setUrl("url1");
		site.setDescription("description1");
	}

	@Test
	void testGetId() {
		assertEquals(site.getId(), 1L);
	}

	@Test
	void testGetUrl() {
		assertEquals(site.getUrl(), "url1");
	}

	@Test
	void testGetDescription() {
		assertEquals(site.getDescription(), "description1");
	}

	@Test
	void testSetId() {
		site.setId(2L);
		assertEquals(site.getId(), 2L);
	}

	@Test
	void testSetUrl() {
		site.setUrl("url2");
		assertEquals(site.getUrl(), "url2");
	}

	@Test
	void testSetDescription() {
		site.setDescription("description2");
	}

}
