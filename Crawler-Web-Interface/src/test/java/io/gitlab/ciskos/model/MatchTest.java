package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatchTest {

	private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now();
	private static final LocalDateTime LOCAL_DATE_TIME_NEW = LocalDateTime.now();
	
	private Match match;
	private Theme theme;
	private Theme themeNew;
	private Page page;
	private Page pageNew;
	private Site site;
	private Site siteNew;
	
	@BeforeEach
	void setUp() throws Exception {
		theme = new Theme();
		
		theme.setId(1L);
		theme.setName("theme1");

		themeNew = new Theme();

		themeNew.setId(2L);
		themeNew.setName("theme2");
		
		site = new Site();
		
		site.setId(1L);
		site.setUrl("url1");
		site.setDescription("description1");

		siteNew = new Site();

		siteNew.setId(2L);
		siteNew.setUrl("url2");
		siteNew.setDescription("description2");
		
		page = new Page();
		
		page.setId(1L);
		page.setSite(site);
		page.setUrl("url1");
		page.setRegistered(LOCAL_DATE_TIME);
		page.setLastScan(LOCAL_DATE_TIME);

		pageNew = new Page();

		pageNew.setId(2L);
		pageNew.setSite(siteNew);
		pageNew.setUrl("url2");
		pageNew.setRegistered(LOCAL_DATE_TIME_NEW);
		pageNew.setLastScan(LOCAL_DATE_TIME_NEW);
		
		match = new Match();
		
		match.setId(1L);
		match.setTheme(theme);
		match.setPage(page);
		match.setCounts(10L);
	}

	@Test
	void testGetId() {
		assertEquals(match.getId(), 1L);
	}

	@Test
	void testGetTheme() {
		assertEquals(match.getTheme(), theme);
	}

	@Test
	void testGetPage() {
		assertEquals(match.getPage(), page);
	}

	@Test
	void testGetCounts() {
		assertEquals(match.getCounts(), 10L);
	}

	@Test
	void testSetId() {
		match.setId(2L);
		assertEquals(match.getId(), 2L);
	}

	@Test
	void testSetTheme() {
		match.setTheme(themeNew);
		assertEquals(match.getTheme(), themeNew);
	}

	@Test
	void testSetPage() {
		match.setPage(pageNew);
		assertEquals(match.getPage(), pageNew);
	}

	@Test
	void testSetCounts() {
		match.setCounts(11L);
		assertEquals(match.getCounts(), 11L);
	}

}
