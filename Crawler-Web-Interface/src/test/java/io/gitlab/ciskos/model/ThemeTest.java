package io.gitlab.ciskos.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ThemeTest {

	private Theme theme;
	
	@BeforeEach
	void setUp() throws Exception {
		theme = new Theme();
		
		theme.setId(1L);
		theme.setName("theme1");
	}

	@Test
	void testGetId() {
		assertEquals(theme.getId(), 1L);
	}

	@Test
	void testGetName() {
		assertEquals(theme.getName(), "theme1");
	}

	@Test
	void testSetId() {
		theme.setId(2L);
		assertEquals(theme.getId(), 2L);
	}

	@Test
	void testSetName() {
		theme.setName("theme2");
		assertEquals(theme.getName(), "theme2");
	}

}
