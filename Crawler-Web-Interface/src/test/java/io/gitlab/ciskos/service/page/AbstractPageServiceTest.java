package io.gitlab.ciskos.service.page;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.repository.PageRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractPageServiceTest {

	protected Page pageToTest;
	protected static Page pageToAdd;
	
	protected static final long PAGE_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;
	
	protected static final String PAGE_URL_UPDATED = "page_url11";
	protected static final String SITE_URL_UPDATED = "site_url11";

	private static final long SITE_ID_ADD = 1L;
	private static final String SITE_URL_ADD = "siteUrl1";
	private static final String SITE_DESCRIPTION_ADD = "siteDescription1";

	private static final String PAGE_URL_ADD = "pageUrl1";
	private static final LocalDateTime PAGE_LOCALDATETIME_ADD = LocalDateTime.now();
	
	@Autowired
	protected PageRepository pageRepository;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		Site site = new Site();
		
		site.setId(SITE_ID_ADD);
		site.setUrl(SITE_URL_ADD);
		site.setDescription(SITE_DESCRIPTION_ADD);
		
		pageToAdd = new Page();
		
		pageToAdd.setId(null);
		pageToAdd.setSite(site);
		pageToAdd.setUrl(PAGE_URL_ADD);
		pageToAdd.setRegistered(PAGE_LOCALDATETIME_ADD);
		pageToAdd.setLastScan(PAGE_LOCALDATETIME_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		pageToTest = this.pageRepository.findById(PAGE_ID_FIRST).get();
	}
}
