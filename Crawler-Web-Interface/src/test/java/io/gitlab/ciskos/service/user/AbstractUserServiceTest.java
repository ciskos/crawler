package io.gitlab.ciskos.service.user;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.repository.UserRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractUserServiceTest {


	protected User userToTest;
	protected static User userToAdd;
	
	protected static final long USER_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;

	protected static final String ADMIN_NAME_UPDATED = "adminUpdated";
	protected static final String ADMIN_PASSWORD_UPDATED = "passUpdated";
	protected static final String ADMIN_EMAIL_UPDATED = "adminUpdated@email.com";
	
	protected static final String USER_NAME_ADD = "userAdded";
	protected static final String USER_PASSWORD_ADD = "passAdded";
	protected static final String USER_EMAIL_ADD = "mailAdded@email.com";
	protected static final boolean USER_ENABLED_ADD = Boolean.TRUE;
	protected static final LocalDateTime USER_LOCALDATETIME_ADD = LocalDateTime.now();

	@Autowired
	protected UserRepository userRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		userToAdd = new User();
		
		userToAdd.setId(null);
		userToAdd.setUsername(USER_NAME_ADD);
		userToAdd.setPassword(USER_PASSWORD_ADD);
		userToAdd.setEmail(USER_EMAIL_ADD);
		userToAdd.setEnabled(USER_ENABLED_ADD);
		userToAdd.setRegistered(USER_LOCALDATETIME_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		userToTest = this.userRepository.findById(USER_ID_FIRST).get();
	}

}