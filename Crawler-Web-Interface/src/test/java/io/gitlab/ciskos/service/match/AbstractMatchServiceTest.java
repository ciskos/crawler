package io.gitlab.ciskos.service.match;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.repository.MatchRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractMatchServiceTest {

	protected Match matchToTest;
	protected static Match matchToAdd;
	
	protected static final long MATCH_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;
	
	protected static final String THEME_NAME_UPDATED = "themeNameUpdated";
	protected static final String PAGE_URL_UPDATED = "pageUrlUpdated";
	protected static final long COUNTS_UPDATED = 11L;

	private static final long SITE_ID_ADD = 1L;
	private static final String SITE_URL_ADD = "siteUrl1";
	private static final String SITE_DESCRIPTION_ADD = "siteDescription1";

	private static final long PAGE_ID_ADD = 1L;
	private static final String PAGE_URL_ADD = "pageUrl1";
	private static final LocalDateTime PAGE_LOCALDATETIME_ADD = LocalDateTime.now();

	private static final long THEME_ID_ADD = 2L;
	private static final String THEME_NAME_ADD = "theme2";

	private static final long MATCH_COUNTS_ADD = 10L;
	
	@Autowired
	protected MatchRepository matchRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		Site site = new Site();
		
		site.setId(SITE_ID_ADD);
		site.setUrl(SITE_URL_ADD);
		site.setDescription(SITE_DESCRIPTION_ADD);
		
		Page page = new Page();
		
		page.setId(PAGE_ID_ADD);
		page.setSite(site);
		page.setUrl(PAGE_URL_ADD);
		page.setRegistered(PAGE_LOCALDATETIME_ADD);
		page.setLastScan(PAGE_LOCALDATETIME_ADD);
		
		Theme theme = new Theme();
		
		theme.setId(THEME_ID_ADD);
		theme.setName(THEME_NAME_ADD);
		
		matchToAdd = new Match();
		
		matchToAdd.setId(null);
		matchToAdd.setTheme(theme);
		matchToAdd.setPage(page);
		matchToAdd.setCounts(MATCH_COUNTS_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		matchToTest = matchRepository.findById(MATCH_ID_FIRST).get();
	}

}