package io.gitlab.ciskos.service.site;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class SiteServiceTest extends AbstractSiteServiceTest {

	@Test
	void testGetAllSites() {
		var sites = this.siteRepository.findAll();
		
		assertThat(sites).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteSiteById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateSite() {
		siteToTest.setUrl(SITE_URL_UPDATED);
		siteToTest.setDescription(SITE_DESCRIPTION_UPDATED);
		
		this.siteRepository.save(siteToTest);
		
		var siteUpdated = this.siteRepository.findById(SITE_ID_FIRST).get();
		
		assertThat(siteUpdated.getUrl()).hasToString(SITE_URL_UPDATED);
		assertThat(siteUpdated.getDescription()).hasToString(SITE_DESCRIPTION_UPDATED);
	}

	@Test
	void testGetSiteById() {
		var siteGetById = this.siteRepository.findById(SITE_ID_FIRST).get();
		
		assertThat(siteGetById.getUrl()).hasToString(siteToTest.getUrl());
		assertThat(siteGetById.getDescription()).hasToString(siteToTest.getDescription());
	}

	@Test
	void testAddSite() {
		var siteSaved = this.siteRepository.save(siteToAdd);
		
		assertThat(siteSaved.getId().longValue()).isNotNull();
		assertThat(siteSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
