package io.gitlab.ciskos.service.authority;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.repository.AuthorityRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractAuthorityServiceTest {

	protected Authority authorityToTest;
	protected Authority authorityToTestThird;
	protected static Authority authorityToAdd;

	protected static final long AUTHORITY_ID_FIRST = 1L;
	protected static final long AUTHORITY_ID_THIRD = 3L;
	protected static final int GREATER_THAN = 0;
	
	protected static final Role ROLE_ADMIN_UPDATED = Role.ROLE_ADMIN;

	protected static final long USER_ID_ADD = 2L;
	protected static final String USER_NAME_ADD = "user2";
	protected static final String USER_PASSWORD_ADD = "pass2";
	protected static final String USER_EMAIL_ADD = "mail2@email.org";
	protected static final boolean USER_ENABLED_ADD = true;
	protected static final LocalDateTime USER_LOCALDATETIME_ADD = LocalDateTime.now();

	protected static final Role ROLE_ADMIN_ADD = Role.ROLE_ADMIN;

	@Autowired
	protected AuthorityRepository authorityRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		User user = new User();
		
		user.setId(USER_ID_ADD);
		user.setUsername(USER_NAME_ADD);
		user.setPassword(USER_PASSWORD_ADD);
		user.setEmail(USER_EMAIL_ADD);
		user.setEnabled(USER_ENABLED_ADD);
		user.setRegistered(USER_LOCALDATETIME_ADD);

		authorityToAdd = new Authority();
		
		authorityToAdd.setId(null);
		authorityToAdd.setUser(user);
		authorityToAdd.setRole(ROLE_ADMIN_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		authorityToTest = this.authorityRepository.findById(AUTHORITY_ID_FIRST).get();
		authorityToTestThird = this.authorityRepository.findById(AUTHORITY_ID_THIRD).get();
	}

}