package io.gitlab.ciskos.service.keyword;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.repository.KeywordRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractKeywordServiceTest {

	protected Keyword keywordToTest;
	protected static Keyword keywordToAdd;

	protected static final long KEYWORD_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;
	
	protected static final String KEYWORD_WORD_UPDATED = "keyword11";
	protected static final String THEME_NAME_UPDATED = "themes11";
	
	private static final long THEME_ID_ADD = 1L;
	protected static final String THEME_NAME_ADDED = "theme1";
	
	protected static final String KEYWORD_WORD_ADDED = "keywordNew";

	@Autowired
	protected KeywordRepository keywordRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		Theme theme = new Theme();
		
		theme.setId(THEME_ID_ADD);
		theme.setName(THEME_NAME_ADDED);

		keywordToAdd = new Keyword();
		
		keywordToAdd.setId(null);
		keywordToAdd.setTheme(theme);
		keywordToAdd.setWord(KEYWORD_WORD_ADDED);
	}
	
	@BeforeEach
	void setUp() throws Exception {
		keywordToTest = this.keywordRepository.findById(KEYWORD_ID_FIRST).get();
	}

}