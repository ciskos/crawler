package io.gitlab.ciskos.service.theme;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.repository.ThemeRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractThemeServiceTest {

	protected Theme themeToTest;
	protected static Theme themeToAdd;
	
	protected static final long THEME_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;
	
	protected static final String THEME_NAME_UPDATE = "themes11";
	
	private static final String THEME_NAME_ADD = "theme1";
	
	@Autowired
	protected ThemeRepository themeRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		themeToAdd = new Theme();
		
		themeToAdd.setId(null);
		themeToAdd.setName(THEME_NAME_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		themeToTest = this.themeRepository.findById(THEME_ID_FIRST).get();
	}

}