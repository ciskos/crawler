package io.gitlab.ciskos.service.page;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class PageServiceTest extends AbstractPageServiceTest {

	@Test
	void testGetAllPages() {
		var pages = this.pageRepository.findAll();
		
		assertThat(pages).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeletePageById() {
		// TODO nothing to check
	}

	@Test
	void testUpdatePage() {
		pageToTest.getSite().setUrl(SITE_URL_UPDATED);
		pageToTest.setUrl(PAGE_URL_UPDATED);
		
		this.pageRepository.save(pageToTest);
		
		var pageUpdated = this.pageRepository.findById(PAGE_ID_FIRST).get();
		
		assertThat(pageUpdated.getSite().getUrl()).hasToString(SITE_URL_UPDATED);
		assertThat(pageUpdated.getUrl()).hasToString(PAGE_URL_UPDATED);
	}

	@Test
	void testGetPageById() {
		var pageGetById = this.pageRepository.findById(PAGE_ID_FIRST).get();
		
		assertThat(pageGetById.getSite()).isEqualTo(pageToTest.getSite());
		assertThat(pageGetById.getUrl()).hasToString(pageToTest.getUrl());
	}

	@Test
	void testAddPage() {
		var pageSaved = this.pageRepository.save(pageToAdd);
		
		assertThat(pageSaved.getId().longValue()).isNotNull();
		assertThat(pageSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
