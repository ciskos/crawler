package io.gitlab.ciskos.service.site;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.repository.SiteRepository;

@SpringBootTest
@Transactional
@ActiveProfiles(profiles = "h2")
public abstract class AbstractSiteServiceTest {

	protected Site siteToTest;
	protected static Site siteToAdd;
	
	protected static final long SITE_ID_FIRST = 1L;
	protected static final int GREATER_THAN = 0;

	protected static final String SITE_DESCRIPTION_UPDATED = "site_description11";
	protected static final String SITE_URL_UPDATED = "site_url11";

	private static final String SITE_URL_ADD = "siteUrl1";
	private static final String SITE_DESCRIPTION_ADD = "siteDescription1";
	
	@Autowired
	protected SiteRepository siteRepository;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		siteToAdd = new Site();
		
		siteToAdd.setId(null);
		siteToAdd.setUrl(SITE_URL_ADD);
		siteToAdd.setDescription(SITE_DESCRIPTION_ADD);
	}

	@BeforeEach
	void setUp() throws Exception {
		siteToTest = this.siteRepository.findById(SITE_ID_FIRST).get();
	}

}