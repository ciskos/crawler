package io.gitlab.ciskos.service.match;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class MatchServiceTest extends AbstractMatchServiceTest {

	@Test
	void testGetAllMatches() {
		var matches = this.matchRepository.findAll();
		
		assertThat(matches).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteMatchById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateMatch() {
		matchToTest.getTheme().setName(THEME_NAME_UPDATED);
		matchToTest.getPage().setUrl(PAGE_URL_UPDATED);
		matchToTest.setCounts(COUNTS_UPDATED);
		
		this.matchRepository.save(matchToTest);

		var matchUpdated = this.matchRepository.findById(MATCH_ID_FIRST).get();
		
		assertThat(matchUpdated.getTheme().getName()).hasToString(THEME_NAME_UPDATED);
		assertThat(matchUpdated.getPage().getUrl()).hasToString(PAGE_URL_UPDATED);
		assertThat(matchUpdated.getCounts()).isEqualTo(COUNTS_UPDATED);
	}

	@Test
	void testGetMatchById() {
		var matchGetById = this.matchRepository.findById(MATCH_ID_FIRST).get();
		
		assertThat(matchGetById.getTheme()).isEqualTo(matchToTest.getTheme());
		assertThat(matchGetById.getPage()).isEqualTo(matchToTest.getPage());
		assertThat(matchGetById.getCounts()).isGreaterThan(GREATER_THAN);
	}

	@Test
	void testAddMatch() {
		var matchSaved = this.matchRepository.save(matchToAdd);
		
		assertThat(matchSaved.getId().longValue()).isNotNull();
		assertThat(matchSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
