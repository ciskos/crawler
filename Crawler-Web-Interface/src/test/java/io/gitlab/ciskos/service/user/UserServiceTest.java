package io.gitlab.ciskos.service.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class UserServiceTest extends AbstractUserServiceTest {

	@Test
	void testGetAllUsers() {
		var users = this.userRepository.findAll();
		
		assertThat(users).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteUserById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateUser() {
		userToTest.setUsername(ADMIN_NAME_UPDATED);
		userToTest.setPassword(ADMIN_PASSWORD_UPDATED);
		userToTest.setEmail(ADMIN_EMAIL_UPDATED);
		userToTest.setEnabled(Boolean.FALSE);
		
		this.userRepository.save(userToTest);
		
		var userUpdated = this.userRepository.findById(USER_ID_FIRST).get();
		
		assertThat(userUpdated.getUsername()).hasToString(ADMIN_NAME_UPDATED);
		assertThat(userUpdated.getPassword()).hasToString(ADMIN_PASSWORD_UPDATED);
		assertThat(userUpdated.getEmail()).hasToString(ADMIN_EMAIL_UPDATED);
		assertThat(userUpdated.getEnabled()).isFalse();
	}

	@Test
	void testGetUserById() {
		var userGetById = this.userRepository.findById(USER_ID_FIRST).get();
		
		assertThat(userGetById.getUsername()).hasToString(userToTest.getUsername());
		assertThat(userGetById.getPassword()).hasToString(userToTest.getPassword());
		assertThat(userGetById.getEmail()).hasToString(userToTest.getEmail());
		assertThat(userGetById.getEnabled()).isTrue();
	}

	@Test
	void testAddUser() {
		var userSaved = this.userRepository.save(userToAdd);
		
		assertThat(userSaved.getId().longValue()).isNotNull();
		assertThat(userSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
