package io.gitlab.ciskos.service.keyword;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class KeywordServiceTest extends AbstractKeywordServiceTest {

	@Test
	void testGetAllKeywords() {
		var keywords = this.keywordRepository.findAll();

		assertThat(keywords).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteKeywordById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateKeyword() {
		keywordToTest.getTheme().setName(THEME_NAME_UPDATED);
		keywordToTest.setWord(KEYWORD_WORD_UPDATED);
		
		this.keywordRepository.save(keywordToTest);
		
		var keywordUpdated = this.keywordRepository.findById(KEYWORD_ID_FIRST).get();
		
		assertThat(keywordUpdated.getTheme().getName()).hasToString(THEME_NAME_UPDATED);
		assertThat(keywordUpdated.getWord()).hasToString(KEYWORD_WORD_UPDATED);
	}

	@Test
	void testGetKeywordById() {
		var keywordGetById = this.keywordRepository.findById(KEYWORD_ID_FIRST).get();
		
		assertThat(keywordGetById.getId()).isEqualTo(KEYWORD_ID_FIRST);
		assertThat(keywordGetById.getTheme()).isEqualTo(keywordToTest.getTheme());
		assertThat(keywordGetById.getWord()).hasToString(keywordToTest.getWord());
	}

	@Test
	void testAddKeyword() {
		var keywordSaved = this.keywordRepository.save(keywordToAdd);

		assertThat(keywordSaved.getId().longValue()).isNotNull();
		assertThat(keywordSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
