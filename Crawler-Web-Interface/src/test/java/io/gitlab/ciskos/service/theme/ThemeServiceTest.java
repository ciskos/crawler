package io.gitlab.ciskos.service.theme;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class ThemeServiceTest extends AbstractThemeServiceTest {

	@Test
	void testGetAllThemes() {
		var themes = this.themeRepository.findAll();
		
		assertThat(themes).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteThemeById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateTheme() {
		themeToTest.setName(THEME_NAME_UPDATE);
		
		this.themeRepository.save(themeToTest);
		
		var themeUpdated = this.themeRepository.findById(THEME_ID_FIRST).get();
		
		assertThat(themeUpdated.getName()).hasToString(THEME_NAME_UPDATE);
	}

	@Test
	void testGetThemeById() {
		var themeGetById = this.themeRepository.findById(THEME_ID_FIRST).get();
		
		assertThat(themeGetById.getName()).hasToString(themeToTest.getName());
	}

	@Test
	void testAddTheme() {
		var themeSaved = this.themeRepository.save(themeToAdd);
		
		assertThat(themeSaved.getId().longValue()).isNotNull();
		assertThat(themeSaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
