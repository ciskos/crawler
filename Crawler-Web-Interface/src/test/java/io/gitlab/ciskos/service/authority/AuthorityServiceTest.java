package io.gitlab.ciskos.service.authority;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class AuthorityServiceTest extends AbstractAuthorityServiceTest {

	@Test
	void testGetAllAuthorities() {
		var authorities = this.authorityRepository.findAll();

		assertThat(authorities).hasSizeGreaterThan(GREATER_THAN);
	}

	@Disabled
	@Test
	void testDeleteAuthorityById() {
		// TODO nothing to check
	}

	@Test
	void testUpdateAuthority() {
		authorityToTestThird.setRole(ROLE_ADMIN_UPDATED);
		
		this.authorityRepository.save(authorityToTestThird);
		
		var authorityUpdated = this.authorityRepository.findById(AUTHORITY_ID_THIRD).get();
		
		assertThat(authorityUpdated.getRole()).isEqualTo(ROLE_ADMIN_UPDATED);
	}

	@Test
	void testGetAuthorityById() {
		var authority = this.authorityRepository.findById(AUTHORITY_ID_FIRST).get();
		
		assertThat(authority.getId()).isEqualTo(authorityToTest.getId());
		assertThat(authority.getUser()).isEqualTo(authorityToTest.getUser());
		assertThat(authority.getRole()).isEqualTo(authorityToTest.getRole());
	}

	@Test
	void testAddAuthority() {
		var authoritySaved = this.authorityRepository.save(authorityToAdd);

		assertThat(authoritySaved.getId().longValue()).isNotNull();
		assertThat(authoritySaved.getId().longValue()).isGreaterThan(GREATER_THAN);
	}

}
