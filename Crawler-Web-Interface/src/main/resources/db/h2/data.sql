-- pass1
INSERT INTO users VALUES(1, 'admin', '$2y$10$mKrGc8YmvKWrgkCgjBKqy.MfTHXbkDAJwfvX1yE4nI/GcvmUU/kSq', 'admin@email.com', TRUE, '2021-04-22T00:00:00');
-- pass2
INSERT INTO users VALUES(2, 'user1', '$2y$10$0c7gysNWlkNuloniy0z67.I6oB/5KENf39yblJMwn7ZG/J3xvfUzu', 'user1@email.com', TRUE, '2021-04-22T00:00:00');
-- pass3
INSERT INTO users VALUES(3, 'user2', '$2y$10$y19OeOj/4.sYpBMumgAque8yzhtBnodcNiHn.M.d0NSt4U9kLl0nG', 'user2@email.com', TRUE, '2021-04-22T00:00:00');

INSERT INTO authorities VALUES(1, 1, 'ROLE_ADMIN');
INSERT INTO authorities VALUES(2, 1, 'ROLE_USER');
INSERT INTO authorities VALUES(3, 2, 'ROLE_USER');
INSERT INTO authorities VALUES(4, 3, 'ROLE_USER');

INSERT INTO sites VALUES(1, 'site_url1', 'site_description1');
INSERT INTO sites VALUES(2, 'site_url2', 'site_description2');
INSERT INTO sites VALUES(3, 'site_url3', 'site_description3');

INSERT INTO pages VALUES(1, 1, 'page_url1', '2021-04-22T00:00:00', '2021-04-22T00:00:00');
INSERT INTO pages VALUES(2, 2, 'page_url2', '2021-04-22T00:00:00', '2021-04-22T00:00:00');
INSERT INTO pages VALUES(3, 3, 'page_url3', '2021-04-22T00:00:00', '2021-04-22T00:00:00');

INSERT INTO themes VALUES(1, 'themes1');
INSERT INTO themes VALUES(2, 'themes2');
INSERT INTO themes VALUES(3, 'themes3');

INSERT INTO matches VALUES(1, 1, 1, 1);
INSERT INTO matches VALUES(2, 2, 2, 2);
INSERT INTO matches VALUES(3, 3, 3, 3);

INSERT INTO keywords VALUES(1, 1, 'keyword1');
INSERT INTO keywords VALUES(2, 2, 'keyword2');
INSERT INTO keywords VALUES(3, 3, 'keyword3');
