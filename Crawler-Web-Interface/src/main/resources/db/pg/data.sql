-- pass1
INSERT INTO users(username, password, email, enabled, registered) VALUES('admin', '$2y$10$mKrGc8YmvKWrgkCgjBKqy.MfTHXbkDAJwfvX1yE4nI/GcvmUU/kSq', 'admin@email.com', TRUE, '2021-04-22T00:00:00');
-- pass2
INSERT INTO users(username, password, email, enabled, registered) VALUES('user1', '$2y$10$0c7gysNWlkNuloniy0z67.I6oB/5KENf39yblJMwn7ZG/J3xvfUzu', 'user1@email.com', TRUE, '2021-04-22T00:00:00');
-- pass3
INSERT INTO users(username, password, email, enabled, registered) VALUES('user2', '$2y$10$y19OeOj/4.sYpBMumgAque8yzhtBnodcNiHn.M.d0NSt4U9kLl0nG', 'user2@email.com', TRUE, '2021-04-22T00:00:00');

INSERT INTO authorities(user_id, role) VALUES(1, 'ROLE_ADMIN');
INSERT INTO authorities(user_id, role) VALUES(1, 'ROLE_USER');
INSERT INTO authorities(user_id, role) VALUES(2, 'ROLE_USER');
INSERT INTO authorities(user_id, role) VALUES(3, 'ROLE_USER');

INSERT INTO sites(url, description) VALUES('site_url1', 'site_description1');
INSERT INTO sites(url, description) VALUES('site_url2', 'site_description2');
INSERT INTO sites(url, description) VALUES('site_url3', 'site_description3');

INSERT INTO pages(site_id, url, registered, last_scan) VALUES(1, 'page_url1', '2021-04-22T00:00:00', '2021-04-22T00:00:00');
INSERT INTO pages(site_id, url, registered, last_scan) VALUES(2, 'page_url2', '2021-04-22T00:00:00', '2021-04-22T00:00:00');
INSERT INTO pages(site_id, url, registered, last_scan) VALUES(3, 'page_url3', '2021-04-22T00:00:00', '2021-04-22T00:00:00');

INSERT INTO themes(name) VALUES('themes1');
INSERT INTO themes(name) VALUES('themes2');
INSERT INTO themes(name) VALUES('themes3');

INSERT INTO matches(theme_id, page_id, counts) VALUES(1, 1, 1);
INSERT INTO matches(theme_id, page_id, counts) VALUES(2, 2, 2);
INSERT INTO matches(theme_id, page_id, counts) VALUES(3, 3, 3);

INSERT INTO keywords(theme_id, word) VALUES(1, 'keyword1');
INSERT INTO keywords(theme_id, word) VALUES(2, 'keyword2');
INSERT INTO keywords(theme_id, word) VALUES(3, 'keyword3');
