DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS authorities CASCADE;
DROP TABLE IF EXISTS sites CASCADE;
DROP TABLE IF EXISTS pages CASCADE;
DROP TABLE IF EXISTS matches CASCADE;
DROP TABLE IF EXISTS themes CASCADE;
DROP TABLE IF EXISTS keywords CASCADE;

CREATE TABLE IF NOT EXISTS users (
	id				BIGSERIAL	PRIMARY KEY
	, username		VARCHAR(255)				NOT NULL
	, password		VARCHAR(255)				NOT NULL
	, email			VARCHAR(255)				NOT NULL
	, enabled		BOOLEAN		DEFAULT TRUE	NOT NULL
	, registered	TIMESTAMP	DEFAULT now()	NOT NULL
);
CREATE INDEX IF NOT EXISTS idx_users_username ON users (username);

CREATE TABLE IF NOT EXISTS authorities (
	id			BIGSERIAL	PRIMARY KEY
	, user_id	BIGINT							NOT NULL
	, ROLE		VARCHAR(255)					NOT NULL
);
ALTER TABLE IF EXISTS authorities ADD CONSTRAINT fk_authorities_users FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
CREATE UNIQUE INDEX IF NOT EXISTS idx_authorities_username_role ON authorities (user_id, role);

CREATE TABLE IF NOT EXISTS sites (
	id				BIGSERIAL	PRIMARY KEY
	, url			VARCHAR(255)				NOT NULL
	, description	VARCHAR(255)				NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_sites_url ON sites (url);

CREATE TABLE IF NOT EXISTS pages (
	id				BIGSERIAL	PRIMARY KEY
	, site_id		BIGINT						NOT NULL
	, url			VARCHAR(255)				NOT NULL
	, registered	TIMESTAMP					NOT NULL
	, last_scan		TIMESTAMP					NOT NULL
);
ALTER TABLE IF EXISTS pages ADD CONSTRAINT fk_pages_sites FOREIGN KEY (site_id) REFERENCES sites (id) ON DELETE CASCADE;
CREATE UNIQUE INDEX IF NOT EXISTS idx_pages_url ON pages (url);

CREATE TABLE IF NOT EXISTS themes (
	id		BIGSERIAL	PRIMARY KEY
	, name	VARCHAR(255)						NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_themes_name ON themes (name);

CREATE TABLE IF NOT EXISTS matches (
	id			BIGSERIAL	PRIMARY KEY
	, theme_id	BIGINT							NOT NULL
	, page_id	BIGINT							NOT NULL
	, counts	BIGINT							NOT NULL
);
CREATE INDEX IF NOT EXISTS idx_matches_counts ON matches (counts);
ALTER TABLE IF EXISTS matches ADD CONSTRAINT fk_matches_themes FOREIGN KEY (theme_id) REFERENCES themes (id) ON DELETE CASCADE;
ALTER TABLE IF EXISTS matches ADD CONSTRAINT fk_matches_pages FOREIGN KEY (page_id) REFERENCES pages (id) ON DELETE CASCADE;
CREATE UNIQUE INDEX IF NOT EXISTS idx_matches_theme_id_page_id ON matches (theme_id, page_id);

CREATE TABLE keywords (
	id			BIGSERIAL	PRIMARY KEY
	, theme_id	BIGINT							NOT NULL
	, word		VARCHAR(255)					NOT NULL
);
ALTER TABLE IF EXISTS keywords ADD CONSTRAINT fk_keywords_themes FOREIGN KEY (theme_id) REFERENCES themes (id) ON DELETE CASCADE;
