package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.KeywordHateoasModel;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.restController.KeywordsRestController;

public class KeywordHateoasModelAssembler extends RepresentationModelAssemblerSupport<Keyword, KeywordHateoasModel> {

	public KeywordHateoasModelAssembler() {
		super(KeywordsRestController.class, KeywordHateoasModel.class);
	}

	@Override
	protected KeywordHateoasModel instantiateModel(Keyword keyword) {
		return new KeywordHateoasModel(keyword);
	}

	@Override
	public KeywordHateoasModel toModel(Keyword keyword) {
		return createModelWithId(keyword.getId(), keyword);
	}
	
}
