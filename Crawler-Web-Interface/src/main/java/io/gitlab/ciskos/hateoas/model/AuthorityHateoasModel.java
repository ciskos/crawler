package io.gitlab.ciskos.hateoas.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.hateoas.assembler.UserHateoasModelAssembler;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import lombok.Getter;

@Getter
@Relation(itemRelation = "authority", collectionRelation = "authorities")
public class AuthorityHateoasModel extends RepresentationModel<AuthorityHateoasModel> {

	private static final UserHateoasModelAssembler USER_HATEOAS_MODEL_ASSEMBLER = new UserHateoasModelAssembler();
	
	private UserHateoasModel user;
	private Role role;

	public AuthorityHateoasModel(Authority authority) {
		this.user = USER_HATEOAS_MODEL_ASSEMBLER.toModel(authority.getUser());
		this.role = authority.getRole();
	}
	
}
