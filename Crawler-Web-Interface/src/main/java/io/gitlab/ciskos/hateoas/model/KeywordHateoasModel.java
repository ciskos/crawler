package io.gitlab.ciskos.hateoas.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.hateoas.assembler.ThemeHateoasModelAssembler;
import io.gitlab.ciskos.model.Keyword;
import lombok.Getter;

@Getter
@Relation(itemRelation = "keyword", collectionRelation = "keywords")
public class KeywordHateoasModel extends RepresentationModel<KeywordHateoasModel> {

	private static final ThemeHateoasModelAssembler THEME_HATEOAS_MODEL_ASSEMBLER = new ThemeHateoasModelAssembler();
	
	private ThemeHateoasModel theme;
	private String word;

	public KeywordHateoasModel(Keyword keyword) {
		this.theme = THEME_HATEOAS_MODEL_ASSEMBLER.toModel(keyword.getTheme());
		this.word = keyword.getWord();
	}
	
}
