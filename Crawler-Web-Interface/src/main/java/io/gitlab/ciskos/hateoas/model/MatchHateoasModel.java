package io.gitlab.ciskos.hateoas.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.hateoas.assembler.PageHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.assembler.ThemeHateoasModelAssembler;
import io.gitlab.ciskos.model.Match;
import lombok.Getter;

@Getter
@Relation(itemRelation = "match", collectionRelation = "matches")
public class MatchHateoasModel extends RepresentationModel<MatchHateoasModel> {

	private static final ThemeHateoasModelAssembler THEME_HATEOAS_MODEL_ASSEMBLER = new ThemeHateoasModelAssembler();
	private static final PageHateoasModelAssembler PAGE_HATEOAS_MODEL_ASSEMBLER = new PageHateoasModelAssembler();
	
	private ThemeHateoasModel theme;
	private PageHateoasModel page;
	private Long counts;
	
	public MatchHateoasModel(Match match) {
		this.theme = THEME_HATEOAS_MODEL_ASSEMBLER.toModel(match.getTheme());
		this.page = PAGE_HATEOAS_MODEL_ASSEMBLER.toModel(match.getPage());
		this.counts = match.getCounts();
	}

}
