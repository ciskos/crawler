package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.AuthorityHateoasModel;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.restController.AuthoritiesRestController;

public class AuthorityHateoasModelAssembler extends RepresentationModelAssemblerSupport<Authority, AuthorityHateoasModel> {

	public AuthorityHateoasModelAssembler() {
		super(AuthoritiesRestController.class, AuthorityHateoasModel.class);
	}

	@Override
	protected AuthorityHateoasModel instantiateModel(Authority authority) {
		return new AuthorityHateoasModel(authority);
	}

	@Override
	public AuthorityHateoasModel toModel(Authority authority) {
		return createModelWithId(authority.getId(), authority);
	}
	
}
