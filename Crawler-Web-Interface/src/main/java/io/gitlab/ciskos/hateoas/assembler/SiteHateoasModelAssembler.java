package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.SiteHateoasModel;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.restController.SitesRestController;

public class SiteHateoasModelAssembler extends RepresentationModelAssemblerSupport<Site, SiteHateoasModel> {

	public SiteHateoasModelAssembler() {
		super(SitesRestController.class, SiteHateoasModel.class);
	}

	@Override
	protected SiteHateoasModel instantiateModel(Site site) {
		return new SiteHateoasModel(site);
	}

	@Override
	public SiteHateoasModel toModel(Site site) {
		return createModelWithId(site.getId(), site);
	}
	
}
