package io.gitlab.ciskos.hateoas.model;

import java.time.LocalDateTime;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.model.User;
import lombok.Getter;

@Getter
@Relation(itemRelation = "user", collectionRelation = "users")
public class UserHateoasModel extends RepresentationModel<UserHateoasModel> {

	private String username;
	private String password;
	private String email;
	private Boolean enabled;
	private LocalDateTime registered;

	public UserHateoasModel(User user) {
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.enabled = user.getEnabled();
		this.registered = user.getRegistered();
	}

}
