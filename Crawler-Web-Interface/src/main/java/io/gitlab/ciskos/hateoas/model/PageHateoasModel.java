package io.gitlab.ciskos.hateoas.model;

import java.time.LocalDateTime;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.hateoas.assembler.SiteHateoasModelAssembler;
import io.gitlab.ciskos.model.Page;
import lombok.Getter;

@Getter
@Relation(itemRelation = "page", collectionRelation = "pages")
public class PageHateoasModel extends RepresentationModel<PageHateoasModel> {

	private static final SiteHateoasModelAssembler SITE_HATEOAS_MODEL_ASSEMBLER = new SiteHateoasModelAssembler();
	
	private SiteHateoasModel site;
	private String url;
	private LocalDateTime registered;
	private LocalDateTime lastScan;

	public PageHateoasModel(Page page) {
		this.site = SITE_HATEOAS_MODEL_ASSEMBLER.toModel(page.getSite());
		this.url = page.getUrl();
		this.registered = page.getRegistered();
		this.lastScan = page.getLastScan();
	}

}
