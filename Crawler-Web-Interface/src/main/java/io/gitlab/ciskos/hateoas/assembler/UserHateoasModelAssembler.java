package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.UserHateoasModel;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.restController.UsersRestController;

public class UserHateoasModelAssembler extends RepresentationModelAssemblerSupport<User, UserHateoasModel> {

	public UserHateoasModelAssembler() {
		super(UsersRestController.class, UserHateoasModel.class);
	}

	
	@Override
	protected UserHateoasModel instantiateModel(User user) {
		return new UserHateoasModel(user);
	}

	@Override
	public UserHateoasModel toModel(User user) {
		return createModelWithId(user.getId(), user);
	}

}
