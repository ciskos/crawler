package io.gitlab.ciskos.hateoas.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.model.Theme;
import lombok.Getter;

@Getter
@Relation(itemRelation = "theme", collectionRelation = "themes")
public class ThemeHateoasModel extends RepresentationModel<ThemeHateoasModel> {

	private String name;

	public ThemeHateoasModel(Theme theme) {
		this.name = theme.getName();
	}
	
}
