package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.PageHateoasModel;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.restController.PagesRestController;

public class PageHateoasModelAssembler extends RepresentationModelAssemblerSupport<Page, PageHateoasModel> {

	public PageHateoasModelAssembler() {
		super(PagesRestController.class, PageHateoasModel.class);
	}

	@Override
	protected PageHateoasModel instantiateModel(Page page) {
		return new PageHateoasModel(page);
	}

	@Override
	public PageHateoasModel toModel(Page page) {
		return createModelWithId(page.getId(), page);
	}
	
}
