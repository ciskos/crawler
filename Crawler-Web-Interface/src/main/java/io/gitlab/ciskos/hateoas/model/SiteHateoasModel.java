package io.gitlab.ciskos.hateoas.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.gitlab.ciskos.model.Site;
import lombok.Getter;

@Getter
@Relation(itemRelation = "site", collectionRelation = "sites")
public class SiteHateoasModel extends RepresentationModel<SiteHateoasModel> {

	private String url;
	private String description;

	public SiteHateoasModel(Site site) {
		this.url = site.getUrl();
		this.description = site.getDescription();
	}

}
