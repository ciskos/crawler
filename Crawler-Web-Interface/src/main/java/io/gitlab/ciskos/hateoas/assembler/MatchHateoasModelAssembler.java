package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.MatchHateoasModel;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.restController.MatchesRestController;

public class MatchHateoasModelAssembler extends RepresentationModelAssemblerSupport<Match, MatchHateoasModel> {

	public MatchHateoasModelAssembler() {
		super(MatchesRestController.class, MatchHateoasModel.class);
	}

	@Override
	protected MatchHateoasModel instantiateModel(Match match) {
		return new MatchHateoasModel(match);
	}

	@Override
	public MatchHateoasModel toModel(Match match) {
		return createModelWithId(match.getId(), match);
	}
	
}
