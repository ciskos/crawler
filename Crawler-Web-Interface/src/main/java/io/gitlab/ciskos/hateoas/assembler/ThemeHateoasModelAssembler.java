package io.gitlab.ciskos.hateoas.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import io.gitlab.ciskos.hateoas.model.ThemeHateoasModel;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.restController.ThemesRestController;

public class ThemeHateoasModelAssembler extends RepresentationModelAssemblerSupport<Theme, ThemeHateoasModel> {

	public ThemeHateoasModelAssembler() {
		super(ThemesRestController.class, ThemeHateoasModel.class);
	}

	@Override
	protected ThemeHateoasModel instantiateModel(Theme theme) {
		return new ThemeHateoasModel(theme);
	}

	@Override
	public ThemeHateoasModel toModel(Theme theme) {
		return createModelWithId(theme.getId(), theme);
	}
	
}
