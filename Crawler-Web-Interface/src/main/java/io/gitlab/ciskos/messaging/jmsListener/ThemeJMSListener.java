package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Theme;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ThemeJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.theme")
	public void themeListener(Theme theme) {
		log.info("receiving theme " + theme);
	}
	
}
