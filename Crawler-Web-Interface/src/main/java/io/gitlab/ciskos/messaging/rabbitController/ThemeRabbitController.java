package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.ThemeRabbitService;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@RestController
@RequestMapping(ThemeRabbitController.THEMES_RABBIT_PATH)
public class ThemeRabbitController {

	static final String THEMES_RABBIT_PATH = "/themes/rabbit";

	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private ThemeRabbitService themeRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Theme> sendTheme(@PathVariable("id") Long themeId) {
		Theme theme = themeService.getThemeById(themeId);
		
		themeRabbitService.sendTheme(theme);
		
		return new ResponseEntity<Theme>(theme, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Theme> receiveTheme() {
		return new ResponseEntity<Theme>(themeRabbitService.receiveTheme(), HttpStatus.OK);
	}
	
}
