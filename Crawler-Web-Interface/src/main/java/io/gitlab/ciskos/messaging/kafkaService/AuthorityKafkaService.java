package io.gitlab.ciskos.messaging.kafkaService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.AbstractBaseEntity;
import io.gitlab.ciskos.model.Authority;

@Service
public class AuthorityKafkaService {
	
	@Value("${topics.authority}")
	private String topic;
	
	private KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate;

	public AuthorityKafkaService(KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendAuthority(Authority authority) {
		kafkaTemplate.send(topic, authority);
	}
	
}
