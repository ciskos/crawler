package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.MatchJMSService;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.service.MatchService;

@RestController
@RequestMapping(MatchJMSController.MATCHES_JMS_PATH)
public class MatchJMSController {

	static final String MATCHES_JMS_PATH = "/matches/jms";

	@Autowired
	private MatchService matchService;
	
	@Autowired
	private MatchJMSService matchJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Match> sendMatch(@PathVariable("id") Long matchId) {
		Match match = matchService.getMatchById(matchId);
		
		matchJMSService.sendMatch(match);
		
		return new ResponseEntity<Match>(match, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Match> receiveMatch() {
		return new ResponseEntity<Match>(matchJMSService.receiveMatch(), HttpStatus.OK);
	}
	
}
