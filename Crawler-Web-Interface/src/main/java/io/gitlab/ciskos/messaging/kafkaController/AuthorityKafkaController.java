package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.AuthorityKafkaService;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.service.AuthorityService;

@RestController
@RequestMapping(AuthorityKafkaController.AUTHORITIES_KAFKA_PATH)
public class AuthorityKafkaController {

	static final String AUTHORITIES_KAFKA_PATH = "/authorities/kafka";

	@Autowired
	private AuthorityService authorityService;
	
	@Autowired
	private AuthorityKafkaService authorityKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Authority> sendAuthority(@PathVariable("id") Long authorityId) {
		Authority authority = authorityService.getAuthorityById(authorityId);
		
		authorityKafkaService.sendAuthority(authority);
		
		return new ResponseEntity<Authority>(authority, HttpStatus.OK);
	}

}
