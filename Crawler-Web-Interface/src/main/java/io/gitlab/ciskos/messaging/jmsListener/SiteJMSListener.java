package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Site;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SiteJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.site")
	public void siteListener(Site site) {
		log.info("receiving site " + site);
	}
	
}
