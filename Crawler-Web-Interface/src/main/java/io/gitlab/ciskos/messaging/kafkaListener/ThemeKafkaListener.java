package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Theme;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ThemeKafkaListener {

	@KafkaListener(topics = "${topics.theme}")
	public void themeListener(Theme theme) {
		log.info("receiving theme " + theme);
	}
	
}
