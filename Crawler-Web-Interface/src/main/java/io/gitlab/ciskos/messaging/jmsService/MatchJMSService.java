package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Match;

@Service
public class MatchJMSService {

	private static final String CRAWLER_QUEUE_MATCH = "crawler.queue.match";
	
	private JmsTemplate jmsTemplate;

	public MatchJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendMatch(Match match) {
		jmsTemplate.send(CRAWLER_QUEUE_MATCH, session -> session.createObjectMessage(match));
	}
	
	public Match receiveMatch() {
		return (Match) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_MATCH);
	}

}
