package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Keyword;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KeywordJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.keyword")
	public void keywordListener(Keyword keyword) {
		log.info("receiving keyword " + keyword);
	}
	
}
