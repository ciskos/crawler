package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Match;

@Service
public class MatchRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.match";
	private static final String QUEUE = "crawler.queue.match";
	
	private RabbitTemplate rabbitTemplate;

	public MatchRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendMatch(Match match) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, match);
	}
	
	public Match receiveMatch() {
		return (Match) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
