package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Keyword;

@Service
public class KeywordRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.keyword";
	private static final String QUEUE = "crawler.queue.keyword";

	private RabbitTemplate rabbitTemplate;

	public KeywordRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendKeyword(Keyword keyword) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, keyword);
	}
	
	public Keyword receiveKeyword() {
		return (Keyword) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
