package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Site;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SiteRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.site")
	public void siteListener(Site site) {
		log.info("receiving site " + site);
	}
	
}
