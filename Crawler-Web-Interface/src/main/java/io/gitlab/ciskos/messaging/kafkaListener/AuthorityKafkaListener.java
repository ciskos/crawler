package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Authority;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthorityKafkaListener {

	@KafkaListener(topics = "${topics.authority}")
	public void authorityListener(Authority authority) {
		log.info("receiving authority " + authority);
	}
	
}
