package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Keyword;

@Service
public class KeywordJMSService {

	private static final String CRAWLER_QUEUE_KEYWORD = "crawler.queue.keyword";
	
	private JmsTemplate jmsTemplate;

	public KeywordJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendKeyword(Keyword keyword) {
		jmsTemplate.send(CRAWLER_QUEUE_KEYWORD, session -> session.createObjectMessage(keyword));
	}
	
	public Keyword receiveKeyword() {
		return (Keyword) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_KEYWORD);
	}

}
