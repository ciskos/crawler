package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Authority;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthorityRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.authority")
	public void authorityListener(Authority authority) {
		log.info("receiving authority " + authority);
	}
	
}
