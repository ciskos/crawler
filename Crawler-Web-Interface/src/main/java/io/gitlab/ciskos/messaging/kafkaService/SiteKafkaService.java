package io.gitlab.ciskos.messaging.kafkaService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.AbstractBaseEntity;
import io.gitlab.ciskos.model.Site;

@Service
public class SiteKafkaService {
	
	@Value("${topics.site}")
	private String topic;
	
	private KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate;

	public SiteKafkaService(KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendSite(Site site) {
		kafkaTemplate.send(topic, site);
	}
	
}
