package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.MatchRabbitService;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.service.MatchService;

@RestController
@RequestMapping(MatchRabbitController.MATCHES_RABBIT_PATH)
public class MatchRabbitController {

	static final String MATCHES_RABBIT_PATH = "/matches/rabbit";

	@Autowired
	private MatchService matchService;
	
	@Autowired
	private MatchRabbitService matchRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Match> sendMatch(@PathVariable("id") Long matchId) {
		Match match = matchService.getMatchById(matchId);
		
		matchRabbitService.sendMatch(match);
		
		return new ResponseEntity<Match>(match, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Match> receiveMatch() {
		return new ResponseEntity<Match>(matchRabbitService.receiveMatch(), HttpStatus.OK);
	}
	
}
