package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Page;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PageKafkaListener {

	@KafkaListener(topics = "${topics.page}")
	public void pageListener(Page page) {
		log.info("receiving page " + page);
	}
	
}
