package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Keyword;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KeywordKafkaListener {

	@KafkaListener(topics = "${topics.keyword}")
	public void keywordListener(Keyword keyword) {
		log.info("receiving keyword " + keyword);
	}
	
}
