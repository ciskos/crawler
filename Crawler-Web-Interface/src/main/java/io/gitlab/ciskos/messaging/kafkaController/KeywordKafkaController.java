package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.KeywordKafkaService;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.service.KeywordService;

@RestController
@RequestMapping(KeywordKafkaController.KEYWORDS_KAFKA_PATH)
public class KeywordKafkaController {

	static final String KEYWORDS_KAFKA_PATH = "/keywords/kafka";

	@Autowired
	private KeywordService keywordService;
	
	@Autowired
	private KeywordKafkaService keywordKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Keyword> sendKeyword(@PathVariable("id") Long keywordId) {
		Keyword keyword = keywordService.getKeywordById(keywordId);
		
		keywordKafkaService.sendKeyword(keyword);
		
		return new ResponseEntity<Keyword>(keyword, HttpStatus.OK);
	}

}
