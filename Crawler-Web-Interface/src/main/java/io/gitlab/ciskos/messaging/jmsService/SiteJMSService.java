package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Site;

@Service
public class SiteJMSService {

	private static final String CRAWLER_QUEUE_SITE = "crawler.queue.site";
	
	private JmsTemplate jmsTemplate;

	public SiteJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendSite(Site site) {
		jmsTemplate.send(CRAWLER_QUEUE_SITE, session -> session.createObjectMessage(site));
	}
	
	public Site receiveSite() {
		return (Site) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_SITE);
	}

}
