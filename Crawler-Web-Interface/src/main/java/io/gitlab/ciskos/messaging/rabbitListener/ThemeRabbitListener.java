package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Theme;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ThemeRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.theme")
	public void themeListener(Theme theme) {
		log.info("receiving theme " + theme);
	}
	
}
