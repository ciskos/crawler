package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.user")
	public void userListener(User user) {
		log.info("receiving user " + user);
	}
	
}
