package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.AuthorityRabbitService;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.service.AuthorityService;

@RestController
@RequestMapping(AuthorityRabbitController.AUTHORITIES_RABBIT_PATH)
public class AuthorityRabbitController {

	static final String AUTHORITIES_RABBIT_PATH = "/authorities/rabbit";

	@Autowired
	private AuthorityService authorityService;
	
	@Autowired
	private AuthorityRabbitService authorityRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Authority> sendAuthority(@PathVariable("id") Long authorityId) {
		Authority authority = authorityService.getAuthorityById(authorityId);
		
		authorityRabbitService.sendAuthority(authority);
		
		return new ResponseEntity<Authority>(authority, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Authority> receiveAuthority() {
		return new ResponseEntity<Authority>(authorityRabbitService.receiveAuthority(), HttpStatus.OK);
	}
	
}
