package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.UserRabbitService;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.UserService;

@RestController
@RequestMapping(UserRabbitController.USERS_RABBIT_PATH)
public class UserRabbitController {

	static final String USERS_RABBIT_PATH = "/users/rabbit";

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRabbitService userRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<User> sendUser(@PathVariable("id") Long userId) {
		User user = userService.getUserById(userId);
		
		userRabbitService.sendUser(user);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<User> receiveUser() {
		return new ResponseEntity<User>(userRabbitService.receiveUser(), HttpStatus.OK);
	}
	
}
