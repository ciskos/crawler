package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.ThemeJMSService;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@RestController
@RequestMapping(ThemeJMSController.THEMES_JMS_PATH)
public class ThemeJMSController {

	static final String THEMES_JMS_PATH = "/themes/jms";

	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private ThemeJMSService themeJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Theme> sendTheme(@PathVariable("id") Long themeId) {
		Theme theme = themeService.getThemeById(themeId);
		
		themeJMSService.sendTheme(theme);
		
		return new ResponseEntity<Theme>(theme, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Theme> receiveTheme() {
		return new ResponseEntity<Theme>(themeJMSService.receiveTheme(), HttpStatus.OK);
	}
	
}
