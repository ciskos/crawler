package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.user")
	public void userListener(User user) {
		log.info("receiving user " + user);
	}
	
}
