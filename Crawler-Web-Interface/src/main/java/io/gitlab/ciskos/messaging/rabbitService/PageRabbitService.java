package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Page;

@Service
public class PageRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.page";
	private static final String QUEUE = "crawler.queue.page";
	
	private RabbitTemplate rabbitTemplate;

	public PageRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendPage(Page page) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, page);
	}
	
	public Page receivePage() {
		return (Page) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
