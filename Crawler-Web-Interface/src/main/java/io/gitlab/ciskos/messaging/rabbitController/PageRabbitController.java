package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.PageRabbitService;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.service.PageService;

@RestController
@RequestMapping(PageRabbitController.PAGES_RABBIT_PATH)
public class PageRabbitController {

	static final String PAGES_RABBIT_PATH = "/pages/rabbit";

	@Autowired
	private PageService pageService;
	
	@Autowired
	private PageRabbitService pageRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Page> sendPage(@PathVariable("id") Long pageId) {
		Page page = pageService.getPageById(pageId);
		
		pageRabbitService.sendPage(page);
		
		return new ResponseEntity<Page>(page, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Page> receivePage() {
		return new ResponseEntity<Page>(pageRabbitService.receivePage(), HttpStatus.OK);
	}
	
}
