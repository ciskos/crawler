package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.User;

@Service
public class UserJMSService {

	private static final String CRAWLER_QUEUE_USER = "crawler.queue.user";

	private JmsTemplate jmsTemplate;

	public UserJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendUser(User user) {
		jmsTemplate.send(CRAWLER_QUEUE_USER, session -> session.createObjectMessage(user));
	}
	
	public User receiveUser() {
		return (User) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_USER);
	}

}
