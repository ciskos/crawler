package io.gitlab.ciskos.messaging.kafkaService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.AbstractBaseEntity;
import io.gitlab.ciskos.model.Keyword;

@Service
public class KeywordKafkaService {
	
	@Value("${topics.keyword}")
	private String topic;
	
	private KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate;

	public KeywordKafkaService(KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendKeyword(Keyword keyword) {
		kafkaTemplate.send(topic, keyword);
	}
	
}
