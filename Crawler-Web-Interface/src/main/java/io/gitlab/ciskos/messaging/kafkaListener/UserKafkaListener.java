package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserKafkaListener {

	@KafkaListener(topics = "${topics.user}")
	public void userListener(User user) {
		log.info("receiving user " + user);
	}
	
}
