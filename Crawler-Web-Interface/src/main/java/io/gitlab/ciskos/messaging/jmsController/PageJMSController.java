package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.PageJMSService;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.service.PageService;

@RestController
@RequestMapping(PageJMSController.PAGES_JMS_PATH)
public class PageJMSController {

	static final String PAGES_JMS_PATH = "/pages/jms";

	@Autowired
	private PageService pageService;
	
	@Autowired
	private PageJMSService pageJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Page> sendPage(@PathVariable("id") Long pageId) {
		Page page = pageService.getPageById(pageId);
		
		pageJMSService.sendPage(page);
		
		return new ResponseEntity<Page>(page, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Page> receivePage() {
		return new ResponseEntity<Page>(pageJMSService.receivePage(), HttpStatus.OK);
	}
	
}
