package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Match;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MatchJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.match")
	public void matchListener(Match match) {
		log.info("receiving match " + match);
	}
	
}
