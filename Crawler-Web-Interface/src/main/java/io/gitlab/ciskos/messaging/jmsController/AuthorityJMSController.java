package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.AuthorityJMSService;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.service.AuthorityService;

@RestController
@RequestMapping(AuthorityJMSController.AUTHORITIES_JMS_PATH)
public class AuthorityJMSController {

	static final String AUTHORITIES_JMS_PATH = "/authorities/jms";

	@Autowired
	private AuthorityService authorityService;
	
	@Autowired
	private AuthorityJMSService authorityJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Authority> sendAuthority(@PathVariable("id") Long authorityId) {
		Authority authority = authorityService.getAuthorityById(authorityId);
		
		authorityJMSService.sendAuthority(authority);
		
		return new ResponseEntity<Authority>(authority, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Authority> receiveAuthority() {
		return new ResponseEntity<Authority>(authorityJMSService.receiveAuthority(), HttpStatus.OK);
	}
	
}
