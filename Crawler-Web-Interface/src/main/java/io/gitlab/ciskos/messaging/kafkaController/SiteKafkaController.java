package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.SiteKafkaService;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@RestController
@RequestMapping(SiteKafkaController.SITES_KAFKA_PATH)
public class SiteKafkaController {

	static final String SITES_KAFKA_PATH = "/sites/kafka";

	@Autowired
	private SiteService siteService;
	
	@Autowired
	private SiteKafkaService siteKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Site> sendSite(@PathVariable("id") Long siteId) {
		Site site = siteService.getSiteById(siteId);
		
		siteKafkaService.sendSite(site);
		
		return new ResponseEntity<Site>(site, HttpStatus.OK);
	}

}
