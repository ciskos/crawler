package io.gitlab.ciskos.messaging.kafkaService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.AbstractBaseEntity;
import io.gitlab.ciskos.model.User;

@Service
public class UserKafkaService {
	
	@Value("${topics.user}")
	private String topic;
	
	private KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate;

	public UserKafkaService(KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendUser(User user) {
		kafkaTemplate.send(topic, user);
	}
	
}
