package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.SiteRabbitService;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@RestController
@RequestMapping(SiteRabbitController.SITES_RABBIT_PATH)
public class SiteRabbitController {

	static final String SITES_RABBIT_PATH = "/sites/rabbit";

	@Autowired
	private SiteService siteService;
	
	@Autowired
	private SiteRabbitService siteRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Site> sendSite(@PathVariable("id") Long siteId) {
		Site site = siteService.getSiteById(siteId);
		
		siteRabbitService.sendSite(site);
		
		return new ResponseEntity<Site>(site, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Site> receiveSite() {
		return new ResponseEntity<Site>(siteRabbitService.receiveSite(), HttpStatus.OK);
	}
	
}
