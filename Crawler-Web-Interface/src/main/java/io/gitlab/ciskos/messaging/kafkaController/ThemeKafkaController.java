package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.ThemeKafkaService;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@RestController
@RequestMapping(ThemeKafkaController.THEMES_KAFKA_PATH)
public class ThemeKafkaController {

	static final String THEMES_KAFKA_PATH = "/themes/kafka";

	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private ThemeKafkaService themeKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Theme> sendTheme(@PathVariable("id") Long themeId) {
		Theme theme = themeService.getThemeById(themeId);
		
		themeKafkaService.sendTheme(theme);
		
		return new ResponseEntity<Theme>(theme, HttpStatus.OK);
	}

}
