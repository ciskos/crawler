package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Site;

@Service
public class SiteRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.site";
	private static final String QUEUE = "crawler.queue.site";
	
	private RabbitTemplate rabbitTemplate;

	public SiteRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendSite(Site site) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, site);
	}
	
	public Site receiveSite() {
		return (Site) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
