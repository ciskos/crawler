package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Theme;

@Service
public class ThemeRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.theme";
	private static final String QUEUE = "crawler.queue.theme";
	
	private RabbitTemplate rabbitTemplate;

	public ThemeRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendTheme(Theme theme) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, theme);
	}
	
	public Theme receiveTheme() {
		return (Theme) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
