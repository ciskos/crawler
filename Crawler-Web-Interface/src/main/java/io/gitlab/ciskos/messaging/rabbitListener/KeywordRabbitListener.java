package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Keyword;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KeywordRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.keyword")
	public void keywordListener(Keyword keyword) {
		log.info("receiving keyword " + keyword);
	}
	
}
