package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Authority;

@Service
public class AuthorityRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.authority";
	private static final String QUEUE = "crawler.queue.authority";
	
	private RabbitTemplate rabbitTemplate;

	public AuthorityRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendAuthority(Authority authority) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, authority);
	}
	
	public Authority receiveAuthority() {
		return (Authority) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
