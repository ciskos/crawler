package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Site;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SiteKafkaListener {

	@KafkaListener(topics = "${topics.site}")
	public void siteListener(Site site) {
		log.info("receiving site " + site);
	}
	
}
