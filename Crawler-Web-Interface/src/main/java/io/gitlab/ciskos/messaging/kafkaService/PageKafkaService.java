package io.gitlab.ciskos.messaging.kafkaService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.AbstractBaseEntity;
import io.gitlab.ciskos.model.Page;

@Service
public class PageKafkaService {
	
	@Value("${topics.page}")
	private String topic;
	
	private KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate;

	public PageKafkaService(KafkaTemplate<String, AbstractBaseEntity> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}
	
	public void sendPage(Page page) {
		kafkaTemplate.send(topic, page);
	}
	
}
