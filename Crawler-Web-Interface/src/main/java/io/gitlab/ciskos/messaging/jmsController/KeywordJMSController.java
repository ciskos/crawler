package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.KeywordJMSService;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.service.KeywordService;

@RestController
@RequestMapping(KeywordJMSController.KEYWORDS_JMS_PATH)
public class KeywordJMSController {

	static final String KEYWORDS_JMS_PATH = "/keywords/jms";

	@Autowired
	private KeywordService keywordService;
	
	@Autowired
	private KeywordJMSService keywordJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Keyword> sendKeyword(@PathVariable("id") Long keywordId) {
		Keyword keyword = keywordService.getKeywordById(keywordId);
		
		keywordJMSService.sendKeyword(keyword);
		
		return new ResponseEntity<Keyword>(keyword, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Keyword> receiveKeyword() {
		return new ResponseEntity<Keyword>(keywordJMSService.receiveKeyword(), HttpStatus.OK);
	}
	
}
