package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Authority;

@Service
public class AuthorityJMSService {

	private static final String CRAWLER_QUEUE_AUTHORITY = "crawler.queue.authority";
	
	private JmsTemplate jmsTemplate;

	public AuthorityJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendAuthority(Authority authority) {
		jmsTemplate.send(CRAWLER_QUEUE_AUTHORITY, session -> session.createObjectMessage(authority));
	}
	
	public Authority receiveAuthority() {
		return (Authority) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_AUTHORITY);
	}

}
