package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Theme;

@Service
public class ThemeJMSService {

	private static final String CRAWLER_QUEUE_THEME = "crawler.queue.theme";
	
	private JmsTemplate jmsTemplate;

	public ThemeJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendTheme(Theme theme) {
		jmsTemplate.send(CRAWLER_QUEUE_THEME, session -> session.createObjectMessage(theme));
	}
	
	public Theme receiveTheme() {
		return (Theme) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_THEME);
	}

}
