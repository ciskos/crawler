package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Page;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PageJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.page")
	public void pageListener(Page page) {
		log.info("receiving page " + page);
	}
	
}
