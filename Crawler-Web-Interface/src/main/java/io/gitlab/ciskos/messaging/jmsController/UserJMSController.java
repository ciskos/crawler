package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.UserJMSService;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.UserService;

@RestController
@RequestMapping(UserJMSController.USERS_JMS_PATH)
public class UserJMSController {

	static final String USERS_JMS_PATH = "/users/jms";

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserJMSService userJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<User> sendUser(@PathVariable("id") Long userId) {
		User user = userService.getUserById(userId);
		
		userJMSService.sendUser(user);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<User> receiveUser() {
		return new ResponseEntity<User>(userJMSService.receiveUser(), HttpStatus.OK);
	}
	
}
