package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Match;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MatchRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.match")
	public void matchListener(Match match) {
		log.info("receiving match " + match);
	}
	
}
