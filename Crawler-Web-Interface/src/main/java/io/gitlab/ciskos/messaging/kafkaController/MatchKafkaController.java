package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.MatchKafkaService;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.service.MatchService;

@RestController
@RequestMapping(MatchKafkaController.MATCHES_KAFKA_PATH)
public class MatchKafkaController {

	static final String MATCHES_KAFKA_PATH = "/matches/kafka";

	@Autowired
	private MatchService matchService;
	
	@Autowired
	private MatchKafkaService matchKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Match> sendMatch(@PathVariable("id") Long matchId) {
		Match match = matchService.getMatchById(matchId);
		
		matchKafkaService.sendMatch(match);
		
		return new ResponseEntity<Match>(match, HttpStatus.OK);
	}

}
