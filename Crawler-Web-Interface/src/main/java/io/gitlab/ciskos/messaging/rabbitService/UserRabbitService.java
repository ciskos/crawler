package io.gitlab.ciskos.messaging.rabbitService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.User;

@Service
public class UserRabbitService {

	private static final String EXCHANGE = "crawler.exchange";
	private static final String ROUTING_KEY = "crawler.key.user";
	private static final String QUEUE = "crawler.queue.user";
	
	private RabbitTemplate rabbitTemplate;

	public UserRabbitService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	public void sendUser(User user) {
		rabbitTemplate.convertAndSend(EXCHANGE, ROUTING_KEY, user);
	}
	
	public User receiveUser() {
		return (User) rabbitTemplate.receiveAndConvert(QUEUE);
	}

}
