package io.gitlab.ciskos.messaging.rabbitController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.rabbitService.KeywordRabbitService;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.service.KeywordService;

@RestController
@RequestMapping(KeywordRabbitController.KEYWORDS_RABBIT_PATH)
public class KeywordRabbitController {

	static final String KEYWORDS_RABBIT_PATH = "/keywords/rabbit";

	@Autowired
	private KeywordService keywordService;
	
	@Autowired
	private KeywordRabbitService keywordRabbitService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Keyword> sendKeyword(@PathVariable("id") Long keywordId) {
		Keyword keyword = keywordService.getKeywordById(keywordId);
		
		keywordRabbitService.sendKeyword(keyword);
		
		return new ResponseEntity<Keyword>(keyword, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Keyword> receiveKeyword() {
		return new ResponseEntity<Keyword>(keywordRabbitService.receiveKeyword(), HttpStatus.OK);
	}
	
}
