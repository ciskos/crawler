package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.PageKafkaService;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.service.PageService;

@RestController
@RequestMapping(PageKafkaController.PAGES_KAFKA_PATH)
public class PageKafkaController {

	static final String PAGES_KAFKA_PATH = "/pages/kafka";

	@Autowired
	private PageService pageService;
	
	@Autowired
	private PageKafkaService pageKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Page> sendPage(@PathVariable("id") Long pageId) {
		Page page = pageService.getPageById(pageId);
		
		pageKafkaService.sendPage(page);
		
		return new ResponseEntity<Page>(page, HttpStatus.OK);
	}

}
