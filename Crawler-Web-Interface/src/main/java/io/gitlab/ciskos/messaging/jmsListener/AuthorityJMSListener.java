package io.gitlab.ciskos.messaging.jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Authority;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthorityJMSListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@JmsListener(destination = "crawler.queue.authority")
	public void authorityListener(Authority authority) {
		log.info("receiving authority " + authority);
	}
	
}
