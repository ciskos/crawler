package io.gitlab.ciskos.messaging.kafkaController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.kafkaService.UserKafkaService;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.UserService;

@RestController
@RequestMapping(UserKafkaController.USERS_KAFKA_PATH)
public class UserKafkaController {

	static final String USERS_KAFKA_PATH = "/users/kafka";

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserKafkaService userKafkaService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<User> sendUser(@PathVariable("id") Long userId) {
		User user = userService.getUserById(userId);
		
		userKafkaService.sendUser(user);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

}
