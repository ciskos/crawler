package io.gitlab.ciskos.messaging.jmsController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.messaging.jmsService.SiteJMSService;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@RestController
@RequestMapping(SiteJMSController.SITES_JMS_PATH)
public class SiteJMSController {

	static final String SITES_JMS_PATH = "/sites/jms";

	@Autowired
	private SiteService siteService;
	
	@Autowired
	private SiteJMSService siteJMSService;
	
	@GetMapping("send/{id}")
	public ResponseEntity<Site> sendSite(@PathVariable("id") Long siteId) {
		Site site = siteService.getSiteById(siteId);
		
		siteJMSService.sendSite(site);
		
		return new ResponseEntity<Site>(site, HttpStatus.OK);
	}

	@GetMapping("receive")
	public ResponseEntity<Site> receiveSite() {
		return new ResponseEntity<Site>(siteJMSService.receiveSite(), HttpStatus.OK);
	}
	
}
