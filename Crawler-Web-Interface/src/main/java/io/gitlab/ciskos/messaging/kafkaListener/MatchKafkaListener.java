package io.gitlab.ciskos.messaging.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Match;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MatchKafkaListener {

	@KafkaListener(topics = "${topics.match}")
	public void matchListener(Match match) {
		log.info("receiving match " + match);
	}
	
}
