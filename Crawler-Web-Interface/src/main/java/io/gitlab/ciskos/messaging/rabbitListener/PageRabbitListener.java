package io.gitlab.ciskos.messaging.rabbitListener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import io.gitlab.ciskos.model.Page;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PageRabbitListener {

	// отключено, т.к. с отключёнными сервером производит ошибки
//	@RabbitListener(queues = "crawler.queue.page")
	public void pageListener(Page page) {
		log.info("receiving page " + page);
	}
	
}
