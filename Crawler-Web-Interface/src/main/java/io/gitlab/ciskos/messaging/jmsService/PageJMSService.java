package io.gitlab.ciskos.messaging.jmsService;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Page;

@Service
public class PageJMSService {

	private static final String CRAWLER_QUEUE_PAGE = "crawler.queue.page";
	
	private JmsTemplate jmsTemplate;

	public PageJMSService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void sendPage(Page page) {
		jmsTemplate.send(CRAWLER_QUEUE_PAGE, session -> session.createObjectMessage(page));
	}
	
	public Page receivePage() {
		return (Page) jmsTemplate.receiveAndConvert(CRAWLER_QUEUE_PAGE);
	}

}
