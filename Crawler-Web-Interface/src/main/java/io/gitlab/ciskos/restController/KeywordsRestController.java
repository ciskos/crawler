package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertKeyword;
import io.gitlab.ciskos.dto.KeywordDTO;
import io.gitlab.ciskos.hateoas.assembler.KeywordHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.KeywordHateoasModel;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.service.KeywordService;
import io.gitlab.ciskos.service.ThemeService;

@RestController
@RequestMapping(value = KeywordsRestController.KEYWORD_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class KeywordsRestController {
	

	static final String KEYWORD_PATH = "/keywords";
	private static final String HATEOAS_ALL_KEYWORDS = "allKeywords";
	private static final String HATEOAS_KEYWORDS_PAGE = "keywordsPage";

	private KeywordService keywordService;
	private ThemeService themeService;

	public KeywordsRestController(KeywordService keywordService, ThemeService themeService) {
		this.keywordService = keywordService;
		this.themeService = themeService;
	}

	@GetMapping
	public CollectionModel<KeywordHateoasModel> getAllKeywords() {
		var keywords = (List<Keyword>) keywordService.getAllKeywords();
		var hateoas = new KeywordHateoasModelAssembler().toCollectionModel(keywords);
		
		hateoas.add(linkTo(methodOn(KeywordsRestController.class).getAllKeywords())
				.withRel(HATEOAS_ALL_KEYWORDS));

		return hateoas;
	}
	
	@GetMapping("/page/{pageNumber}")
	public CollectionModel<KeywordHateoasModel> getAllKeywordsByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var keywordsPage = keywordService.getAllKeywordsPage(pageNumber);
		var hateoas = new KeywordHateoasModelAssembler().toCollectionModel(keywordsPage);
		
		hateoas.add(linkTo(methodOn(KeywordsRestController.class).getAllKeywordsByPage(pageNumber))
				.withRel(HATEOAS_KEYWORDS_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<KeywordHateoasModel> getKeywordById(@PathVariable("id") Long id) {
		var keyword = keywordService.getKeywordById(id);
		
		if (keyword.getId() != null) {
			var hateoas = new KeywordHateoasModelAssembler().toModel(keyword);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public KeywordHateoasModel addKeyword(@RequestBody KeywordDTO keywordDTO) {
		var theme = themeService.getThemeById(keywordDTO.getThemeId());
		var keyword = ConvertKeyword.toEntity(keywordDTO, theme);
		var keywordAdded = keywordService.addKeyword(keyword);
		
		return new KeywordHateoasModelAssembler().toModel(keywordAdded);
	}
	
	@PutMapping("/{keywordId}")
	public KeywordHateoasModel putKeyword(@RequestBody KeywordDTO keywordDTO) {
		var theme = themeService.getThemeById(keywordDTO.getThemeId());
		var keyword = ConvertKeyword.toEntity(keywordDTO, theme);
		var keywordAdded = keywordService.addKeyword(keyword);
		
		return new KeywordHateoasModelAssembler().toModel(keywordAdded);
	}

	@PatchMapping(path = "/{keywordId}", consumes = "application/json")
	public KeywordHateoasModel patchKeyword(
			@PathVariable("keywordId") Long keywordId
			, @RequestBody KeywordDTO keywordDTO) {
		var keyword = keywordService.getKeywordById(keywordId);
		
		if (keywordDTO.getKeyword() != null) {
			keyword.setWord(keywordDTO.getKeyword());
		}
		
		var keywordAdded = keywordService.addKeyword(keyword);
		
		return new KeywordHateoasModelAssembler().toModel(keywordAdded);
	}

	@DeleteMapping("/{keywordId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteKeyword(@PathVariable("keywordId") Long keywordId) {
		try {
			keywordService.deleteKeywordById(keywordId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
