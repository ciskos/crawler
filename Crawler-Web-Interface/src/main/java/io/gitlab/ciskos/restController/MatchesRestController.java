package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.hateoas.assembler.MatchHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.MatchHateoasModel;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.service.MatchService;

@RestController
@RequestMapping(value = MatchesRestController.MATCH_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class MatchesRestController {
	

	static final String MATCH_PATH = "/matches";
	private static final String HATEOAS_ALL_MATCHES = "allMatches";
	private static final String HATEOAS_MATCHES_PAGE = "matchesPage";

	private MatchService matchService;

	public MatchesRestController(MatchService matchService) {
		this.matchService = matchService;
	}

	@GetMapping
	public CollectionModel<MatchHateoasModel> getAllMatches() {
		var matches = (List<Match>) matchService.getAllMatches();
		var hateoas = new MatchHateoasModelAssembler().toCollectionModel(matches);
		
		hateoas.add(linkTo(methodOn(MatchesRestController.class).getAllMatches())
				.withRel(HATEOAS_ALL_MATCHES));
		
		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public CollectionModel<MatchHateoasModel> getAllMatchesByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var matchesPage = matchService.getAllMatchesPage(pageNumber);
		var hateoas = new MatchHateoasModelAssembler().toCollectionModel(matchesPage);
		
		hateoas.add(linkTo(methodOn(MatchesRestController.class).getAllMatchesByPage(pageNumber))
				.withRel(HATEOAS_MATCHES_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<MatchHateoasModel> getMatchById(@PathVariable("id") Long id) {
		var match = matchService.getMatchById(id);
		
		if (match.getId() != null) {
			var hateoas = new MatchHateoasModelAssembler().toModel(match);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	// TODO нужны ли эти методы?
//	@PostMapping(consumes = "application/json")
//	@ResponseStatus(code = HttpStatus.CREATED)
//	public MatchDTO addMatch(@RequestBody MatchDTO matchDTO) {
//		var user = userService.getUserById(matchDTO.getUserId());
//		var match = ConvertMatch.toEntity(matchDTO, user);
//		
//		return ConvertMatch.toDto(matchService.addMatch(match));
//	}
//	
//	@PutMapping("/{matchId}")
//	public MatchDTO putMatch(@RequestBody MatchDTO matchDTO) {
//		var user = userService.getUserById(matchDTO.getUserId());
//		var match = ConvertMatch.toEntity(matchDTO, user);
//		
//		return ConvertMatch.toDto(matchService.addMatch(match));
//	}
//
//	@PatchMapping(path = "/{matchId}", consumes = "application/json")
//	public MatchDTO patchMatch(
//			@PathVariable("matchId") Long matchId
//			, @RequestBody MatchDTO matchDTO) {
//		var match = matchService.getMatchById(matchId);
//		
//		if (matchDTO.getRole() != null) {
//			match.setRole(Role.valueOf(matchDTO.getRole()));
//		}
//		
//		return ConvertMatch.toDto(matchService.addMatch(match));
//	}

	@DeleteMapping("/{matchId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteMatch(@PathVariable("matchId") Long matchId) {
		try {
			matchService.deleteMatchById(matchId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
