package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertTheme;
import io.gitlab.ciskos.dto.ThemeDTO;
import io.gitlab.ciskos.hateoas.assembler.ThemeHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.ThemeHateoasModel;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@RestController
@RequestMapping(value = ThemesRestController.THEME_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class ThemesRestController {
	

	static final String THEME_PATH = "/themes";
	private static final String HATEOAS_ALL_THEMES = "allThemes";
	private static final String HATEOAS_THEMES_PAGE = "themesPage";

	private ThemeService themeService;

	public ThemesRestController(ThemeService themeService) {
		this.themeService = themeService;
	}

	@GetMapping
	public CollectionModel<ThemeHateoasModel> getAllThemes() {
		var themes = (List<Theme>) themeService.getAllThemes();
		var hateoas = new ThemeHateoasModelAssembler().toCollectionModel(themes);
		
		hateoas.add(linkTo(methodOn(ThemesRestController.class).getAllThemes())
				.withRel(HATEOAS_ALL_THEMES));

		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public CollectionModel<ThemeHateoasModel> getAllThemesByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var themesPage = themeService.getAllThemesPage(pageNumber);
		var hateoas = new ThemeHateoasModelAssembler().toCollectionModel(themesPage);
		
		hateoas.add(linkTo(methodOn(ThemesRestController.class).getAllThemesByPage(pageNumber))
				.withRel(HATEOAS_THEMES_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<ThemeHateoasModel> getThemeById(@PathVariable("id") Long id) {
		var theme = themeService.getThemeById(id);
		
		if (theme.getId() != null) {
			var hateoas = new ThemeHateoasModelAssembler().toModel(theme);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ThemeHateoasModel addTheme(@RequestBody ThemeDTO themeDTO) {
		var theme = ConvertTheme.toEntity(themeDTO);
		var themeAdded = themeService.addTheme(theme);
		var hateoas = new ThemeHateoasModelAssembler().toModel(themeAdded);
		
		return hateoas;
	}
	
	@PutMapping("/{themeId}")
	public ThemeHateoasModel putTheme(@RequestBody ThemeDTO themeDTO) {
		var theme = ConvertTheme.toEntity(themeDTO);
		var themeAdded = themeService.addTheme(theme);
		var hateoas = new ThemeHateoasModelAssembler().toModel(themeAdded);
		
		return hateoas;
	}

	@PatchMapping(path = "/{themeId}", consumes = "application/json")
	public ThemeHateoasModel patchTheme(
			@PathVariable("themeId") Long themeId
			, @RequestBody ThemeDTO themeDTO) {
		var theme = themeService.getThemeById(themeId);
		
		if (themeDTO.getName() != null) {
			theme.setName(themeDTO.getName());
		}
		
		var themeAdded = themeService.addTheme(theme);
		
		return new ThemeHateoasModelAssembler().toModel(themeAdded);
	}

	@DeleteMapping("/{themeId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteTheme(@PathVariable("themeId") Long themeId) {
		try {
			themeService.deleteThemeById(themeId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
