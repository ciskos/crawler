package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertUser;
import io.gitlab.ciskos.dto.UserDTO;
import io.gitlab.ciskos.hateoas.assembler.UserHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.UserHateoasModel;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@RestController
@RequestMapping(value = UsersRestController.USER_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class UsersRestController {
	

	static final String USER_PATH = "/users";
	private static final String HATEOAS_ALL_USERS = "allUsers";
	private static final String HATEOAS_USERS_PAGE = "usersPage";

	private UserService userService;
	private AuthorityService authorityService;

	public UsersRestController(UserService userService, AuthorityService authorityService) {
		this.userService = userService;
		this.authorityService = authorityService;
	}

	@GetMapping
	public CollectionModel<UserHateoasModel> getAllUsers() {
		var users = (List<User>) userService.getAllUsers();
		var hateoas = new UserHateoasModelAssembler().toCollectionModel(users);
		
		hateoas.add(linkTo(methodOn(UsersRestController.class).getAllUsers())
				.withRel(HATEOAS_ALL_USERS));

		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public CollectionModel<UserHateoasModel> getAllUsersByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var usersPage = userService.getAllUsersPage(pageNumber);
		var hateoas = new UserHateoasModelAssembler().toCollectionModel(usersPage);
		
		hateoas.add(linkTo(methodOn(UsersRestController.class).getAllUsersByPage(pageNumber))
				.withRel(HATEOAS_USERS_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<UserHateoasModel> getUserById(@PathVariable("id") Long id) {
		var user = userService.getUserById(id);
		
		if (user.getId() != null) {
			var hateoas = new UserHateoasModelAssembler().toModel(user);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public UserHateoasModel addUser(@RequestBody UserDTO userDTO) {
		var userAdded = userService.addUser(ConvertUser.toEntity(userDTO));

		var authority = new Authority();
		authority.setUser(userAdded);
		authority.setRole(Role.ROLE_USER);
		
		authorityService.addAuthority(authority);
		
		var hateoas = new UserHateoasModelAssembler().toModel(userAdded);
		
		return hateoas;
	}
	
	@PutMapping("/{userId}")
	public UserHateoasModel putUser(@RequestBody UserDTO userDTO) {
		var user = userService.addUser(ConvertUser.toEntity(userDTO));
		var hateoas = new UserHateoasModelAssembler().toModel(user);
		
		return hateoas;
	}

	@PatchMapping(path = "/{userId}", consumes = "application/json")
	public UserHateoasModel patchUser(
			@PathVariable("userId") Long userId
			, @RequestBody UserDTO userDTO) {
		var user = userService.getUserById(userId);
		
		if (userDTO.getUsername() != null) {
			user.setUsername(userDTO.getUsername());
		}
		
		if (userDTO.getPassword() != null) {
			user.setPassword(userDTO.getPassword());
		}
		
		if (userDTO.getEmail() != null) {
			user.setEmail(userDTO.getEmail());
		}
		
		if (userDTO.getEnabled() != null) {
			user.setEnabled(userDTO.getEnabled());
		}
		
		if (userDTO.getRegistered() != null) {
			user.setRegistered(userDTO.getRegistered());
		}
		
		var userAdded = userService.addUser(user);
		
		return new UserHateoasModelAssembler().toModel(userAdded);
	}

	@DeleteMapping("/{userId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable("userId") Long userId) {
		try {
			userService.deleteUserById(userId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
