package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertAuthority;
import io.gitlab.ciskos.dto.AuthorityDTO;
import io.gitlab.ciskos.hateoas.assembler.AuthorityHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.AuthorityHateoasModel;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@RestController
@RequestMapping(value = AuthoritiesRestController.AUTHORITY_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class AuthoritiesRestController {
	
	static final String AUTHORITY_PATH = "/authorities";
	private static final String HATEOAS_ALL_AUTHORITIES = "allAuthorities";
	private static final String HATEOAS_AUTHORITIES_PAGE = "authoritiesPage";

	private AuthorityService authorityService;
	private UserService userService;

	public AuthoritiesRestController(AuthorityService authorityService, UserService userService) {
		this.authorityService = authorityService;
		this.userService = userService;
	}

	@GetMapping
	public CollectionModel<AuthorityHateoasModel> getAllAuthorities() {
		var authorities = (List<Authority>) authorityService.getAllAuthorities();
		var hateoas = new AuthorityHateoasModelAssembler().toCollectionModel(authorities);
		
		hateoas.add(linkTo(methodOn(AuthoritiesRestController.class).getAllAuthorities())
				.withRel(HATEOAS_ALL_AUTHORITIES));
		
		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public CollectionModel<AuthorityHateoasModel> getAllAuthoritiesByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var authoritiesPage = authorityService.getAllAuthoritiesPage(pageNumber);
		var hateoas = new AuthorityHateoasModelAssembler().toCollectionModel(authoritiesPage);
		
		hateoas.add(linkTo(methodOn(AuthoritiesRestController.class).getAllAuthoritiesByPage(pageNumber))
				.withRel(HATEOAS_AUTHORITIES_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<AuthorityHateoasModel> getAuthorityById(@PathVariable("id") Long id) {
		var authority = authorityService.getAuthorityById(id);
		
		if (authority.getId() != null) {
			var hateoas = new AuthorityHateoasModelAssembler().toModel(authority);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public AuthorityHateoasModel addAuthority(@RequestBody AuthorityDTO authorityDTO) {
		var user = userService.getUserById(authorityDTO.getUserId());
		var authority = ConvertAuthority.toEntity(authorityDTO, user);
		var authorityAdded = authorityService.addAuthority(authority);
		
		return new AuthorityHateoasModelAssembler().toModel(authorityAdded);
	}
	
	@PutMapping("/{authorityId}")
	public AuthorityHateoasModel putAuthority(@RequestBody AuthorityDTO authorityDTO) {
		var user = userService.getUserById(authorityDTO.getUserId());
		var authority = ConvertAuthority.toEntity(authorityDTO, user);
		var authorityAdded = authorityService.addAuthority(authority);
		
		return new AuthorityHateoasModelAssembler().toModel(authorityAdded);
	}

	@PatchMapping(path = "/{authorityId}", consumes = "application/json")
	public AuthorityHateoasModel patchAuthority(
			@PathVariable("authorityId") Long authorityId
			, @RequestBody AuthorityDTO authorityDTO) {
		var authority = authorityService.getAuthorityById(authorityId);
		
		if (authorityDTO.getRole() != null) {
			authority.setRole(Role.valueOf(authorityDTO.getRole()));
		}
		
		var authorityAdded = authorityService.addAuthority(authority);
		
		return new AuthorityHateoasModelAssembler().toModel(authorityAdded);
	}

	@DeleteMapping("/{authorityId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteAuthority(@PathVariable("authorityId") Long authorityId) {
		try {
			authorityService.deleteAuthorityById(authorityId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
