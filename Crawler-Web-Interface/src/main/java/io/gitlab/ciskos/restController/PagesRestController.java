package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertPage;
import io.gitlab.ciskos.dto.PageDTO;
import io.gitlab.ciskos.hateoas.assembler.PageHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.PageHateoasModel;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.service.PageService;
import io.gitlab.ciskos.service.SiteService;

@RestController
@RequestMapping(value = PagesRestController.PAGE_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class PagesRestController {
	

	static final String PAGE_PATH = "/pages";
	private static final String HATEOAS_ALL_PAGES = "allPages";
	private static final String HATEOAS_PAGES_PAGE = "pagesPage";

	private PageService pageService;
	private SiteService siteService;

	public PagesRestController(PageService pageService, SiteService siteService) {
		this.pageService = pageService;
		this.siteService = siteService;
	}

	@GetMapping
	public CollectionModel<PageHateoasModel> getAllPages() {
		var pages = (List<Page>) pageService.getAllPages();
		var hateoas = new PageHateoasModelAssembler().toCollectionModel(pages);
		
		hateoas.add(linkTo(methodOn(PagesRestController.class).getAllPages())
				.withRel(HATEOAS_ALL_PAGES));

		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public CollectionModel<PageHateoasModel> getAllPagesByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var pagesPage = pageService.getAllPagesPage(pageNumber);
		var hateoas = new PageHateoasModelAssembler().toCollectionModel(pagesPage);
		
		hateoas.add(linkTo(methodOn(MatchesRestController.class).getAllMatchesByPage(pageNumber))
				.withRel(HATEOAS_PAGES_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<PageHateoasModel> getPageById(@PathVariable("id") Long id) {
		var page = pageService.getPageById(id);
		
		if (page.getId() != null) {
			var hateoas = new PageHateoasModelAssembler().toModel(page);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public PageHateoasModel addPage(@RequestBody PageDTO pageDTO) {
		var site = siteService.getSiteById(pageDTO.getSiteId());
		var page = ConvertPage.toEntity(pageDTO, site);
		var pageAdded = pageService.addPage(page);
		
		return new PageHateoasModelAssembler().toModel(pageAdded);
	}
	
	@PutMapping("/{pageId}")
	public PageHateoasModel putPage(@RequestBody PageDTO pageDTO) {
		var site = siteService.getSiteById(pageDTO.getSiteId());
		var page = ConvertPage.toEntity(pageDTO, site);
		var pageAdded = pageService.addPage(page);
		
		return new PageHateoasModelAssembler().toModel(pageAdded);
	}

	@PatchMapping(path = "/{pageId}", consumes = "application/json")
	public PageHateoasModel patchPage(
			@PathVariable("pageId") Long pageId
			, @RequestBody PageDTO pageDTO) {
		var page = pageService.getPageById(pageId);
		
		if (pageDTO.getSiteId() != null) {
			var siteById = siteService.getSiteById(pageDTO.getSiteId());
			page.setSite(siteById);
		}
		
		if (pageDTO.getPageUrl() != null) {
			page.setUrl(pageDTO.getPageUrl());
		}
		
		if (pageDTO.getRegistered() != null) {
			page.setRegistered(pageDTO.getRegistered());
		}
		
		if (pageDTO.getLastScan() != null) {
			page.setLastScan(pageDTO.getLastScan());
		}
		
		var pageAdded = pageService.addPage(page);

		return new PageHateoasModelAssembler().toModel(pageAdded);
	}

	@DeleteMapping("/{pageId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deletePage(@PathVariable("pageId") Long pageId) {
		try {
			pageService.deletePageById(pageId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
