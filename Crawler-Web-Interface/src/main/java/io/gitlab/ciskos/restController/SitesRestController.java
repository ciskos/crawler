package io.gitlab.ciskos.restController;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.converter.ConvertSite;
import io.gitlab.ciskos.dto.SiteDTO;
import io.gitlab.ciskos.hateoas.assembler.SiteHateoasModelAssembler;
import io.gitlab.ciskos.hateoas.model.SiteHateoasModel;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@RestController
@RequestMapping(value = SitesRestController.SITE_PATH, produces = "application/json")
//@CrossOrigin(origins = "*")
public class SitesRestController {
	

	static final String SITE_PATH = "/sites";
	private static final String HATEOAS_ALL_SITES = "allSites";
	private static final String HATEOAS_SITES_PAGE = "sitesPage";

	private SiteService siteService;

	public SitesRestController(SiteService siteService) {
		this.siteService = siteService;
	}

	@GetMapping
	public CollectionModel<SiteHateoasModel> getAllSites() {
		var sites = (List<Site>) siteService.getAllSites();
		var hateoas = new SiteHateoasModelAssembler().toCollectionModel(sites);
		
		hateoas.add(linkTo(methodOn(SitesRestController.class).getAllSites())
				.withRel(HATEOAS_ALL_SITES));

		return hateoas;
	}

	@GetMapping("/page/{pageNumber}")
	public Iterable<SiteHateoasModel> getAllSitesByPage(@PathVariable("pageNumber") Integer pageNumber) {
		var sitesPage = siteService.getAllSitesPage(pageNumber);
		var hateoas = new SiteHateoasModelAssembler().toCollectionModel(sitesPage);
		
		hateoas.add(linkTo(methodOn(SitesRestController.class).getAllSitesByPage(pageNumber))
				.withRel(HATEOAS_SITES_PAGE + pageNumber));
		
		return hateoas;
	}

	@GetMapping("/{id}")
	public ResponseEntity<SiteHateoasModel> getSiteById(@PathVariable("id") Long id) {
		var site = siteService.getSiteById(id);
		
		if (site.getId() != null) {
			var hateoas = new SiteHateoasModelAssembler().toModel(site);
			return new ResponseEntity<>(hateoas, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public SiteHateoasModel addSite(@RequestBody SiteDTO siteDTO) {
		var site = ConvertSite.toEntity(siteDTO);
		var siteAdded = siteService.addSite(site);
		
		return new SiteHateoasModelAssembler().toModel(siteAdded);
	}
	
	@PutMapping("/{siteId}")
	public SiteHateoasModel putSite(@RequestBody SiteDTO siteDTO) {
		var site = ConvertSite.toEntity(siteDTO);
		var siteAdded = siteService.addSite(site);
		
		return new SiteHateoasModelAssembler().toModel(siteAdded);
	}

	@PatchMapping(path = "/{siteId}", consumes = "application/json")
	public SiteHateoasModel patchSite(
			@PathVariable("siteId") Long siteId
			, @RequestBody SiteDTO siteDTO) {
		var site = siteService.getSiteById(siteId);
		
		if (siteDTO.getUrl() != null) {
			site.setUrl(siteDTO.getUrl());
		}
		
		if (siteDTO.getDescription() != null) {
			site.setDescription(siteDTO.getDescription());
		}
		
		var siteAdded = siteService.addSite(site);
		
		return new SiteHateoasModelAssembler().toModel(siteAdded);
	}

	@DeleteMapping("/{siteId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteSite(@PathVariable("siteId") Long siteId) {
		try {
			siteService.deleteSiteById(siteId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
