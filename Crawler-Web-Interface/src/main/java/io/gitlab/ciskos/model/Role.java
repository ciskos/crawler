package io.gitlab.ciskos.model;

public enum Role {

	ROLE_ADMIN
	, ROLE_USER

}
