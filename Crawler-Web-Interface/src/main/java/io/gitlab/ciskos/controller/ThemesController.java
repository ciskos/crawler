package io.gitlab.ciskos.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertTheme;
import io.gitlab.ciskos.dto.ThemeDTO;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.ThemeService;

@Controller
@RequestMapping(value = ThemesController.THEMES_PATH)
@SessionAttributes(value = "themeToUpdate")
public class ThemesController {

	static final String THEMES_PATH = "/themes";
	
	private ThemeService themeService;

	public ThemesController(ThemeService themeService) {
		this.themeService = themeService;
	}

	@ModelAttribute(value = "themeToUpdate")
	public ThemeDTO themeDTO() {
		return new ThemeDTO();
	}
	
	@GetMapping
	public String getAllThemes(Model model) {
		List<Theme> themes = (List<Theme>) themeService.getAllThemes();
		
		model.addAttribute("themes", themes.stream()
										.map(t -> ConvertTheme.toDto(t))
										.collect(Collectors.toList()));
		
		return "get" + THEMES_PATH;
	}

	@GetMapping(value = "/{id}/delete")
	public String deleteThemeById(@PathVariable Long id) {
		themeService.deleteThemeById(id);
		
		return "redirect:" + THEMES_PATH;
	}
	
	@GetMapping(value = "/{id}/update")
	public String initUpdateThemeById(@PathVariable Long id, Model model) {
		Theme theme = themeService.getThemeById(id);

		if (theme.getId() != null) {
			model.addAttribute("themeToUpdate", ConvertTheme.toDto(theme));
			
			return "update/theme";
		}
		
		return "redirect:" + THEMES_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdateThemeById(@Valid @ModelAttribute("themeToUpdate") ThemeDTO themeDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("themeToUpdate", themeDTO);
			
			return "update/theme";
		}
		
		themeService.updateTheme(ConvertTheme.toEntity(themeDTO));
		sessionStatus.setComplete();
		
		return "redirect:" + THEMES_PATH;
	}

	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("themeToUpdate", new ThemeDTO());

		return "/add/theme";
	}
	
	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("themeToUpdate") ThemeDTO themeDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("themeToUpdate", themeDTO);
			
			return "add/theme";
		}
		
		themeService.addTheme(ConvertTheme.toEntity(themeDTO));
		sessionStatus.setComplete();
		
		return "redirect:" + THEMES_PATH;
	}

}
