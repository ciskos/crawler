package io.gitlab.ciskos.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.gitlab.ciskos.converter.ConvertMatch;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.service.MatchService;

@Controller
@RequestMapping(value = MatchesController.MATCHES_PATH)
public class MatchesController {

	static final String MATCHES_PATH = "/matches";
	
	private MatchService matchService;

	public MatchesController(MatchService matchService) {
		this.matchService = matchService;
	}
	
	@GetMapping
	public String getAllMatches(Model model) {
		List<Match> matches = (List<Match>) matchService.getAllMatches();
		
		model.addAttribute("matches", matches.stream()
										.map(m -> ConvertMatch.toDto(m))
										.collect(Collectors.toList()));
		
		return "get" + MATCHES_PATH;
	}
	
	@GetMapping(value = "/{id}/delete")
	public String deleteMatchById(@PathVariable Long id) {
		matchService.deleteMatchById(id);
		
		return "redirect:" + MATCHES_PATH;
	}
	
	/* TODO нужны ли эти методы вообще?
	 * @GetMapping(value = "/{id}/update") public String
	 * initUpdateMatchById(@PathVariable Long id, Model model) { Optional<Match>
	 * match = matchService.getMatchById(id);
	 * 
	 * if (match.isPresent()) { model.addAttribute("match", match.get());
	 * 
	 * return "update/match"; }
	 * 
	 * return "redirect:get" + MATCHES_PATH; }
	 * 
	 * @PostMapping(value = "/{id}/update") public String
	 * processUpdateMatchById(@PathVariable Long id) { 
	 * "redirect:get" + MATCHES_PATH; }
	 */

}
