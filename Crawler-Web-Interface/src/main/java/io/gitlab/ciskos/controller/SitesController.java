package io.gitlab.ciskos.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertSite;
import io.gitlab.ciskos.dto.SiteDTO;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.SiteService;

@Controller
@RequestMapping(value = SitesController.SITES_PATH)
@SessionAttributes(value = "siteToUpdate")
public class SitesController {

	static final String SITES_PATH = "/sites";
	
	private SiteService siteService;

	public SitesController(SiteService siteService) {
		this.siteService = siteService;
	}

	@ModelAttribute(value = "siteToUpdate")
	public SiteDTO siteDTO() {
		return new SiteDTO();
	}
	
	@GetMapping
	public String getAllSites(Model model) {
		List<Site> sites = (List<Site>) siteService.getAllSites();
		
		model.addAttribute("sites", sites.stream()
										.map(s -> ConvertSite.toDto(s))
										.collect(Collectors.toList()));
		
		return "get" + SITES_PATH;
	}
	
	@GetMapping(value = "/{id}/delete")
	public String deleteSiteById(@PathVariable Long id) {
		siteService.deleteSiteById(id);
		
		return "redirect:" + SITES_PATH;
	}
	
	@GetMapping(value = "/{id}/update")
	public String initUpdateSiteById(@PathVariable Long id, Model model) {
		Site site = siteService.getSiteById(id);

		if (site.getId() != null) {
			model.addAttribute("siteToUpdate", ConvertSite.toDto(site));
			
			return "update/site";
		}
		
		return "redirect:" + SITES_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdateSiteById(@Valid @ModelAttribute("siteToUpdate") SiteDTO siteDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("siteToUpdate", siteDTO);
			
			return "update/site";
		}
		
		siteService.updateSite(ConvertSite.toEntity(siteDTO));
		sessionStatus.setComplete();
		
		return "redirect:" + SITES_PATH;
	}

	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("siteToUpdate", new SiteDTO());

		return "/add/site";
	}

	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("siteToUpdate") SiteDTO siteDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("siteToUpdate", siteDTO);
			
			return "add/site";
		}
		
		siteService.addSite(ConvertSite.toEntity(siteDTO));
		sessionStatus.setComplete();
		
		return "redirect:" + SITES_PATH;
	}

}
