package io.gitlab.ciskos.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertKeyword;
import io.gitlab.ciskos.dto.KeywordDTO;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.service.KeywordService;
import io.gitlab.ciskos.service.ThemeService;

@Controller
@RequestMapping(value = KeywordsController.KEYWORDS_PATH)
@SessionAttributes(value = "keywordToUpdate")
public class KeywordsController {

	static final String KEYWORDS_PATH = "/keywords";
	
	private KeywordService keywordService;
	private ThemeService themeService;

	public KeywordsController(KeywordService keywordService, ThemeService themeService) {
		this.keywordService = keywordService;
		this.themeService = themeService;
	}
	
	@ModelAttribute(value = "keywordToUpdate")
	public KeywordDTO keywordDTO() {
		return new KeywordDTO();
	}

	@GetMapping
	public String getAllKeywords(Model model) {
		List<Keyword> keywords = (List<Keyword>) keywordService.getAllKeywords();
		
		model.addAttribute("keywords", keywords.stream()
										.map(k -> ConvertKeyword.toDto(k))
										.collect(Collectors.toList()));
		
		return "get" + KEYWORDS_PATH;
	}
	
	@GetMapping(value = "/{id}/delete")
	public String deleteKeywordById(@PathVariable Long id) {
		keywordService.deleteKeywordById(id);
		
		return "redirect:" + KEYWORDS_PATH;
	}

	@GetMapping(value = "/{id}/update")
	public String initUpdateKeywordById(@PathVariable Long id, Model model) {
		Keyword keyword = keywordService.getKeywordById(id);

		if (keyword.getId() != null) {
			model.addAttribute("keywordToUpdate", ConvertKeyword.toDto(keyword));
			
			return "update/keyword";
		}
		
		return "redirect:" + KEYWORDS_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdateKeywordById(@Valid @ModelAttribute("keywordToUpdate") KeywordDTO keywordDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("keywordToUpdate", keywordDTO);
			
			return "update/keyword";
		}
		
		Theme theme = themeService.getThemeById(keywordDTO.getThemeId());
		
		keywordService.updateKeyword(ConvertKeyword.toEntity(keywordDTO, theme));
		sessionStatus.setComplete();

		return "redirect:" + KEYWORDS_PATH;
	}
	
	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("keywordToUpdate", new KeywordDTO());
		model.addAttribute("themes", themeService.getAllThemes());

		return "/add/keyword";
	}
	
	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("keywordToUpdate") KeywordDTO keywordDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("keywordToUpdate", keywordDTO);
			model.addAttribute("themes", themeService.getAllThemes());
			
			return "add/keyword";
		}
		
		Theme theme = themeService.getThemeById(keywordDTO.getThemeId());
		
		keywordService.addKeyword(ConvertKeyword.toEntity(keywordDTO, theme));
		sessionStatus.setComplete();
		
		return "redirect:" + KEYWORDS_PATH;
	}

}
