package io.gitlab.ciskos.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertAuthority;
import io.gitlab.ciskos.dto.AuthorityDTO;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@Controller
@RequestMapping(value = AuthoritiesController.AUTHORITIES_PATH)
@SessionAttributes(value = "authorityToUpdate")
public class AuthoritiesController {
	
	static final String AUTHORITIES_PATH = "/authorities";

	private AuthorityService authorityService;
	private UserService userService;

	public AuthoritiesController(AuthorityService authorityService, UserService userService) {
		this.authorityService = authorityService;
		this.userService = userService;
	}

	@ModelAttribute(value = "authorityToUpdate")
	public AuthorityDTO authorityDTO() {
		return new AuthorityDTO();
	}

	@GetMapping
	public String getAllAuthorities(Model model) {
		List<Authority> authorities = (List<Authority>) authorityService.getAllAuthorities();

		model.addAttribute("authorities", authorities.stream()
											.map(a -> ConvertAuthority.toDto(a))
											.collect(Collectors.toList()));

		return "get" + AUTHORITIES_PATH;
	}

	@GetMapping(value = "/{id}/delete")
	public String deleteAuthorityById(@PathVariable Long id) {
		authorityService.deleteAuthorityById(id);

		return "redirect:" + AUTHORITIES_PATH;
	}

	@GetMapping(value = "/{id}/update")
	public String initUpdateAuthorityById(@PathVariable Long id, Model model) {
		Authority authority = authorityService.getAuthorityById(id);

		if (authority.getId() != null) {
			model.addAttribute("authorityToUpdate", ConvertAuthority.toDto(authority));
			model.addAttribute("roles", Arrays.asList(Role.values()));
			
			return "update/authority";
		}
		
		return "redirect:" + AUTHORITIES_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdateAuthorityById(@Valid @ModelAttribute("authorityToUpdate") AuthorityDTO authorityDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("authorityToUpdate", authorityDTO);
			
			return "update/authority";
		}
		
		User user = userService.getUserById(authorityDTO.getUserId());
		
		authorityService.updateAuthority(ConvertAuthority.toEntity(authorityDTO, user));
		sessionStatus.setComplete();

		return "redirect:" + AUTHORITIES_PATH;
	}

	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("authorityToUpdate", new AuthorityDTO());
		model.addAttribute("users", userService.getAllUsers());
		model.addAttribute("roles", Arrays.asList(Role.values()));
		
		return "/add/authority";
	}

	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("authorityToUpdate") AuthorityDTO authorityDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("authorityToUpdate", authorityDTO);
			model.addAttribute("users", userService.getAllUsers());
			model.addAttribute("roles", Arrays.asList(Role.values()));
			
			return "add/authority";
		}
		
		User user = userService.getUserById(authorityDTO.getUserId());
		
		authorityService.addAuthority(ConvertAuthority.toEntity(authorityDTO, user));
		sessionStatus.setComplete();
		
		return "redirect:" + AUTHORITIES_PATH;
	}

}
