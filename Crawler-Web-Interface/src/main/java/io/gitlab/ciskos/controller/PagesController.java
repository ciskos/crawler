package io.gitlab.ciskos.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertPage;
import io.gitlab.ciskos.dto.PageDTO;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.service.PageService;
import io.gitlab.ciskos.service.SiteService;

@Controller
@RequestMapping(value = PagesController.PAGES_PATH)
@SessionAttributes(value = "pageToUpdate")
public class PagesController {

	static final String PAGES_PATH = "/pages";
	
	private PageService pageService;
	private SiteService siteService;

	public PagesController(PageService pageService, SiteService siteService) {
		this.pageService = pageService;
		this.siteService = siteService;
	}

	@ModelAttribute(value = "pageToUpdate")
	public PageDTO pageDTO() {
		return new PageDTO();
	}

	@GetMapping
	public String getAllPages(Model model) {
		List<Page> pages = (List<Page>) pageService.getAllPages();
		
		model.addAttribute("pages", pages.stream()
										.map(p -> ConvertPage.toDto(p))
										.collect(Collectors.toList()));
		
		return "get" + PAGES_PATH;
	}

	@GetMapping(value = "/{id}/delete")
	public String deletePageById(@PathVariable Long id) {
		pageService.deletePageById(id);
		
		return "redirect:" + PAGES_PATH;
	}

	@GetMapping(value = "/{id}/update")
	public String initUpdatePageById(@PathVariable Long id, Model model) {
		Page page = pageService.getPageById(id);

		if (page.getId() != null) {
			model.addAttribute("pageToUpdate", ConvertPage.toDto(page));
			
			return "update/page";
		}
		
		return "redirect:" + PAGES_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdatePageById(@Valid @ModelAttribute("pageToUpdate") PageDTO pageDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("pageToUpdate", pageDTO);
			model.addAttribute("sites", siteService.getAllSites());
			
			return "update/page";
		}
		
		Site site = siteService.getSiteById(pageDTO.getSiteId()); 
		
		pageService.updatePage(ConvertPage.toEntity(pageDTO, site));
		sessionStatus.setComplete();
		
		return "redirect:" + PAGES_PATH;
	}
	
	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("pageToUpdate", new PageDTO());
		model.addAttribute("sites", siteService.getAllSites());
		
		return "/add/page";
	}
	
	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("pageToUpdate") PageDTO pageDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("pageToUpdate", pageDTO);
			model.addAttribute("sites", siteService.getAllSites());
			
			return "add/page";
		}
		
		pageDTO.setRegistered(LocalDateTime.now());
		pageDTO.setLastScan(LocalDateTime.MIN);
		
		Site site = siteService.getSiteById(pageDTO.getSiteId()); 
		
		pageService.addPage(ConvertPage.toEntity(pageDTO, site));
		sessionStatus.setComplete();
		
		return "redirect:" + PAGES_PATH;
	}

}
