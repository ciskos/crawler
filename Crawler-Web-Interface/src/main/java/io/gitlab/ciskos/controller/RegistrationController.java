package io.gitlab.ciskos.controller;

import javax.validation.Valid;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertRegistration;
import io.gitlab.ciskos.dto.RegistrationDTO;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@Controller
@RequestMapping(value = RegistrationController.REGISTRATION_PATH)
public class RegistrationController {

	static final String REGISTRATION_PATH="/registration";
	
	private UserService userService;
	private AuthorityService authorityService;

	public RegistrationController(UserService userService, AuthorityService authorityService) {
		this.userService = userService;
		this.authorityService = authorityService;
	}
	
	@GetMapping
	public String initCreationForm(Model model) {
		model.addAttribute("user", new RegistrationDTO());

		return "/registration";
	}
	
	@PostMapping
	public String processCreationForm(@Valid @ModelAttribute("user") RegistrationDTO registrationDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus) {
		if (bindingResult.hasErrors()) {
			return "/registration";
		}

		// TODO добавить сверку вводимых паролей
//		if (!registrationDTO.getPassword().equals(registrationDTO.getConfirmPassword())) {
//			model.addAttribute("user", registrationDTO);
//			System.out.println("registration");
//
//			return "/registration";
//		}
		
		var encodePassword = new BCryptPasswordEncoder().encode(registrationDTO.getPassword());
		registrationDTO.setPassword(encodePassword);
		
		User u = userService.addUser(ConvertRegistration.toEntity(registrationDTO));
		
		Authority authority = new Authority();
		authority.setUser(u);
		authority.setRole(Role.ROLE_USER);
		
		authorityService.addAuthority(authority);
		sessionStatus.setComplete();
		
		return "redirect:/login";
	}

}
