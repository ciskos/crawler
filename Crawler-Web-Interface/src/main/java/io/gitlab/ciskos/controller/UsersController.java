package io.gitlab.ciskos.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import io.gitlab.ciskos.converter.ConvertUser;
import io.gitlab.ciskos.dto.UserDTO;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.service.AuthorityService;
import io.gitlab.ciskos.service.UserService;

@Controller
@RequestMapping(value = UsersController.USERS_PATH)
@SessionAttributes(value = "userToUpdate")
public class UsersController {

	static final String USERS_PATH="/users";
	
	private UserService userService;
	private AuthorityService authorityService;

	public UsersController(UserService userService, AuthorityService authorityService) {
		this.userService = userService;
		this.authorityService = authorityService;
	}

	@ModelAttribute(value = "userToUpdate")
	public UserDTO userDTO() {
		return new UserDTO();
	}
	
	@GetMapping
	public String getAllUsers(Model model) {
		List<User> users = (List<User>) userService.getAllUsers();
		
		model.addAttribute("users", users.stream()
									.map(u -> ConvertUser.toDto(u))
									.collect(Collectors.toList()));
		
		return "get" + USERS_PATH;
	}
	
	@GetMapping(value = "/{id}/delete")
	public String deleteUserById(@PathVariable Long id) {
		userService.deleteUserById(id);
		
		return "redirect:" + USERS_PATH;
	}
	
	@GetMapping(value = "/{id}/update")
	public String initUpdateUserById(@PathVariable Long id, Model model) {
		User user = userService.getUserById(id);

		if (user.getId() != null) {
			model.addAttribute("userToUpdate", ConvertUser.toDto(user));
			
			return "update/user";
		}
		
		return "redirect:" + USERS_PATH;
	}
	
	@PostMapping(value = "/{id}/update")
	public String processUpdateUserById(@Valid @ModelAttribute("userToUpdate") UserDTO userDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus
			, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("userToUpdate", userDTO);
			
			return "update/user";
		}
		
		userService.updateUser(ConvertUser.toEntity(userDTO));
		sessionStatus.setComplete();

		return "redirect:" + USERS_PATH;
	}
	
	@GetMapping(value = "/add")
	public String initCreationForm(Model model) {
		model.addAttribute("userToUpdate", new UserDTO());

		return "/add/user";
	}
	
	@PostMapping(value = "/add")
	public String processCreationForm(@Valid @ModelAttribute("userToUpdate") UserDTO userDTO
			, BindingResult bindingResult
			, SessionStatus sessionStatus) {
		if (bindingResult.hasErrors()) {
			return "add/user";
		}
		
		userDTO.setEnabled(true);
		userDTO.setRegistered(LocalDateTime.now());
		
		User u = userService.addUser(ConvertUser.toEntity(userDTO));
		
		Authority authority = new Authority();
		authority.setUser(u);
		authority.setRole(Role.ROLE_USER);
		
		authorityService.addAuthority(authority);
		sessionStatus.setComplete();
		
		return "redirect:" + USERS_PATH;
	}

}
