package io.gitlab.ciskos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrawlerWebInterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrawlerWebInterfaceApplication.class, args);
	}

}
