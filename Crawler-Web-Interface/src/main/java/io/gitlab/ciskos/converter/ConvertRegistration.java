package io.gitlab.ciskos.converter;

import java.time.LocalDateTime;

import io.gitlab.ciskos.dto.RegistrationDTO;
import io.gitlab.ciskos.model.User;

public class ConvertRegistration {
	
	public static RegistrationDTO toDto(User user) {
		return new RegistrationDTO(user.getUsername()
							, user.getPassword()
//							, user.getPassword()	// TODO добавить поле для сверки паролей
							, user.getEmail());
	}

	public static User toEntity(RegistrationDTO registrationDTO) {
		return new User(null
						, registrationDTO.getUsername()
						, registrationDTO.getPassword()
						, registrationDTO.getEmail()
						, Boolean.TRUE
						, LocalDateTime.now());
	}

}
