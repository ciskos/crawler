package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.UserDTO;
import io.gitlab.ciskos.model.User;

public class ConvertUser {
	
	public static UserDTO toDto(User user) {
		return new UserDTO(user.getId()
							, user.getUsername()
							, user.getPassword()
							, user.getEmail()
							, user.getEnabled()
							, user.getRegistered());
	}

	public static User toEntity(UserDTO userDTO) {
		return new User(userDTO.getId()
						, userDTO.getUsername()
						, userDTO.getPassword()
						, userDTO.getEmail()
						, userDTO.getEnabled()
						, userDTO.getRegistered());
	}

}
