package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.ThemeDTO;
import io.gitlab.ciskos.model.Theme;

public class ConvertTheme {
	
	public static ThemeDTO toDto(Theme theme) {
		return new ThemeDTO(theme.getId()
							, theme.getName());
	}

	public static Theme toEntity(ThemeDTO themeDTO) {
		return new Theme(themeDTO.getId()
						, themeDTO.getName());
	}

}
