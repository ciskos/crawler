package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.SiteDTO;
import io.gitlab.ciskos.model.Site;

public class ConvertSite {
	
	public static SiteDTO toDto(Site site) {
		return new SiteDTO(site.getId()
							, site.getUrl()
							, site.getDescription());
	}

	public static Site toEntity(SiteDTO siteDTO) {
		return new Site(siteDTO.getId()
						, siteDTO.getUrl()
						, siteDTO.getDescription());
	}

}
