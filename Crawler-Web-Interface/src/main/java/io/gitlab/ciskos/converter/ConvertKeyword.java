package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.KeywordDTO;
import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.model.Theme;

public class ConvertKeyword {
	
	public static KeywordDTO toDto(Keyword keyword) {
		return new KeywordDTO(keyword.getId()
							, keyword.getTheme().getId()
							, keyword.getTheme().getName()
							, keyword.getWord());
	}

	public static Keyword toEntity(KeywordDTO keywordDTO, Theme theme) {
		return new Keyword(keywordDTO.getId()
							, theme
							, keywordDTO.getKeyword());
	}

}
