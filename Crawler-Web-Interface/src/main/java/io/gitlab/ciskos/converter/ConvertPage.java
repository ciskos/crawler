package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.PageDTO;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Site;

public class ConvertPage {
	
	public static PageDTO toDto(Page page) {
		return new PageDTO(page.getId()
							, page.getSite().getId()
							, page.getSite().getUrl()
							, page.getUrl()
							, page.getRegistered()
							, page.getLastScan());
	}

	public static Page toEntity(PageDTO pageDTO, Site site) {
		return new Page(pageDTO.getId()
						, site
						, pageDTO.getPageUrl()
						, pageDTO.getRegistered()
						, pageDTO.getLastScan());
	}

}
