package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.AuthorityDTO;
import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.model.Role;
import io.gitlab.ciskos.model.User;

public class ConvertAuthority {
	
	public static AuthorityDTO toDto(Authority authority) {
		return new AuthorityDTO(authority.getId()
								, authority.getUser().getId()
								, authority.getUser().getUsername()
								, authority.getRole().name());
	}

	public static Authority toEntity(AuthorityDTO authorityDTO, User user) {
		return new Authority(authorityDTO.getId()
							, user
							, Role.valueOf(authorityDTO.getRole()));
	}

}
