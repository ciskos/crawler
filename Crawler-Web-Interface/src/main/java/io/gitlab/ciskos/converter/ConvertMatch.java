package io.gitlab.ciskos.converter;

import io.gitlab.ciskos.dto.MatchDTO;
import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.model.Theme;

public class ConvertMatch {
	
	public static MatchDTO toDto(Match match) {
		return new MatchDTO(match.getId()
							, match.getTheme().getId()
							, match.getPage().getId()
							, match.getTheme().getName()
							, match.getPage().getUrl()
							, match.getCounts());
	}

	public static Match toEntity(MatchDTO matchDTO, Theme theme, Page page) {
		return new Match(matchDTO.getId()
						, theme
						, page
						, matchDTO.getCounts());
	}

}
