package io.gitlab.ciskos.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThemeDTO {

	private Long id;

	@NotBlank(message = "Theme must not be blank.")
	@Size(min = 3, max = 255, message = "Theme must be minimum 3 and maximum 255.")
	private String name;
	
}
