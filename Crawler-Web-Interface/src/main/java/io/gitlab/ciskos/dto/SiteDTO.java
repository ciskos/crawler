package io.gitlab.ciskos.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SiteDTO {

	private Long id;

	@NotBlank(message = "Url must not be blank.")
	@Size(min = 3, max = 255, message = "Url must be minimum 3 and maximum 255.")
	private String url;
	
	@NotBlank(message = "Description must not be blank.")
	@Size(min = 3, max = 255, message = "Description must be minimum 3 and maximum 255.")
	private String description;
	
}
