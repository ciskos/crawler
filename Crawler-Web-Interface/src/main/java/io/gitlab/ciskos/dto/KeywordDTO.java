package io.gitlab.ciskos.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeywordDTO {

	private Long id;

	@NotNull(message = "Theme must not be blank.")
	private Long themeId;

	private String themeName;
	
	@NotBlank(message = "Keyword must not be blank.")
	@Size(min = 3, max = 255, message = "Keyword must be minimum 3 and maximum 255.")
	private String keyword;
	
}
