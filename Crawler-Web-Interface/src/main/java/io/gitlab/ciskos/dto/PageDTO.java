package io.gitlab.ciskos.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageDTO {

	private Long id;

	@NotNull(message = "Site must not be blank.")
	private Long siteId;

	private String siteUrl;

	@NotBlank(message = "Url must not be blank.")
	@Size(min = 3, max = 255, message = "Url must be minimum 3 and maximum 255.")
	private String pageUrl;
	
	private LocalDateTime registered;
	
	private LocalDateTime lastScan;
	
}
