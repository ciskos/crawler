package io.gitlab.ciskos.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityDTO {

	private Long id;
	
	@NotNull(message = "User must not be blank.")
	private Long userId;
	
	private String username;

	@NotBlank(message = "Role must not be blank.")
	private String role;
	
}
