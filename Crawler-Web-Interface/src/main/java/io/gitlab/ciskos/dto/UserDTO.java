package io.gitlab.ciskos.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

	private Long id;
	
	@NotBlank(message = "Username must not be blank.")
	@Size(min = 3, max = 255, message = "Username must be minimum 3 and maximum 255.")
	private String username;
	
	@NotBlank(message = "Password must not be blank.")
	@Size(min = 8, max = 255, message = "Password must be minimum 8 and maximum 255.")
	private String password;
	
	@NotBlank(message = "E-mail must not be blank.")
	@Size(min = 8, max = 255, message = "E-mail must be minimum 8 and maximum 255.")
	@Email(message = "Must be e-mail.")
	private String email;
	
	private Boolean enabled;
	
	private LocalDateTime registered;

}
