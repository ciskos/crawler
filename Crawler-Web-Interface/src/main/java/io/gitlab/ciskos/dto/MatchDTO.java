package io.gitlab.ciskos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatchDTO {

	private Long id;

	private Long themeId;

	private Long pageId;

	private String themeName;

	private String pageName;
	
	private Long counts;

}
