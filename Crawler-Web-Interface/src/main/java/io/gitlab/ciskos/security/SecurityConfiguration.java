package io.gitlab.ciskos.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String USERS = "SELECT"
											+ " u.username"
											+ " , u.password"
											+ " , u.enabled"
										+ " FROM"
											+ " USERS u"
										+ " WHERE"
											+ " u.username=?";
	private static final String AUTHORITIES = " SELECT"
												+ "	u.username"
												+ "	, a.role"
											+ " FROM"
												+ "	USERS u"
												+ "	LEFT JOIN AUTHORITIES a ON a.user_id=u.id"
											+ " WHERE"
												+ "	u.username=?";
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.jdbcAuthentication()
				.dataSource(dataSource)
				.usersByUsernameQuery(USERS)
				.authoritiesByUsernameQuery(AUTHORITIES)
				.passwordEncoder(encoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
//				.antMatchers("/keywords", "/pages", "/authorities", "/sites", "/themes", "/matches", "/users")
//					.hasRole("USER")
//				.antMatchers("/keywords/**", "/pages/**", "/authorities/**", "/sites/**", "/themes/**", "/matches/**", "/users/**")
//					.hasRole("ADMIN")
				.antMatchers("/", "/**").permitAll()
			.and()
				.formLogin()
					.loginPage("/login")
//					.loginProcessingUrl("/auth")
			.and()
				.logout()
					.logoutSuccessUrl("/")
			.and()
				.csrf().disable()
//			.and()
//				.csrf().ignoringAntMatchers("/h2-console/**")
//			.and()
//				.headers().frameOptions().sameOrigin()
			;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring()
				.antMatchers("/h2-console/**");
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
	
}
