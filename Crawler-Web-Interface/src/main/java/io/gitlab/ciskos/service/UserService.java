package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.User;
import io.gitlab.ciskos.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

	public List<User> getAllUsersPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("username").ascending());
		
		return userRepository.findAll(page).getContent();
	}

	public void deleteUserById(Long userId) {
		userRepository.deleteById(userId);
	}
	
	public User updateUser(User user) {
		return userRepository.save(user);
	}
	
	public User getUserById(Long userId) {
		return userRepository.findById(userId).orElse(new User());
	}
	
	public User addUser(User user) {
		return userRepository.save(user);
	}
	
}
