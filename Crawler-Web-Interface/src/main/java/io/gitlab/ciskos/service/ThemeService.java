package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Theme;
import io.gitlab.ciskos.repository.ThemeRepository;

@Service
public class ThemeService {

	private ThemeRepository themeRepository;

	public ThemeService(ThemeRepository themeRepository) {
		this.themeRepository = themeRepository;
	}

	public Iterable<Theme> getAllThemes() {
		return themeRepository.findAll();
	}

	public List<Theme> getAllThemesPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("name").ascending());
		
		return themeRepository.findAll(page).getContent();
	}
	
	public void deleteThemeById(Long themeId) {
		themeRepository.deleteById(themeId);
	}
	
	public Theme updateTheme(Theme theme) {
		return themeRepository.save(theme);
	}
	
	public Theme getThemeById(Long themeId) {
		return themeRepository.findById(themeId).orElse(new Theme());
	}

	public Theme addTheme(Theme theme) {
		return themeRepository.save(theme);
	}

}
