package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Site;
import io.gitlab.ciskos.repository.SiteRepository;

@Service
public class SiteService {

	private SiteRepository siteRepository;

	public SiteService(SiteRepository siteRepository) {
		this.siteRepository = siteRepository;
	}

	public Iterable<Site> getAllSites() {
		return siteRepository.findAll();
	}

	public List<Site> getAllSitesPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("url").ascending());
		
		return siteRepository.findAll(page).getContent();
	}
	
	public void deleteSiteById(Long siteId) {
		siteRepository.deleteById(siteId);
	}
	
	public Site updateSite(Site site) {
		return siteRepository.save(site);
	}
	
	public Site getSiteById(Long siteId) {
		return siteRepository.findById(siteId).orElse(new Site());
	}

	public Site addSite(Site site) {
		return siteRepository.save(site);
	}

}
