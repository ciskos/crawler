package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Match;
import io.gitlab.ciskos.repository.MatchRepository;

@Service
public class MatchService {

	private MatchRepository matchRepository;

	public MatchService(MatchRepository matchRepository) {
		this.matchRepository = matchRepository;
	}

	public Iterable<Match> getAllMatches() {
		return matchRepository.findAll();
	}

	public List<Match> getAllMatchesPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("theme_id").ascending());
		
		return matchRepository.findAll(page).getContent();
	}
	
	public void deleteMatchById(Long matchId) {
		matchRepository.deleteById(matchId);
	}
	
	public Match updateMatch(Match match) {
		return matchRepository.save(match);
	}
	
	public Match getMatchById(Long matchId) {
		return matchRepository.findById(matchId).orElse(new Match());
	}

	public Match addMatch(Match match) {
		return matchRepository.save(match);
	}

}
