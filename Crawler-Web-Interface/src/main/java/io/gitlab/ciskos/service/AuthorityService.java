package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Authority;
import io.gitlab.ciskos.repository.AuthorityRepository;

@Service
public class AuthorityService {

	private AuthorityRepository authorityRepository;

	public AuthorityService(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;
	}

	public Iterable<Authority> getAllAuthorities() {
		return authorityRepository.findAll();
	}

	public List<Authority> getAllAuthoritiesPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("role").ascending());
		
		return authorityRepository.findAll(page).getContent();
	}
	
	public void deleteAuthorityById(Long authorityId) {
		authorityRepository.deleteById(authorityId);
	}
	
	public Authority updateAuthority(Authority authority) {
		return authorityRepository.save(authority);
	}
	
	public Authority getAuthorityById(Long authorityId) {
		return authorityRepository.findById(authorityId).orElse(new Authority());
	}
	
	public Authority addAuthority(Authority authority) {
		return authorityRepository.save(authority);
	}

}
