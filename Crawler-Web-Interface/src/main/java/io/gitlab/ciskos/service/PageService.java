package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Page;
import io.gitlab.ciskos.repository.PageRepository;

@Service
public class PageService {

	private PageRepository pageRepository;

	public PageService(PageRepository pageRepository) {
		this.pageRepository = pageRepository;
	}

	public Iterable<Page> getAllPages() {
		return pageRepository.findAll();
	}

	public List<Page> getAllPagesPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("url").ascending());
		
		return pageRepository.findAll(page).getContent();
	}
	
	public void deletePageById(Long pageId) {
		pageRepository.deleteById(pageId);
	}
	
	public Page updatePage(Page page) {
		return pageRepository.save(page);
	}
	
	public Page getPageById(Long pageId) {
		return pageRepository.findById(pageId).orElse(new Page());
	}

	public Page addPage(Page page) {
		return pageRepository.save(page);
	}

}
