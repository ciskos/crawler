package io.gitlab.ciskos.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import io.gitlab.ciskos.model.Keyword;
import io.gitlab.ciskos.repository.KeywordRepository;

@Service
public class KeywordService {

	private KeywordRepository keywordRepository;

	public KeywordService(KeywordRepository keywordRepository) {
		this.keywordRepository = keywordRepository;
	}

	public Iterable<Keyword> getAllKeywords() {
		return keywordRepository.findAll();
	}

	public List<Keyword> getAllKeywordsPage(Integer pageNumber) {
		if (pageNumber == null || pageNumber < 1) return null;
		
		var lastPage = 10 * pageNumber;
		var firstPage = lastPage - 10;
		var page = PageRequest.of(firstPage, lastPage, Sort.by("word").ascending());
		
		return keywordRepository.findAll(page).getContent();
	}
	
	public void deleteKeywordById(Long keywordId) {
		keywordRepository.deleteById(keywordId);
	}
	
	public Keyword updateKeyword(Keyword keyword) {
		return keywordRepository.save(keyword);
	}
	
	public Keyword getKeywordById(Long keywordId) {
		return keywordRepository.findById(keywordId).orElse(new Keyword());
	}
	
	public Keyword addKeyword(Keyword keyword) {
		return keywordRepository.save(keyword);
	}

}
