package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Page;

public interface PageRepository extends PagingAndSortingRepository<Page, Long> {

}
