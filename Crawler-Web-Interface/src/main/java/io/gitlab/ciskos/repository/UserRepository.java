package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

}
