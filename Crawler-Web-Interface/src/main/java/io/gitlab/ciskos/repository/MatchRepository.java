package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Match;

public interface MatchRepository extends PagingAndSortingRepository<Match, Long> {

}
