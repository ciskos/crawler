package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Authority;

public interface AuthorityRepository extends PagingAndSortingRepository<Authority, Long> {

}
