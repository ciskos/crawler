package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Theme;

public interface ThemeRepository extends PagingAndSortingRepository<Theme, Long> {

}
