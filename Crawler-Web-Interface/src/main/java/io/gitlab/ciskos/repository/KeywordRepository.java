package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Keyword;

public interface KeywordRepository extends PagingAndSortingRepository<Keyword, Long> {

}
