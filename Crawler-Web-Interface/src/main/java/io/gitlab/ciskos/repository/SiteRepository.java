package io.gitlab.ciskos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.ciskos.model.Site;

public interface SiteRepository extends PagingAndSortingRepository<Site, Long> {

}
